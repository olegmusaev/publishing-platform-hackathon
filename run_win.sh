#!/usr/bin/env bash


docker-compose -f build/compose/docker-compose.yml -f build/compose/docker-compose.win.yml down --volume
docker-compose -f build/compose/docker-compose.yml -f build/compose/docker-compose.win.yml up -d