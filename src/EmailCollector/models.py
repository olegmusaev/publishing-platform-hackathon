# -*- coding: utf-8 -*-
from django.db import models


class Email(models.Model):
    """
    Email and password are required. Other fields are optional.
    """
    email = models.EmailField(unique=True)
