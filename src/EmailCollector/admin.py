# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Email


class EmailAdmin(admin.ModelAdmin):
    list_display = ('email',)

    def has_add_permission(self, request):
        return False


admin.site.register(Email, EmailAdmin)