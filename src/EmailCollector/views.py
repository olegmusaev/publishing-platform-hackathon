# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseBadRequest

from .models import Email


def create(request):
    model = Email()
    model.email = request.POST.get('email')
    model.save()

    return HttpResponse()
