import './scss/main.scss';
import './js/page/levelup';
import './js/page/home';

import axios from 'axios';

const classHidden = 'hidden';

var filedUserId = document.getElementById('user_id');
var userId = null;
var _userData = null;

/* blocks */
var blockChoose = null;
var blockLobby = null;
var blockLoading = null;
var blockBattle = null;
var blockResult = null;
var blockLevelup = null;
var buttonsChooseAccept = null;

if (filedUserId) {
    userId = filedUserId.value;
}

if (userId) {
    _userData = getUserData();

    blockChoose = document.getElementById('choose');
    blockLobby = document.getElementById('lobby');
    blockLoading = document.getElementById('loading');
    blockBattle = document.getElementById('battle');
    blockResult = document.getElementById('result');
    blockLevelup = document.getElementById('levelup');

    buttonsChooseAccept = document.querySelectorAll('a.choose_accept');
}

function getUserData() {
    axios.get('/user/' + userId)
        .then(function (response) {
            if (response.data.race !== 0) {
                goLobby()
            } else {
                goChoose()
            }

            return response.data;
        })
        .catch(function (error) {
            console.log(error);
        });
}

function showLoading(userData) {
    console.log(userData);
}

function goLobby() {
    var buttonPlay = document.getElementById('button_play');

    removeAllEventListener();
    hideAllBlocks();
    showLobby();

    buttonPlay.addEventListener('click', function(){
        goLoading(_userData);
    });
}

function goChoose() {
    removeAllEventListener();

    for(var i=0; i < buttonsChooseAccept.length; i++) {
        buttonsChooseAccept[i].addEventListener('click', onButtonChooseAcceptClick);
    }

    hideAllBlocks();
    showChoose();
}

function goBattle() {
    removeAllEventListener();
    hideAllBlocks();
    showBattle();
}

function goLoading() {
    removeAllEventListener();
    hideAllBlocks();
    showLoading();

    setTimeout(function() {
        goBattle();
    }, 1000)
}


function hideAllBlocks() {
    blockChoose.classList.add(classHidden);
    blockLobby.classList.add(classHidden);
    blockLoading.classList.add(classHidden);
    blockBattle.classList.add(classHidden);
    blockResult.classList.add(classHidden);
    blockLevelup.classList.add(classHidden);
}

function showChoose() {
    blockChoose.classList.remove(classHidden);
}

function showLobby() {
    blockLobby.classList.remove(classHidden);
}

function showBattle() {
    blockBattle.classList.remove(classHidden);
}

function showResult() {
    blockResult.classList.remove(classHidden);
}

function showLevelup() {
    blockLevelup.classList.remove(classHidden);
}

function showLoading() {
    blockLoading.classList.remove(classHidden);
}

function removeAllEventListener() {
    for(var i=0; i < buttonsChooseAccept.length; i++) {
        buttonsChooseAccept[i].removeEventListener('click', onButtonChooseAcceptClick);
    }
}

function onButtonChooseAcceptClick(e) {
    var race = e.target.getAttribute('data-race');

    e.preventDefault();

    axios.get('/user/update/?delta_stone_lvl=1&delta_paper_lvl=1&delta_scissors_lvl=1&race=' + race, {})
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });

    goLobby();
}