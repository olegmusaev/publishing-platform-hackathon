(function() {

    'use strict';

    /**
     * tabs
     *
     * @description The Tabs component.
     * @param {Object} options The options hash
     */
    var tabs = function(options) {

        var el = document.querySelector(options.el);
        var elInd = options.tabNavigationIndex;
        var tabNavigationLinks = el.querySelectorAll(options.tabNavigationLinks);
        var tabContentContainers = el.querySelectorAll(options.tabContentContainers);
        var activeIndex = elInd;
        var initCalled = false;

        /**
         * init
         *
         * @description Initializes the component by removing the no-js class from
         *   the component, and attaching event listeners to each of the nav items.
         *   Returns nothing.
         */
        var init = function() {
            if (!initCalled) {
                initCalled = true;
                el.classList.remove('no-js');

                for (var i = 0; i < tabNavigationLinks.length; i++) {
                    var link = tabNavigationLinks[i];
                    handleClick(link, i);
                }
            }
        };

        /**
         * handleClick
         *
         * @description Handles click event listeners on each of the links in the
         *   tab navigation. Returns nothing.
         * @param {HTMLElement} link The link to listen for events on
         * @param {Number} index The index of that link
         */
        var handleClick = function(link, index) {
            link.addEventListener('click', function(e) {
                e.preventDefault();
                goToTab(index);
            });
        };

        /**
         * goToTab
         *
         * @description Goes to a specific tab based on index. Returns nothing.
         * @param {Number} index The index of the tab to go to
         */
        var goToTab = function(index) {
            if (activeIndex < 0 ) {
                tabNavigationLinks[index].classList.add('is-active');
                tabContentContainers[index].classList.add('is-active');
                activeIndex = index;
            } else if (index !== activeIndex && index >= 0 && index <= tabNavigationLinks.length) {
                tabNavigationLinks[activeIndex].classList.remove('is-active');
                tabNavigationLinks[index].classList.add('is-active');
                tabContentContainers[activeIndex].classList.remove('is-active');
                tabContentContainers[index].classList.add('is-active');
                activeIndex = index;
            } else if (index === activeIndex) {
                tabNavigationLinks[activeIndex].classList.remove('is-active');
                tabContentContainers[activeIndex].classList.remove('is-active');
                activeIndex = -1;
            }
        };


        /**
         * closeTab
         *
         * @description Closes open tabs. Returns nothing.
         * @param {Number} index The index of the tab to go to
         */
        var closeTab = function(index) {
            if (activeIndex > -1 ) {
                tabNavigationLinks[activeIndex].classList.remove('is-active');
                tabContentContainers[activeIndex].classList.remove('is-active');
                activeIndex = "-1";
            }
        };

        /**
         * Returns init and goToTab
         */
        return {
            init: init,
            goToTab: goToTab,
            close: closeTab
        };

    };

    /**
     * Attach to global namespace
     */
    window.tabs = tabs;

})();

var FP_ENABLED_CLASSPATH = 'fp-enabled',
    MIN_WIDTH_INIT = 1050,
    SLIDE_CLASSPATH = '.fp-section',
    DOWN_DIRECTION = 'down',
    UP_DIRECTION = 'up',
    MIN_HEIGHT_INIT = 500;

var fullpageSlider = {
    $el: $('#fullpage')
};

var footerAccess = 0;
var $countSlidesSection = $(SLIDE_CLASSPATH).length;

fullpageSlider.apply = function () {
    fullpageSlider.$el.fullpage({
        menu: '#main-menu-list',
        responsiveWidth: 720,
        navigation: false,
        navigationPosition: 'left',
        onLeave: function (index, nextIndex, direction) {
            if((direction == DOWN_DIRECTION) && (nextIndex == $countSlidesSection) && (footerAccess == 0)){
                return false;
            }
            if ((direction == UP_DIRECTION)) {
                footerAccess = 0;
            }
            skillTabs.close();
        }
    });
};

fullpageSlider.destroy = function () {
    $.fn.fullpage.destroy('all');
};

var mainMenu = $('.js-main-menu-list'),
    mainMenuWrapper = $('.js-main-menu'),
    mainMenuWrapperOpenedClass = 'main-menu__open',
    mainMenuOpenedClass = 'main-menu_list__open',
    menuToggleButton = $('.js-main-menu-toggle'),
    menuToggleButtonOpenedClass = 'button-toggle__active',
    bodyMainMenuOpenedClass = "main-menu-opened";


function openMenu() {
    mainMenu.addClass(mainMenuOpenedClass);
    mainMenuWrapper.addClass(mainMenuWrapperOpenedClass);
    menuToggleButton.addClass(menuToggleButtonOpenedClass);
    $('body').addClass(bodyMainMenuOpenedClass);
}

function closeMenu() {
    mainMenu.removeClass(mainMenuOpenedClass);
    mainMenuWrapper.removeClass(mainMenuWrapperOpenedClass);
    menuToggleButton.removeClass(menuToggleButtonOpenedClass);
    $('body').removeClass(bodyMainMenuOpenedClass);
}


menuToggleButton.click(function () {
    if ($(this).hasClass(menuToggleButtonOpenedClass)) {
        closeMenu();
    } else {
        openMenu();
    }
});

$(window).on('resize', function () {
    closeMenu();
});

$('.js-main-menu-link').click(function (e) {
    e.preventDefault();
    var $el = $(this),
        anchor = $el.data('menuanchor'),
        target = $('body').find("[data-anchor='" + anchor + "']");
    if ($('html').hasClass(FP_ENABLED_CLASSPATH)) {
        $.fn.fullpage.moveTo(anchor);
    } else {
        closeMenu();
        $('.js-main-menu-link').removeClass('active');
        $(this).addClass('active');
        $('html, body').animate({
            scrollTop: target.offset().top
        }, 500);
    }
});

$('.js-anchor-to-about').click(function(e){
    e.preventDefault();
    footerAccess = 1;
    $.fn.fullpage.moveTo($countSlidesSection);
});

var raceTabs = tabs({
    el: '#tabs-race',
    tabNavigationLinks: '.js-tabs-race-nav-link',
    tabContentContainers: '.js-tabs-race-tab',
    tabNavigationIndex: "0"
});
var skillTabs = tabs({
    el: '#tabs-skill',
    tabNavigationLinks: '.js-tabs-skill-nav-link',
    tabContentContainers: '.js-tabs-skill-tab',
    tabNavigationIndex: "-1"
});

$('body').click(function(evt){
    if(evt.target.id == "tabs-skill")
        return;
    //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
    if($(evt.target).closest('#tabs-skill').length)
        return;

    skillTabs.close();
});

$(document).ready(function() {
    if (!$('html').hasClass(FP_ENABLED_CLASSPATH)) {
        $('.js-main-menu-item:first-child .js-main-menu-link').addClass('active');
    }
    $(window).resize(function () {
        if ($(window).width() < MIN_WIDTH_INIT || $(window).height() < MIN_HEIGHT_INIT) {
            if ($('html').hasClass(FP_ENABLED_CLASSPATH)) {
                fullpageSlider.destroy();
            }
        } else if (!$('html').hasClass(FP_ENABLED_CLASSPATH)) {
            fullpageSlider.apply();
        }
    }).trigger('resize');


    raceTabs.init();
    skillTabs.init();
});

$('.js-front-news-header').on('click', function() {
    $('.js-front-news-item').removeClass('active');
    $(this).closest('.js-front-news-item').addClass('active');
});