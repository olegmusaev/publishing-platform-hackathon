var luMaxChecks = 2,
    luBuffer = [],
    luFormGroup = $('.js-lu-form-group'),
    luFormCheckbox = $('.js-lu-form-checkbox');

/** отслеживаем изменение состояния чекбокса **/
/** при выборе чекбокса, индекс родительского дива записывается в buffer **/
/** если число выбранных чекбоксов превышает maxChecks, чекбокс в диве с индексом хранящимся в buffer[0] отключается,  **/
/** buffer[0] удаляется, все элементы buffer смещаются в начало, последний индекс родительского дива выбранного чекбокса записывается в buffer **/
/** сразу после изменений в buffer, этот массив записывается в локальное хранилище **/
/** если пользователь снимает флажок с чекбокса, значение индекса родителя удаляется из buffer, смещая данные на 1 элемент к началу **/
(function($) {
    luFormCheckbox.on('change', function () {
        if ($(this).is(":checked")) {
            if (luBuffer.length == luMaxChecks) {
                luFormGroup.eq(luBuffer[0]).find('.js-lu-form-checkbox').prop("checked", false);
                luBuffer.splice(0, 1);
            }
            luBuffer.push($(this).closest('.js-lu-form-group').index());
            $(this).prop("checked", true);
        } else {
            $(this).prop("checked", false);
            luBuffer.splice(luBuffer.indexOf($(this).closest('.js-lu-form-group').index()), 1);
        }
    });
})(jQuery);