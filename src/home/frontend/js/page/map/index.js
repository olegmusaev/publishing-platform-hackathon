(function($) {
    $(document).ready(function(){
        var mapProfileHpBar = $('.js-map-profile-hp-value'),
            mapProfileHpBarValue = mapProfileHpBar.attr('data-width');
        mapProfileHpBar.css('width', '' + mapProfileHpBarValue + '%');

        $('.js-profile-skill-value').each(function() {
           var mapProfileSkillBar = $(this),
               mapProfileSkillBarValue = mapProfileSkillBar.attr('data-width');
            mapProfileSkillBar.css('width', '' + mapProfileSkillBarValue + 'px');
        });
    });
})(jQuery);
