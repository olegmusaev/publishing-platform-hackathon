webpackJsonp(["main"],{

/***/ "../../../../../src lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src lazy recursive";

/***/ }),

/***/ "../../../../../src/app/api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ApiService = (function () {
    function ApiService(http) {
        /*  this.http.get(this.apilink + "user/fake-login/666", { withCredentials: true }).subscribe((data) => {
      
        })*/
        var _this = this;
        this.http = http;
        this.apilink = "http://prod.goty.io/";
        this.apiending = "";
        this.currentState = "race-chooser";
        this.currentBattleStatus = "";
        this.myId = 0;
        this.applicationId = 0;
        this.battleId = 0;
        this.timeForRound = 10000;
        this.currentRounds = [];
        this.currentLevel = 0;
        this.hp = 0;
        this.wins = 0;
        this.battles = 0;
        this.hp_lvl = 0;
        this.scissors_lvl = 0;
        this.paper_lvl = 0;
        this.stone_lvl = 0;
        this.race = 0;
        this.free_xp = 0;
        this.userName = "";
        this.enemyGet = false;
        this.enemyMaxHp = 0;
        this.myMaxHp = 0;
        this.currentRound = 0;
        this.moveMaked = false;
        this.enemy = {};
        // this.currentState = "battle";
        this.getMe();
        setInterval(function () {
            if (_this.currentState != "await" && _this.currentState != "battle") {
                _this.getMe();
            }
        }, 1000);
    }
    ApiService.prototype.getMe = function () {
        var _this = this;
        // console.log("get me:");
        if (this.currentState != "await") {
            this.http.get(this.apilink + "user/account_info/", { withCredentials: true }).subscribe(function (data) {
                _this.user = data;
                //console.log("user:", this.user);
                _this.userName = data["first_name"] + " " + data["last_name"];
                _this.myId = data["user_id"];
                /*
          last_name	"fake_user_666_last_name"
          hp	500
          hp_updated_at	null
          xp	0
          name	"fake_user_666"
          first_name	"fake_user_666_first_name"
          stone_lvl	0
          free_xp_amount	0
          level	0
          wins	0
          battles	0
          hp_lvl	0
          paper_lvl	0
          race	2
          scissors_lvl	0
                */
                _this.hp = data.hp;
                _this.battles = data.battles;
                _this.stone_lvl = data.stone_lvl;
                _this.paper_lvl = data.paper_lvl;
                _this.scissors_lvl = data.scissors_lvl;
                _this.free_xp = data.free_xp_amount;
                if (_this.user.race == 0) {
                    _this.currentState = "race-chooser";
                }
                else {
                    _this.currentState = "lobby";
                }
                _this.currentLevel = data.level;
            });
        }
    };
    ApiService.prototype.chooseRace = function (num) {
        var _this = this;
        // this.currentState = "lobby";
        this.http.get(this.apilink + "user/update/?race=" + num, { withCredentials: true }).subscribe(function (data) {
            _this.currentState = "lobby";
        });
    };
    ApiService.prototype.toBattle = function () {
        var _this = this;
        this.currentState = "await";
        this.http.post(this.apilink + "battle/application/new/" + this.apiending, {}, { withCredentials: true }).subscribe(function (data) {
            _this.applicationId = data["application_id"];
            _this.startChecking();
        }, function (err) {
            _this.currentState = "await";
            _this.getApplicationsList();
        });
    };
    ApiService.prototype.getApplicationsList = function () {
        var _this = this;
        this.http.get(this.apilink + "battle/application/list/", { withCredentials: true }).subscribe(function (data) {
            console.log("get applicationslist:", data);
            if (data["applications"].length != 0) {
                _this.applicationId = data["applications"][0].application_id;
                _this.startChecking();
            }
            else {
                setTimeout(function () {
                    _this.getApplicationsList();
                }, 500);
            }
        });
    };
    ApiService.prototype.startChecking = function () {
        var _this = this;
        this.timer = setInterval(function () {
            _this.http.get(_this.apilink + "battle/application/" + _this.applicationId + "/status/" + _this.apiending, { withCredentials: true }).subscribe(function (data) {
                if (data.battle_id != null) {
                    clearInterval(_this.timer);
                    _this.battleId = data["battle_id"];
                    _this.currentState = "battle";
                    console.log(data);
                    _this.updateBattleStatus();
                }
                else {
                    console.log("not exist");
                }
            });
        }, 1000);
    };
    ApiService.prototype.updateBattleStatus = function () {
        var _this = this;
        this.http.get(this.apilink + "battle/" + this.battleId + "/status", { withCredentials: true }).subscribe(function (data) {
            console.log("battle status:", data);
            console.log(_this.currentRound, data.data.length);
            if (_this.currentRound != data.data.length) {
                _this.currentRounds = [];
                for (var _i = 0, _a = data.data; _i < _a.length; _i++) {
                    var item = _a[_i];
                    _this.currentRounds.push(_this.parseRound(item));
                }
                _this.currentRounds = _this.currentRounds.reverse();
                if (!_this.enemyGet) {
                    console.log(_this.currentRounds);
                    console.log(_this.currentRounds[0]);
                    console.log(_this.currentRounds[0].enemy_id);
                    _this.getEnemy(_this.currentRounds[0].enemy_id);
                    _this.myMaxHp = _this.currentRounds[0].my_hp;
                    _this.enemyMaxHp = _this.currentRounds[0].enemy_hp;
                    _this.enemyGet = true;
                }
                if (_this.currentRounds[_this.currentRounds.length - 1].me_hp <= 0) {
                    _this.lose();
                }
                else if (_this.currentRounds[_this.currentRounds.length - 1].enemy_hp) {
                    _this.win();
                }
                if (_this.currentRounds.length > _this.currentRound) {
                    _this.moveMaked = false;
                    _this.currentRound = _this.currentRounds.length;
                }
            }
            else {
                setInterval(function () {
                    _this.updateBattleStatus();
                }, 500);
            }
        });
    };
    ApiService.prototype.lose = function () {
        this.currentState = "lose";
    };
    ApiService.prototype.win = function () {
        this.currentState = "win";
    };
    ApiService.prototype.parseBattleStatus = function (data) {
    };
    ApiService.prototype.isBattleAvailable = function () {
        if (this.currentState == "lobby") {
            return true;
        }
        else {
            return false;
        }
    };
    ApiService.prototype.makeMove = function (num) {
        var _this = this;
        if (!this.moveMaked) {
            this.moveMaked = true;
            this.http.post(this.apilink + "battle/" + this.battleId + "/move/", { choice: num }, { withCredentials: true }).subscribe(function (data) {
                if (data.result == true) {
                    _this.updateBattleStatus();
                }
            });
        }
    };
    ApiService.prototype.parseRound = function (data) {
        var obj = {};
        if (data["players_choices"].length != 0) {
            obj.round_id = data["round_id"];
            obj.round_created = data["round_created"];
            obj.round_status = data["round_status"];
            //0,1,2
            //running, finished, stale
            console.log("parse round:", data, this.myId);
            var me = 0;
            ;
            var enemy = 0;
            ;
            if (data["players_choices"][0]["id"] == this.myId) {
                me = 0;
                enemy = 1;
            }
            else {
                me = 1;
                enemy = 0;
            }
            obj.battle_id = data["battle_id"];
            obj.battle_status = data["battle_status"];
            //0,1,2
            //init, running, finished
            obj.me_move = data["players_choices"][me]["choice"];
            obj.me_hp = data["players_choices"][me]["hp"];
            obj.enemy_id = data["players_choices"][enemy]["id"];
            obj.enemy_move = data["players_choices"][enemy]["choice"];
            obj.enemy_hp = data["players_choices"][enemy]["hp"];
            return obj;
        }
        else {
        }
    };
    ApiService.prototype.getEnemy = function (id) {
        var _this = this;
        this.http.get(this.apilink + "/user/" + id).subscribe(function (data) {
            _this.enemy = {};
            _this.enemy.nickname = data["first_name"] + " " + data["last_name"];
            _this.enemy.stone_lvl = data["stone_lvl"];
            _this.enemy.paper_lvl = data["paper_lvl"];
            _this.enemy.scissors_lvl = data["scissors_lvl"];
            _this.enemy.race = data["race"];
        });
    };
    ApiService.prototype.getMyHealth = function () {
        if (this.currentRounds) {
            return this.currentRounds[this.currentRounds.length - 1].me_hp;
        }
    };
    ApiService.prototype.getEnemyHealth = function () {
        if (this.currentRounds) {
            return this.currentRounds[this.currentRounds.length - 1].enemy_hp;
        }
    };
    return ApiService;
}());
ApiService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
], ApiService);

var _a;
//# sourceMappingURL=api.service.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host{\r\n      background-image:url(/assets/images/bg.png);\r\n    background-position:center;\r\n    background-size:cover;\r\n\r\n    height:100%;\r\n    width:100%;\r\n    display:block;\r\n\r\n    \r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-main>\n</app-main>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api_service__ = __webpack_require__("../../../../../src/app/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__main_main_component__ = __webpack_require__("../../../../../src/app/main/main.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__race_chooser_race_chooser_component__ = __webpack_require__("../../../../../src/app/race-chooser/race-chooser.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__lobby_lobby_component__ = __webpack_require__("../../../../../src/app/lobby/lobby.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__battle_battle_component__ = __webpack_require__("../../../../../src/app/battle/battle.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__chat_chat_component__ = __webpack_require__("../../../../../src/app/chat/chat.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__user_state_user_state_component__ = __webpack_require__("../../../../../src/app/user-state/user-state.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__await_page_await_page_component__ = __webpack_require__("../../../../../src/app/await-page/await-page.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_4__main_main_component__["a" /* MainComponent */],
            __WEBPACK_IMPORTED_MODULE_5__race_chooser_race_chooser_component__["a" /* RaceChooserComponent */],
            __WEBPACK_IMPORTED_MODULE_6__lobby_lobby_component__["a" /* LobbyComponent */],
            __WEBPACK_IMPORTED_MODULE_7__battle_battle_component__["a" /* BattleComponent */],
            __WEBPACK_IMPORTED_MODULE_9__chat_chat_component__["a" /* ChatComponent */],
            __WEBPACK_IMPORTED_MODULE_10__user_state_user_state_component__["a" /* UserStateComponent */],
            __WEBPACK_IMPORTED_MODULE_11__await_page_await_page_component__["a" /* AwaitPageComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_8__angular_common_http__["b" /* HttpClientModule */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_0__api_service__["a" /* ApiService */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/await-page/await-page.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "p{\r\n    font-size:200px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/await-page/await-page.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  await-page works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/await-page/await-page.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AwaitPageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AwaitPageComponent = (function () {
    function AwaitPageComponent() {
    }
    AwaitPageComponent.prototype.ngOnInit = function () {
    };
    return AwaitPageComponent;
}());
AwaitPageComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-await-page',
        template: __webpack_require__("../../../../../src/app/await-page/await-page.component.html"),
        styles: [__webpack_require__("../../../../../src/app/await-page/await-page.component.css")]
    }),
    __metadata("design:paramtypes", [])
], AwaitPageComponent);

//# sourceMappingURL=await-page.component.js.map

/***/ }),

/***/ "../../../../../src/app/battle/battle.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"battle\" class=\"hidden\" style=\"background-image:url(/static/assets/images/battle/game_back_1.png)\">\n  <div class=\"game\">\n\n    <!-- left user -->\n    <div class=\"game_user-area game_user-area__left\">\n      <div class=\"game_user-info\">\n        {{ this.api.user.userName }}\n        <div class=\"hp-bar\">\n          <div class=\"hp-bar_fill\" [style.width]=\"this.getMyPercent()\">{{this.getMyHealth()}}</div>\n        </div>\n      </div>\n\n      <div class=\"game_user-history\">\n        <div class=\"game_user-history-item\" style=\"background-image:url(/static/assets/images/battle/fireball.png)\"></div>\n        <div class=\"game_user-history-item\" style=\"background-image:url(/static/assets/images/battle/chain.png)\"></div>\n        <div class=\"game_user-history-item\" style=\"background-image:url(/static/assets/images/battle/chain.png)\"></div>\n        <div class=\"game_user-history-item\" style=\"background-image:url(/static/assets/images/battle/fireball.png)\"></div>\n        <div class=\"game_user-history-item\" style=\"background-image:url(/static/assets/images/battle/lazer.png)\"></div>\n      </div>\n\n      <div class=\"game_action-block\">\n        <div class=\"game_pers\">\n          <img alt=\"\" src=\"/static/img/titan.png\">\n        </div>\n      </div>\n\n\n      <div class=\"game_user-skills\">\n        <div class=\"game_user-skill\" style=\"background-color:#4790ff; width:100%;\">\n          <div class=\"game_user-skill_line\" style=\"background-image:url(/static/assets/images/battle/lazer.png)\">{{this.getMyScissors()}}</div>\n        </div>\n        <div class=\"game_user-skill\" style=\"background-color:#ff5500; width:75%;\">\n          <div class=\"game_user-skill_line\" style=\"background-image:url(/static/assets/images/battle/fireball.png)\">{{this.getMyStone()}}</div>\n        </div>\n        <div class=\"game_user-skill\" style=\"background-color:#00ff8a; width:50%;\">\n          <div class=\"game_user-skill_line\" style=\"background-image:url(/static/assets/images/battle/chain.png)\">{{this.getMyPaper()}}</div>\n        </div>\n      </div>\n\n\n    </div>\n    <!-- actions block-->\n    <div class=\"game_user-actions-wrapper\">\n      <div class=\"game_user-actions-wrapper2\">\n        <div class=\"game_user-actions-border\">\n          <div class=\"game_user-actions\" *ngIf=\"!this.api.moveMaked\">\n\n            <div class=\"item1 item\" style=\"background-image:url(/static/assets/images/battle/round-lazer.png)\" (click)=\"this.move(2)\">\n            </div>\n            <div class=\"item2 item\" style=\"background-image:url(/static/assets/images/battle/round-chain.png)\" (click)=\"this.move(1)\">\n            </div>\n            <div class=\"item3 item\" style=\"background-image:url(/static/assets/images/battle/round-fireball.png)\" (click)=\"this.move(0)\">\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"game_user_qwest\" style=\"background-image:url(/static/assets/images/battle/qwest.png)\"></div>\n    </div>\n    <!-- right user -->\n    <div class=\"game_user-area game_user-area__right\">\n      <div class=\"game_user-info\">\n        {{this.api.enemy.nickname}}\n        <div class=\"hp-bar\">\n          <div class=\"hp-bar_fill\" [style.width]=\"this.getEnemyPercent()\">{{this.getEnemyPercent()}}</div>\n        </div>\n      </div>\n\n      <div class=\"game_user-history\">\n        <div class=\"game_user-history-item\" style=\"background-image:url(/static/assets/images/battle/fireball.png)\"></div>\n        <div class=\"game_user-history-item\" style=\"background-image:url(/static/assets/images/battle/chain.png)\"></div>\n        <div class=\"game_user-history-item\" style=\"background-image:url(/static/assets/images/battle/chain.png)\"></div>\n        <div class=\"game_user-history-item\" style=\"background-image:url(/static/assets/images/battle/fireball.png)\"></div>\n        <div class=\"game_user-history-item\" style=\"background-image:url(/static/assets/images/battle/lazer.png)\"></div>\n      </div>\n\n      <div class=\"game_action-block\">\n        <div class=\"game_pers\">\n          <img alt=\"\" src=\"/static/img/titan.png\">\n        </div>\n      </div>\n\n      <div class=\"game_user-skills\">\n        <div class=\"game_user-skill\" style=\"background-color:#4790ff; width:100%;\">\n          <div class=\"game_user-skill_line\" style=\"background-image:url(/static/assets/images/battle/lazer.png)\">{{this.getEnemyScissors()}}</div>\n        </div>\n        <div class=\"game_user-skill\" style=\"background-color:#ff5500; width:75%;\">\n          <div class=\"game_user-skill_line\" style=\"background-image:url(/static/assets/images/battle/fireball.png)\">{{this.getEnemyStone()}}</div>\n        </div>\n        <div class=\"game_user-skill\" style=\"background-color:#00ff8a; width:50%;\">\n          <div class=\"game_user-skill_line\" style=\"background-image:url(/static/assets/images/battle/chain.png)\">{{this.getEnemyPaper()}}</div>\n        </div>\n      </div>\n\n    </div>\n    <div class=\"game_timer\">\n      00:10\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/battle/battle.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#battle {\n  display: block;\n  position: relative;\n  height: 100%;\n  padding-top: 95px;\n  box-sizing: border-box;\n  background-repeat: no-repeat;\n  background-size: cover; }\n\n.game {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  height: 100%;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  width: 100%; }\n\n.game::after {\n  content: '';\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: url(/static/assets/images/people.png) no-repeat;\n  background-position: 0 100%;\n  background-size: 100%;\n  z-index: 0; }\n\n.game_action-block {\n  position: absolute;\n  left: 0;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%); }\n\n.game_user-area__right .game_action-block {\n  left: auto;\n  right: 0; }\n\n.game_user-area,\n.game_user-actions-wrapper {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  height: 100%;\n  position: relative;\n  width: 33%; }\n\n.game_user-area__left {\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start; }\n\n.game_user-area__right {\n  -webkit-box-align: end;\n      -ms-flex-align: end;\n          align-items: flex-end; }\n\n.game_user-info {\n  position: relative;\n  width: calc(100% - 35px);\n  color: #fff;\n  font-weight: bold;\n  margin-bottom: 40px;\n  overflow: hidden; }\n\n.game_user-area__right .game_user-info {\n  text-align: right; }\n\n.hp-bar {\n  background: #ccc;\n  height: 25px;\n  width: 100%; }\n\n.hp-bar_fill {\n  background: #cc0000;\n  float: left;\n  height: 100%;\n  width: 70%; }\n\n.game_user-area__left .game_user-info {\n  left: 35px; }\n\n.game_user-area__right .game_user-info {\n  right: 35px; }\n\n.game_user-area__right .hp-bar_fill {\n  float: right; }\n\n.game_user-history {\n  width: 35px;\n  position: absolute;\n  left: 0;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%); }\n\n.game_user-area__right .game_user-history {\n  left: auto;\n  right: 0; }\n\n.game_user-history-item {\n  height: 35px;\n  width: 35px;\n  margin-bottom: 15px;\n  background-size: cover; }\n\n.game_timer {\n  font-size: 50px;\n  text-align: center;\n  left: 50%;\n  margin-left: -100px;\n  position: absolute;\n  top: 137px;\n  width: 200px;\n  color: #fff; }\n\n.game_user-actions-wrapper {\n  padding: 0 20px;\n  z-index: 10; }\n\n.game_user-actions-wrapper2 {\n  display: block;\n  position: relative;\n  height: 100%; }\n\n.game_user-actions-border {\n  position: absolute;\n  left: 0;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%);\n  width: 156px;\n  height: 156px;\n  margin: 0 auto;\n  border-radius: 50%;\n  border: 3px #464646 solid; }\n\n.game_user-actions {\n  position: absolute;\n  left: 0;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%);\n  width: 150px;\n  height: 150px;\n  margin: 0 auto;\n  border-radius: 50%;\n  border: 9px #4790ff solid; }\n\n.item {\n  position: absolute;\n  width: 75px;\n  height: 75px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  cursor: pointer;\n  transition: .2s; }\n\n.item:hover {\n  -webkit-transform: scale(1.1);\n          transform: scale(1.1); }\n\n.item1 {\n  top: -41px;\n  left: 26px; }\n\n.item2 {\n  bottom: -17px;\n  left: -34px; }\n\n.item3 {\n  right: -34px;\n  bottom: -17px; }\n\n.game_user_qwest {\n  position: absolute;\n  right: 0;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%);\n  width: 75px;\n  height: 75px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin: 0 auto; }\n\n.content {\n  width: 60px;\n  height: 60px;\n  text-align: center;\n  border-radius: 50%;\n  background: #cc0000;\n  margin-left: -8px;\n  margin-top: -8px; }\n\n.content {\n  position: absolute; }\n\n.content a {\n  width: 60px;\n  height: 60px;\n  line-height: 60px;\n  display: block;\n  position: absolute;\n  text-decoration: none;\n  font-family: 'Segoe UI', Arial, Verdana, sans-serif;\n  font-size: 20px;\n  color: #fff; }\n\n.item1 .content a {\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg); }\n\n.item2 .content a {\n  -webkit-transform: rotate(-165deg);\n          transform: rotate(-165deg); }\n\n.item3 .content a {\n  -webkit-transform: rotate(-285deg);\n          transform: rotate(-285deg); }\n\n.game_user-skills {\n  position: absolute;\n  top: -75px;\n  width: calc(100% - 35px); }\n\n.game_user-skill {\n  max-width: 100%;\n  height: 15px;\n  margin-bottom: 10px; }\n\n.game_user-skill_line {\n  width: 100%;\n  height: 15px;\n  padding-left: 25px;\n  background-size: contain;\n  background-position: 0 50%;\n  font-size: 10px;\n  background-repeat: no-repeat; }\n\n.game_user-area__left .game_user-skills {\n  left: 35px; }\n\n.game_user-area__right .game_user-skills {\n  right: 35px; }\n\n.game_pers {\n  position: relative; }\n\n.game_user-area__left .game_pers {\n  margin-left: 40px; }\n\n.game_user-area__right .game_pers {\n  float: right;\n  margin-right: 40px; }\n\n.game_pers img {\n  width: 100%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/battle/battle.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BattleComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api_service__ = __webpack_require__("../../../../../src/app/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BattleComponent = (function () {
    function BattleComponent(api) {
        this.api = api;
    }
    BattleComponent.prototype.ngOnInit = function () {
    };
    BattleComponent.prototype.getMyHealth = function () {
        if (this.api.currentRounds && this.api.currentRounds.length != 0) {
            return this.api.currentRounds[this.api.currentRounds.length - 1].my_hp;
        }
        else {
            return 1;
        }
    };
    BattleComponent.prototype.getEnemyHealth = function () {
        if (this.api.currentRounds && this.api.currentRounds.length != 0) {
            return this.api.currentRounds[this.api.currentRounds.length - 1].enemy_hp;
        }
        else {
            return 1;
        }
    };
    BattleComponent.prototype.getMyPercent = function () {
        return Math.floor(this.getMyHealth() / this.api.myMaxHp * 100) + "%";
    };
    BattleComponent.prototype.getEnemyPercent = function () {
        return Math.floor(this.getEnemyHealth() / this.api.enemyMaxHp * 100) + "%";
    };
    BattleComponent.prototype.move = function (id) {
        this.api.makeMove(id);
    };
    BattleComponent.prototype.getMyPaper = function () {
        return this.api.paper_lvl;
        //  this.stone_lvl = data.stone_lvl;
        //  this.paper_lvl = data.paper_lvl;
        //  this.scissors_lvl = data.scissors_lvl;
        //   return this.api.paper
    };
    BattleComponent.prototype.getMyStone = function () {
        return this.api.stone_lvl;
    };
    BattleComponent.prototype.getMyScissors = function () {
        return this.api.scissors_lvl;
    };
    BattleComponent.prototype.getEnemyPaper = function () {
        return this.api.enemy.paper_lvl;
    };
    BattleComponent.prototype.getEnemyStone = function () {
        return this.api.enemy.stone_lvl;
    };
    BattleComponent.prototype.getEnemyScissors = function () {
        return this.api.enemy.scissors_lvl;
    };
    return BattleComponent;
}());
BattleComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'app-battle',
        template: __webpack_require__("../../../../../src/app/battle/battle.component.html"),
        styles: [__webpack_require__("../../../../../src/app/battle/battle.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__api_service__["a" /* ApiService */]) === "function" && _a || Object])
], BattleComponent);

var _a;
//# sourceMappingURL=battle.component.js.map

/***/ }),

/***/ "../../../../../src/app/chat/chat.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"body\">\n  <div class=\"upper\">\n    <div class=\"rawMessages\">\n      <div class=\"message\">\n        <div class=\"user\">ololosh</div>\n        <div class=\"message\">ololo ololo</div>\n      </div>\n    </div>\n  </div>\n  <div class=\"down\">\n    <input type=\"text\">\n    <button>send</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/chat/chat.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".body {\n  width: 100%;\n  height: 100%;\n  position: relative;\n  border: 1px solid black;\n  background-color: rgba(255, 255, 255, 0.8); }\n  .body * {\n    box-sizing: border-box; }\n  .body .upper {\n    width: 100%;\n    position: absolute;\n    top: 0;\n    left: 0;\n    bottom: 60px; }\n  .body .down {\n    height: 60px;\n    position: absolute;\n    bottom: 0;\n    left: 0;\n    width: 100%; }\n    .body .down input {\n      width: 100%;\n      height: 30px; }\n    .body .down button {\n      height: 30px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/chat/chat.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ChatComponent = (function () {
    function ChatComponent() {
    }
    ChatComponent.prototype.ngOnInit = function () {
    };
    return ChatComponent;
}());
ChatComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-chat',
        template: __webpack_require__("../../../../../src/app/chat/chat.component.html"),
        styles: [__webpack_require__("../../../../../src/app/chat/chat.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], ChatComponent);

//# sourceMappingURL=chat.component.js.map

/***/ }),

/***/ "../../../../../src/app/lobby/lobby.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"body\">\n  <div class=\"map\" [style]=\"this.getBg()\">\n    <!-- <app-chat></app-chat> -->\n    <app-user-state></app-user-state>\n\n  </div>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/lobby/lobby.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  width: 100%;\n  height: 100%;\n  display: block; }\n\n.body {\n  width: 100%;\n  height: 100%;\n  display: block;\n  box-sizing: border-box;\n  position: relative; }\n  .body .map {\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background-size: contain;\n    background-position: center;\n    background-repeat: no-repeat;\n    background-color: black; }\n  .body app-chat {\n    position: absolute;\n    bottom: 0;\n    right: 0;\n    width: 30%;\n    height: 30%; }\n  .body app-user-state {\n    position: absolute;\n    bottom: 0;\n    left: 0;\n    width: 30%;\n    height: 30%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/lobby/lobby.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LobbyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api_service__ = __webpack_require__("../../../../../src/app/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LobbyComponent = (function () {
    function LobbyComponent(sanitizer, api) {
        this.sanitizer = sanitizer;
        this.api = api;
    }
    LobbyComponent.prototype.ngOnInit = function () {
    };
    LobbyComponent.prototype.getBg = function () {
        var img = "/assets/images/bg/" + this.api.currentLevel + ".png";
        var style = "background-image: url(" + img + ")";
        var sanitized = this.sanitizer.bypassSecurityTrustStyle(style);
        // console.log(sanitized)
        return sanitized;
        //this.api.currentLevel
    };
    return LobbyComponent;
}());
LobbyComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'app-lobby',
        template: __webpack_require__("../../../../../src/app/lobby/lobby.component.html"),
        styles: [__webpack_require__("../../../../../src/app/lobby/lobby.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["b" /* DomSanitizer */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["b" /* DomSanitizer */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__api_service__["a" /* ApiService */]) === "function" && _b || Object])
], LobbyComponent);

var _a, _b;
//# sourceMappingURL=lobby.component.js.map

/***/ }),

/***/ "../../../../../src/app/main/main.component.html":
/***/ (function(module, exports) {

module.exports = "<header class=\"header\" [class.chooser]=\"this.api.currentState=='race-chooser'\" *ngIf=\"this.api.currentState!='battle'\">\n  <div class=\"header_container\">\n    <div class=\"header_column header_column__left\">\n      <a href=\"#\"><img class=\"logotype\" src=\"/static/assets/images/logo.png\" alt=\"logo\"></a>\n    </div>\n    <div class=\"header_column header_column__right\">\n      <a href=\"#\" id=\"button_play\" class=\"choose_accept\" (click)=\"this.battle()\" *ngIf=\"this.api.isBattleAvailable()\"> В бой</a>\n    </div>\n  </div>\n</header>\n<app-race-chooser *ngIf=\"this.api.currentState=='race-chooser'\">\n\n</app-race-chooser>\n\n<app-lobby *ngIf=\"this.api.currentState=='lobby'\">\n</app-lobby>\n\n<app-battle *ngIf=\"this.api.currentState=='battle'\">\n</app-battle>\n\n\n<app-await-page *ngIf=\"this.api.currentState=='await'\"></app-await-page>\n"

/***/ }),

/***/ "../../../../../src/app/main/main.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  background-image: url(/static/assets/images/bg.png);\n  background-position: center;\n  background-size: cover;\n  height: 100%;\n  width: 100%; }\n  :host .header {\n    position: fixed;\n    top: 0;\n    height: 87px;\n    width: 100%;\n    background: rgba(0, 0, 0, 0.6);\n    box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.75);\n    z-index: 2; }\n    :host .header .logotype {\n      height: 47px; }\n    :host .header .choose_accept {\n      display: inline-block;\n      vertical-align: middle;\n      color: #fff;\n      font-size: 14px;\n      font-weight: 700;\n      text-transform: uppercase;\n      border: 2px solid #fff;\n      line-height: 18px;\n      padding: 11px 28px;\n      background-color: rgba(0, 0, 0, 0.7);\n      transition: background-color .15s ease-out, color .15s ease-out; }\n    :host .header .choose_accept:hover {\n      color: #474747;\n      background-color: #f9f5e1; }\n    :host .header_container {\n      max-width: 1366px;\n      margin: 0 auto;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: horizontal;\n      -webkit-box-direction: normal;\n          -ms-flex-flow: row wrap;\n              flex-flow: row wrap;\n      padding: 20px;\n      position: absolute;\n      width: 100%;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center; }\n    :host .header_column {\n      -webkit-box-flex: 1;\n          -ms-flex: 1 auto;\n              flex: 1 auto; }\n      :host .header_column__left {\n        text-align: left; }\n      :host .header_column__right {\n        text-align: right; }\n    :host .header.chooser {\n      background-color: initial;\n      box-shadow: none; }\n\napp-lobby {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  top: 0;\n  left: 0; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/main.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api_service__ = __webpack_require__("../../../../../src/app/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MainComponent = (function () {
    function MainComponent(api) {
        this.api = api;
        this.currentState = "race-chooser";
    }
    MainComponent.prototype.ngOnInit = function () {
    };
    MainComponent.prototype.battle = function () {
        this.api.toBattle();
    };
    return MainComponent;
}());
MainComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'app-main',
        template: __webpack_require__("../../../../../src/app/main/main.component.html"),
        styles: [__webpack_require__("../../../../../src/app/main/main.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__api_service__["a" /* ApiService */]) === "function" && _a || Object])
], MainComponent);

var _a;
//# sourceMappingURL=main.component.js.map

/***/ }),

/***/ "../../../../../src/app/race-chooser/race-chooser.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"body\">\n  <div class=\"bgWrapper\"></div>\n  <div class=\"choose-general\">\n    <div class=\"choose\">\n      <h1 class=\"choose_title\">Выбери расу</h1>\n      <ul class=\"choose_container\">\n        <li class=\"choose_card\">\n          <div class=\"choose_image-container\">\n            <div class=\"img dog\">\n\n            </div>\n\n          </div>\n          <ul class=\"choose_description\">\n            <li class=\"choose_item\">\n              <p class=\"choose_late\">Псы войны</p>\n            </li>\n            <li class=\"choose_item\">\n              <div class=\"icon dog\"></div>\n            </li>\n          </ul>\n          <div class=\"choose_card-race\">\n            <div class=\"choose_history\">\n              <p>Это и определило их ментальные особенности. Возможность созерцать вечность сделала их холодными и неэмоциональными.\n                Все, что их заботит – это сохранение баланса в хрупкой вселенной, который сегодня стремятся нарушить псы\n                и котики – временные пришельцы, по их меркам. Вернув баланс, титаны снова смогут погрузиться в сон. Внешнее\n                обладая значительными размерами, они в то же время способны на быстрые перемещения и обладают хорошей подвижностью.\n                Человекоподобное строение позволяет им без труда взаимодействовать с предметами, как телекинезом, так и простым\n                физическим контактом. Кожа отливающая металлом, отражает значительную часть спектра, что позволяет им находиться\n                на значительном количестве планет с суровыми условиями без какой-либо защиты. Особой, псионической способностью\n                титанов является умение вызывать падение метеоритов на планету, в окрестности своего местонахождения. Испепеляя\n                все вокруг, разрушения, при этом не приносят ущебра им самим. Кажущееся воинственное поведение титанов не\n                является таковым по сути, они лишь пытаются навести порядок дома, избавившись от надоедливых паразитов. Их\n                действия носят в первую очередь усмирительный характер.\n              </p>\n            </div>\n            <div class=\"choose_accept-container\">\n              <a href=\"#\" class=\"choose_accept\" data-race=\"1\" (click)=\"this.select(1)\">Выбрать</a>\n            </div>\n          </div>\n        </li>\n        <li class=\"choose_card\">\n          <div class=\"choose_image-container\">\n            <div class=\"img titan\">\n\n            </div>\n          </div>\n          <ul class=\"choose_description\">\n            <li class=\"choose_item\">\n              <p class=\"choose_late\">Титаны</p>\n            </li>\n            <li class=\"choose_item\">\n              <div class=\"icon titan\"></div>\n            </li>\n          </ul>\n          <div class=\"choose_card-race\">\n            <div class=\"choose_history\">\n              <p>Это и определило их ментальные особенности. Возможность созерцать вечность сделала их холодными и неэмоциональными.\n                Все, что их заботит – это сохранение баланса в хрупкой вселенной, который сегодня стремятся нарушить псы\n                и котики – временные пришельцы, по их меркам. Вернув баланс, титаны снова смогут погрузиться в сон. Внешнее\n                обладая значительными размерами, они в то же время способны на быстрые перемещения и обладают хорошей подвижностью.\n                Человекоподобное строение позволяет им без труда взаимодействовать с предметами, как телекинезом, так и простым\n                физическим контактом. Кожа отливающая металлом, отражает значительную часть спектра, что позволяет им находиться\n                на значительном количестве планет с суровыми условиями без какой-либо защиты. Особой, псионической способностью\n                титанов является умение вызывать падение метеоритов на планету, в окрестности своего местонахождения. Испепеляя\n                все вокруг, разрушения, при этом не приносят ущебра им самим. Кажущееся воинственное поведение титанов не\n                является таковым по сути, они лишь пытаются навести порядок дома, избавившись от надоедливых паразитов. Их\n                действия носят в первую очередь усмирительный характер.\n              </p>\n            </div>\n            <div class=\"choose_accept-container\">\n              <a href=\"#\" class=\"choose_accept\" data-race=\"2\" (click)=\"this.select(2)\">Выбрать</a>\n            </div>\n          </div>\n        </li>\n        <li class=\"choose_card\">\n          <div class=\"choose_image-container\">\n            <div class=\"img cat\">\n\n            </div>\n          </div>\n          <ul class=\"choose_description\">\n            <li class=\"choose_item\">\n              <p class=\"choose_late\">Котики</p>\n            </li>\n            <li class=\"choose_item\">\n              <div class=\"icon cats\"></div>\n            </li>\n          </ul>\n          <div class=\"choose_card-race\">\n            <div class=\"choose_history\">\n              <p>Это и определило их ментальные особенности. Возможность созерцать вечность сделала их холодными и неэмоциональными.\n                Все, что их заботит – это сохранение баланса в хрупкой вселенной, который сегодня стремятся нарушить псы\n                и котики – временные пришельцы, по их меркам. Вернув баланс, титаны снова смогут погрузиться в сон. Внешнее\n                обладая значительными размерами, они в то же время способны на быстрые перемещения и обладают хорошей подвижностью.\n                Человекоподобное строение позволяет им без труда взаимодействовать с предметами, как телекинезом, так и простым\n                физическим контактом. Кожа отливающая металлом, отражает значительную часть спектра, что позволяет им находиться\n                на значительном количестве планет с суровыми условиями без какой-либо защиты. Особой, псионической способностью\n                титанов является умение вызывать падение метеоритов на планету, в окрестности своего местонахождения. Испепеляя\n                все вокруг, разрушения, при этом не приносят ущебра им самим. Кажущееся воинственное поведение титанов не\n                является таковым по сути, они лишь пытаются навести порядок дома, избавившись от надоедливых паразитов. Их\n                действия носят в первую очередь усмирительный характер.\n              </p>\n            </div>\n            <div class=\"choose_accept-container\">\n              <a href=\"#\" class=\"choose_accept\" data-race=\"3\" (click)=\"this.select(3)\">Выбрать</a>\n            </div>\n          </div>\n        </li>\n      </ul>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/race-chooser/race-chooser.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@charset \"UTF-8\";\n.body {\n  position: relative;\n  padding-top: 75px;\n  height: 100%; }\n  .body * {\n    box-sizing: border-box; }\n  .body .logotype {\n    height: 47px; }\n  .body .bgWrapper {\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background-color: black;\n    opacity: 0.58; }\n  .body .choose-general {\n    vertical-align: baseline;\n    line-height: 20px;\n    color: #fff;\n    position: relative;\n    -webkit-text-size-adjust: 100%;\n    -webkit-font-smoothing: antialiased;\n    -moz-osx-font-smoothing: grayscale;\n    font-size: 12px; }\n  .body .choose:after {\n    visibility: hidden;\n    display: block;\n    font-size: 0;\n    content: \" \";\n    clear: both;\n    height: 0; }\n  .body .choose_title {\n    /* Style for \"Выбери рас\" */\n    color: #c0f1ff;\n    font-family: Arial;\n    font-size: 72px;\n    font-weight: 700;\n    width: 100%;\n    margin: 0;\n    left: 0;\n    text-align: center;\n    margin-bottom: 65px; }\n  .body .choose_container {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    padding-bottom: 20px;\n    margin: 0; }\n  .body .choose_card {\n    position: relative;\n    width: 28%;\n    box-sizing: border-box;\n    border-radius: 10px;\n    padding: 10px;\n    font-size: 18px;\n    color: #fff;\n    font-family: Arial, Helvetica, sans-serif; }\n  .body .choose_image-container {\n    max-width: 100%; }\n    .body .choose_image-container .img {\n      width: 100%;\n      height: 280px;\n      background-position: center top;\n      background-repeat: no-repeat; }\n      .body .choose_image-container .img.cat {\n        background-image: url(/static/assets/images/cat.png);\n        background-position: center bottom; }\n      .body .choose_image-container .img.dog {\n        background-image: url(/static/assets/images/dog.png);\n        background-position: center bottom; }\n      .body .choose_image-container .img.titan {\n        background-image: url(/static/assets/images/titan.png);\n        background-position: top center; }\n  .body .choose_history {\n    display: block;\n    overflow-y: scroll;\n    overflow-x: hidden;\n    text-overflow: ellipsis;\n    max-height: 100%;\n    font-size: 15px; }\n  .body .choose_description {\n    padding-top: 15px; }\n  .body .choose_item {\n    font-size: 15px;\n    width: 100%; }\n    .body .choose_item .icon {\n      margin: auto;\n      background-repeat: no-repeat; }\n      .body .choose_item .icon.cats {\n        background-image: url(/static/assets/images/icons/i3.png);\n        width: 86px;\n        height: 87px; }\n      .body .choose_item .icon.dog {\n        background-image: url(/static/assets/images/icons/i1.png);\n        width: 121px;\n        height: 64px; }\n      .body .choose_item .icon.titan {\n        background-image: url(/static/assets/images/icons/i2.png);\n        width: 83px;\n        height: 83px; }\n  .body .choose_item:not(:first-child) {\n    padding-top: 10px; }\n  .body .choose_accept {\n    display: inline-block;\n    vertical-align: middle;\n    color: #fff;\n    font-size: 14px;\n    font-weight: 700;\n    text-transform: uppercase;\n    border: 2px solid #fff;\n    line-height: 18px;\n    padding: 11px 28px;\n    background-color: rgba(0, 0, 0, 0.7);\n    transition: background-color .15s ease-out, color .15s ease-out; }\n  .body .choose_accept:hover {\n    color: #474747;\n    background-color: #f9f5e1; }\n  .body .choose_late {\n    /* Style for \"Пьос ваины\" */\n    color: #c0f1ff;\n    font-family: Arial;\n    font-size: 53px;\n    font-weight: 700;\n    text-align: center; }\n  @media screen and (min-width: 720px) {\n    .body .choose_card-race {\n      visibility: hidden;\n      opacity: 0;\n      transition: visibility 0s, opacity 0.5s linear;\n      -ms-flex-item-align: center;\n          -ms-grid-row-align: center;\n          align-self: center;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n      position: absolute;\n      padding: 25px 25px 70px;\n      top: 0;\n      left: 0;\n      bottom: 0;\n      right: 0;\n      color: #fff;\n      z-index: 1;\n      border-radius: 5px;\n      box-shadow: 0px 0px 13px 2px rgba(0, 0, 0, 0.75);\n      cursor: default;\n      font-size: 14px; }\n    .body .choose_accept-container {\n      position: absolute;\n      bottom: 10px;\n      left: 0;\n      text-align: center;\n      height: 44px;\n      right: 0; }\n    .body .choose_card:hover::after {\n      content: '';\n      position: absolute;\n      top: 0;\n      left: 0;\n      bottom: 0;\n      right: 0;\n      background: rgba(28, 28, 30, 0.8); }\n    .body .choose_card:hover .choose_card-race {\n      visibility: visible;\n      opacity: 1; } }\n  @media screen and (max-width: 720px) {\n    .body .choose_container {\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column; }\n    .body .choose_image {\n      max-width: 50%; }\n    .body .choose_card {\n      width: 100%; }\n    .body .choose_card {\n      padding-bottom: 30px;\n      border-radius: 0; }\n    .body .choose_card:after {\n      content: '';\n      border-bottom: 1px solid #2a2a2a;\n      position: absolute;\n      height: 1px;\n      left: 10px;\n      bottom: 5px;\n      right: 20px; }\n    .body .choose_accept-container,\n    .body .choose_image-container {\n      text-align: center; }\n    .body .choose_accept-container {\n      padding-top: 10px; }\n    .body .choose_history {\n      overflow: hidden; }\n      .body .choose_history p {\n        font-family: Arial, Helvetica, sans-serif; } }\n  @media screen and (max-width: 400px) {\n    .body .choose_history {\n      display: none; }\n    .body .choose_image {\n      max-width: 90%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/race-chooser/race-chooser.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RaceChooserComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api_service__ = __webpack_require__("../../../../../src/app/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RaceChooserComponent = (function () {
    function RaceChooserComponent(api) {
        this.api = api;
    }
    RaceChooserComponent.prototype.ngOnInit = function () {
    };
    RaceChooserComponent.prototype.select = function (num) {
        this.api.chooseRace(num);
    };
    return RaceChooserComponent;
}());
RaceChooserComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'app-race-chooser',
        template: __webpack_require__("../../../../../src/app/race-chooser/race-chooser.component.html"),
        styles: [__webpack_require__("../../../../../src/app/race-chooser/race-chooser.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__api_service__["a" /* ApiService */]) === "function" && _a || Object])
], RaceChooserComponent);

var _a;
//# sourceMappingURL=race-chooser.component.js.map

/***/ }),

/***/ "../../../../../src/app/user-state/user-state.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"body\">\n  <div class=\"avatar\"></div>\n  <div class=\"right\">\n    <div class=\"hp\">\n      <div class=\"wrapper\">\n        <div class=\"line\">\n          <div class=\"filled\"></div>\n        </div>\n\n        <div class=\"num\">10/100</div>\n      </div>\n      <button class=\"plusButton\">+</button>\n\n    </div>\n\n\n    <div class=\"stat\">\n      <div class=\"icon\"></div>\n      <div class=\"wrapper\">\n        <div class=\"line\">\n          <div class=\"filled\"></div>\n        </div>\n\n        <div class=\"num\">10/100</div>\n      </div>\n    </div>\n    <div class=\"stat liked\">\n      <div class=\"icon\"></div>\n      <div class=\"wrapper\">\n        <div class=\"line\">\n          <div class=\"filled\"></div>\n        </div>\n\n        <div class=\"num\">10/100</div>\n      </div>\n    </div>\n    <div class=\"stat\">\n      <div class=\"icon\"></div>\n      <div class=\"wrapper\">\n        <div class=\"line\">\n          <div class=\"filled\"></div>\n        </div>\n\n        <div class=\"num\">10/100</div>\n      </div>\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/user-state/user-state.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".body {\n  position: relative;\n  width: 100%;\n  height: 100%;\n  border: 1px solid black;\n  background-color: rgba(0, 0, 0, 0.8);\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n  .body .avatar {\n    width: 20%;\n    height: 100%;\n    background-image: url(/static/assets/images/titan.png);\n    background-size: contain;\n    background-position: center;\n    background-repeat: no-repeat; }\n  .body .right {\n    -webkit-box-flex: 2;\n        -ms-flex-positive: 2;\n            flex-grow: 2;\n    color: white; }\n    .body .right > div {\n      width: 100%;\n      position: relative;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex; }\n      .body .right > div .wrapper {\n        -webkit-box-flex: 2;\n            -ms-flex-positive: 2;\n                flex-grow: 2; }\n        .body .right > div .wrapper .line {\n          width: 100%;\n          border: 1px solid black;\n          height: 20px;\n          overflow: hidden;\n          position: relative; }\n          .body .right > div .wrapper .line .filled {\n            background-color: green;\n            position: absolute;\n            left: 0;\n            top: 0;\n            height: 100%;\n            width: 30%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/user-state/user-state.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserStateComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserStateComponent = (function () {
    function UserStateComponent() {
    }
    UserStateComponent.prototype.ngOnInit = function () {
    };
    return UserStateComponent;
}());
UserStateComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-user-state',
        template: __webpack_require__("../../../../../src/app/user-state/user-state.component.html"),
        styles: [__webpack_require__("../../../../../src/app/user-state/user-state.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], UserStateComponent);

//# sourceMappingURL=user-state.component.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_19" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map