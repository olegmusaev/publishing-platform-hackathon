# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-15 13:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GotyUser', '0004_gotyuser_battles'),
    ]

    operations = [
        migrations.AddField(
            model_name='gotyuser',
            name='hp_lvl',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='gotyuser',
            name='level',
            field=models.IntegerField(default=0),
        ),
    ]
