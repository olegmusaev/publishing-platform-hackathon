# -*- coding: utf-8 -*-
from django.conf import settings as django_settings


__all__ = (
    'settings',
)

APP_PREFIX = 'GOTY_USER_'

default_settings = {
    'FAKE_LOGIN_ENABLED': True,  # TODO: change to False
}


class Settings:
    def __init__(self):
        for k, v in default_settings.items():
            setattr(self, k, getattr(django_settings, APP_PREFIX + k, v))


settings = Settings()