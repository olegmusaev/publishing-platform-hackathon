# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import GotyUser


class GotyUserAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'first_name', 'last_name',)


admin.site.register(GotyUser, GotyUserAdmin)