# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.conf import settings
from django.contrib.auth.decorators import login_required
from GotyUser import settings as app_settings

from . import views


urlpatterns = [
    url(r'^update/$', views.UpdateUser.as_view(), name='update_user'),
    url(r'^levelup/', login_required(views.LevelupView.as_view()), name='levelup'),
    url(r'^(?P<user_id>\d+)/$', views.get, name='get_user'),
    url(r'^account_info/$', views.account_info, name='account_info'),
]

if settings.DEBUG and app_settings.FAKE_LOGIN_ENABLED:
    urlpatterns += [
        url(r'^fake-login/(?P<user_id>\d+)/$', views.fake_login, name='fake_login'),
    ]
