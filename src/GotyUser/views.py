# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth import login
from django.core.urlresolvers import reverse
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from django.views import View
from django.views.generic import TemplateView

from GotyUser import settings as app_settings
from .models import GotyUser

import json

WINS_PER_LEVEL = 10


class LevelupView(TemplateView):
    template_name = 'page/levelup.html'

    def dispatch(self, *args, **kwargs):
        player = self.request.user

        if player.free_xp_amount == 0:
            return HttpResponseRedirect(reverse('battle:game'))

        return super(LevelupView, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(LevelupView, self).get_context_data(*args, **kwargs)

        player = self.request.user
        context.update(player.get_info())

        return context


class UpdateUser(View):

    def get(self, *args, **kwargs):
        return self.update(self.request.GET)

    def post(self, *args, **kwargs):
        return self.update(self.request.POST)

    def update(self, data):
        race = int(data.get('race', 0))
        delta_stone_lvl = int(data.get('delta_stone_lvl', 0))
        delta_paper_lvl = int(data.get('delta_paper_lvl', 0))
        delta_scissors_lvl = int(data.get('delta_scissors_lvl', 0))
        delta_hp_lvl = int(data.get('delta_hp_lvl', 0))

        player = self.request.user

        if race:
            if player.race != 0:
                return HttpResponseBadRequest()
            player.race = race

        lvl_sum = sum([
            delta_stone_lvl,
            delta_paper_lvl,
            delta_scissors_lvl,
            delta_hp_lvl,
            player.stone_lvl,
            player.paper_lvl,
            player.scissors_lvl,
            player.hp_lvl,
        ])

        if lvl_sum <= player.level * player.XP_POINTS_PER_LEVEL:
            if delta_stone_lvl and player.stone_lvl + delta_stone_lvl <= 10:
                player.stone_lvl += delta_stone_lvl
            if delta_paper_lvl and player.paper_lvl + delta_paper_lvl <= 10:
                player.paper_lvl += delta_paper_lvl
            if delta_scissors_lvl and player.scissors_lvl + delta_scissors_lvl <= 10:
                player.scissors_lvl += delta_scissors_lvl
            if delta_hp_lvl and player.hp_lvl + delta_hp_lvl <= 10:
                player.hp_lvl += delta_hp_lvl

        player.save()

        return JsonResponse({'name': player.first_name,
                             'race': player.race,
                             'scissors_lvl': player.scissors_lvl,
                             'stone_lvl': player.stone_lvl,
                             'paper_lvl': player.paper_lvl,
                             'hp_lvl': player.hp_lvl,
                             'level': player.level,
                             })


def get(request, user_id):
    try:
        user = GotyUser.objects.get(pk=user_id)
    except GotyUser.DoesNotExist:
        return HttpResponseBadRequest()

    info = user.get_info()
    info.update({
        'user_id': user.id,
    })
    info.pop('hp_updated_at')

    return HttpResponse(json.dumps(info))


def account_info(request):
    return get(request, request.user.id)


def fake_login(request, user_id, *args, **kwargs):
    if not app_settings.FAKE_LOGIN_ENABLED or settings.DEBUG is not True:
        return HttpResponse(status=403, content='Fake login is not enabled')

    user, created = GotyUser.objects.get_or_create(id=user_id, defaults={
        'id': user_id,
        'username': 'fake_user_{0}'.format(user_id),
        'first_name': 'fake_user_{0}_first_name'.format(user_id),
        'last_name': 'fake_user_{0}_last_name'.format(user_id),
        'password': 12345
    })

    login(request, user, backend='django.contrib.auth.backends.ModelBackend')

    return HttpResponseRedirect(reverse('battle:game'))
