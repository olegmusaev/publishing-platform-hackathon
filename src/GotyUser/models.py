# -*- coding: utf-8 -*-
from datetime import datetime

from collections import defaultdict

from django.db import models
from django.contrib.auth.models import AbstractUser
from battle.models import BattleRoundChoice


class GotyUser(AbstractUser):
    """
    A fully featured User model with admin-compliant permissions that uses
    a full-length email field as the username.

    Email and password are required. Other fields are optional.
    """
    class RACE:
        TITAN = 1
        SPACE_DOG = 2
        CAT = 3

    DAMAGE_MULTIPLIER = 1.3

    XP_POINTS_PER_LEVEL = 2

    hp = models.IntegerField(default=0)
    hp_updated_at = models.DateTimeField(null=True)
    xp = models.IntegerField(default=0)
    level = models.IntegerField(default=0)
    wins = models.IntegerField(default=0, db_index=True)
    battles = models.IntegerField(default=0)
    hp_lvl = models.IntegerField(default=0)
    stone_lvl = models.IntegerField(default=0)
    paper_lvl = models.IntegerField(default=0)
    scissors_lvl = models.IntegerField(default=0)
    race = models.IntegerField(default=0)

    def add_battle_result(self, is_winner):
        wins_for_new_level = {
            0: 0,
            5: 1,
            15: 2,
            40: 3,
            70: 4,
            120: 5,
            190: 6,
            280: 7,
            400: 8,
            600: 9,
            1000: 10
        }

        self.battles += 1

        if not is_winner:
            self.save()
            return

        self.wins += 1
        if self.wins in wins_for_new_level:
            self.level = wins_for_new_level[self.wins]
        self.save()

    def take_damage(self, damage):
        if damage > self.hp:
            self.hp = 0
        else:
            self.hp -= damage
        self.hp_updated_at = datetime.now()
        self.save()

    def calc_damage(self, choice):
        multiplier = 1
        if self._race_multipliers.get(self.race) == choice:
            multiplier = self.DAMAGE_MULTIPLIER
        return multiplier * self._damage.get(getattr(self, self._choices[choice]))

    def attack(self, enemy, choice):
        damage = self.calc_damage(choice)

        enemy.take_damage(damage)

    @property
    def max_hp(self):
        hp_mapping = {
            0: 500,
            1: 750,
            2: 991,
            3: 1221,
            4: 1435,
            5: 1629,
            6: 1798,
            7: 1937,
            8: 2042,
            9: 2108,
            10: 2131,
        }

        return hp_mapping[self.hp_lvl]

    @property
    def is_dead(self):
        return self.hp <= 0

    @property
    def is_alive(self):
        return self.hp > 0

    @property
    def is_healthy(self):
        return self.hp == self.max_hp

    @property
    def free_xp_amount(self):
        return \
            self.level * self.XP_POINTS_PER_LEVEL\
            - self.scissors_lvl\
            - self.stone_lvl\
            - self.paper_lvl\
            - self.hp_lvl

    _regen = {
        0: 45,
        1: 52,
        2: 52,
        3: 50,
        4: 44,
        5: 38,
        6: 33,
        7: 28,
        8: 24,
        9: 21,
        10: 18,
    }
    
    _damage = {
        0: 100,
        1: 200,
        2: 297,
        3: 389,
        4: 475,
        5: 553,
        6: 621,
        7: 677,
        8: 719,
        9: 746,
        10: 756
    }

    _choices = {
        BattleRoundChoice.CHOICE.ROCK: 'stone_lvl',
        BattleRoundChoice.CHOICE.SCISSORS: 'scissors_lvl',
        BattleRoundChoice.CHOICE.PAPER: 'paper_lvl',
    }

    _race_multipliers = {
        RACE.SPACE_DOG: BattleRoundChoice.CHOICE.ROCK,
        RACE.CAT: BattleRoundChoice.CHOICE.SCISSORS,
        RACE.TITAN: BattleRoundChoice.CHOICE.PAPER,
    }
    
    def get_info(self):
        return {
            'hp': self.hp,
            'hp_updated_at': self.hp_updated_at,
            'xp': self.xp,
            'level': self.level,
            'hp_lvl': self.hp_lvl,
            'stone_lvl': self.stone_lvl,
            'paper_lvl': self.paper_lvl,
            'scissors_lvl': self.scissors_lvl,
            'name': self.username,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'battles': self.battles,
            'wins': self.wins,
            'race': self.race,
            'free_xp_amount': self.free_xp_amount,
        }
