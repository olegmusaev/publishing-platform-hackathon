# -*- coding: utf-8 -*-

import logging
import os

gettext_noop = lambda s: s


#################################################################
##### DJANGO
#################################################################
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SECRET_KEY = '_5da)t&1lkc&!jt1rc9df485a9-3rxyaxs^asb_x-asz&#*v*('
DEBUG = True
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = False
LOG_DIR = os.path.join(BASE_DIR, 'logs')
LOG_LEVEL = logging.DEBUG
LOG_CONSOLE_ENABLED = False
ALLOWED_HOSTS = ['*']
ROOT_URLCONF = 'urls'

SECONDS_TO_STALE_ROUND = 60

SITE_URL = 'http://localhost:8000'
SITE_PATH = '/'

DATABASES = {
    # 'default': {
    #     'ENGINE': 'django.db.backends.sqlite3',
    #     'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    # },
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'goty',
        'USER': 'goty',
        'PASSWORD': os.getenv('DB_PASSWORD', 'goty'),
        'HOST': os.getenv('DB_HOST') or 'database',
    }
}

REDIS_HOST = os.getenv('REDIS_HOST', 'redis')

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://{}:6379/0".format(REDIS_HOST),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'social_django.middleware.SocialAuthExceptionMiddleware',
    'battle.middleware.UserHpMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django_jinja.backend.Jinja2',
        'APP_DIRS': True,
        'OPTIONS': {
            'app_dirname': 'jinja2',
            'match_extension': None,
            'newstyle_gettext': True,
            'extensions': [
                'jinja2.ext.do',
                'jinja2.ext.loopcontrols',
                'jinja2.ext.with_',
                'jinja2.ext.i18n',
                'jinja2.ext.autoescape',
                'django_jinja.builtins.extensions.StaticFilesExtension',
                "django_jinja.builtins.extensions.UrlsExtension",
            ],
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
            ],
        }
    },
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ]
        },
    },
]

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

LANGUAGE_CODE = 'ru'

LANGUAGES = (
    ('ru',    gettext_noop(u'Русский')),
    ('en',    gettext_noop(u'English')),
)

try:
    from settings_local import *
except ImportError:
    pass


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django_jinja',
    'social_django',

    'feed',

    'home',
    'GotyUser',
    'battle',
    'EmailCollector',
    'leaderboard'
]

AUTHENTICATION_BACKENDS = (
    'social_core.backends.facebook.FacebookOAuth2',
    'social_core.backends.odnoklassniki.OdnoklassnikiOAuth2',

    'django.contrib.auth.backends.ModelBackend',
)

LOGIN_URL = 'home'
LOGOUT_URL = 'home'
LOGIN_REDIRECT_URL = '/play'

SOCIAL_AUTH_FACEBOOK_KEY = '175371756363486'  # App ID
SOCIAL_AUTH_FACEBOOK_SECRET = '0ea7ce3435aa1261bcc231826dc10f68'  # App Secret
SOCIAL_AUTH_ODNOKLASSNIKI_OAUTH2_KEY = '1255254528'
SOCIAL_AUTH_ODNOKLASSNIKI_OAUTH2_SECRET = 'B4993A8FD7886467C56BF3CA'
SOCIAL_AUTH_ODNOKLASSNIKI_OAUTH2_PUBLIC_NAME = 'CBALKCNLEBABABABA'


class LevelFilter(logging.Filter):

    def __init__(self, level):
        self.level = level

    def filter(self, record):
        if self.level == record.levelno:
            return 1
        else:
            return 0


LOGGING = {
    'version': 1,
    'formatters': {
        'default': {
            'format': '[%(levelname)s] %(asctime)s %(name)s.%(funcName)s line %(lineno)s: %(message)s',
            'datefmt': '%d/%b/%Y:%H:%M:%S %z',
        },
    },
    'filters': {
        'CriticalLevelFilter': {
            '()': LevelFilter,
            'level': logging.CRITICAL,
        },
        'ErrorLevelFilter': {
            '()': LevelFilter,
            'level': logging.ERROR,
        },
        'WarningLevelFilter': {
            '()': LevelFilter,
            'level': logging.WARNING,
        },
        'InfoLevelFilter': {
            '()': LevelFilter,
            'level': logging.INFO,
        },
        'DebugLevelFilter': {
            '()': LevelFilter,
            'level': logging.DEBUG,
        },
    },
    'handlers': {
        'critical_file_handler': {
            'class': 'logging.FileHandler',
            'level': logging.CRITICAL,
            'formatter': 'default',
            'filename': os.path.join(LOG_DIR, 'error.log'),
            'encoding': 'utf8',
            'filters': ['CriticalLevelFilter']
        },
        'error_file_handler': {
            'class': 'logging.FileHandler',
            'level': logging.ERROR,
            'formatter': 'default',
            'filename': os.path.join(LOG_DIR, 'error.log'),
            'encoding': 'utf8',
            'filters': ['ErrorLevelFilter']
        },
        'warning_file_handler': {
            'class': 'logging.FileHandler',
            'level': logging.WARNING,
            'formatter': 'default',
            'filename': os.path.join(LOG_DIR, 'warning.log'),
            'encoding': 'utf8',
            'filters': ['WarningLevelFilter']
        },
        'info_file_handler': {
            'class': 'logging.FileHandler',
            'level': logging.INFO,
            'formatter': 'default',
            'filename': os.path.join(LOG_DIR, 'info.log'),
            'encoding': 'utf8',
            'filters': ['InfoLevelFilter']
        },
        'debug_file_handler': {
            'class': 'logging.FileHandler',
            'level': logging.DEBUG,
            'formatter': 'default',
            'filename': os.path.join(LOG_DIR, 'debug.log'),
            'encoding': 'utf8',
            'filters': ['DebugLevelFilter']
        },
        'console_handler': {
            'class': 'logging.StreamHandler',
            'level': logging.NOTSET,
            'formatter': 'default',
        },
    },
    'root': {
        'level': LOG_LEVEL,
        'handlers': [
            'critical_file_handler',
            'error_file_handler',
            'warning_file_handler',
            'info_file_handler',
            'debug_file_handler',
        ],
    },
}

AUTH_USER_MODEL = 'GotyUser.GotyUser'

if LOG_CONSOLE_ENABLED:
    LOGGING['root']['handlers'].append('console_handler')
