# -*- coding: utf-8 -*-

from datetime import datetime

from django.db import transaction
from django.http import HttpResponseBadRequest

from GotyUser.models import GotyUser
from battle.models import Battle

import logging
logger = logging.getLogger()


class UserHpMiddleware(object):

    @transaction.atomic
    def process_request(self, request):
        if request.user and not request.user.is_anonymous and not request.user.is_healthy:
            battle = Battle.objects.filter(players=request.user, status=Battle.STATUS.RUNNING)
            if not battle:
                user = GotyUser.objects.filter(id=request.user.id).select_for_update()[0]
                hps = [user.max_hp]
                if user.hp_updated_at is None:
                    user.hp = user.max_hp
                    user.save()
                    return
                current_speed = GotyUser._regen[request.user.level]
                delta = user.hp_updated_at - datetime.now()
                hps.append(user.hp + current_speed * delta.seconds)
                user.hp = min(hps)
                user.hp_updated_at = datetime.now()
                user.save()

    def process_response(self, request, response):
        response['Access-Control-Allow-Origin'] = "localhost"
        return response

    # def process_exception(self, request, exception):
    #     logger.exception(exception)
    #     return HttpResponseBadRequest()
