# -*- coding: utf-8 -*-

import json

from django.db.models import Q
from django.views import View
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse
from django.http import JsonResponse, HttpResponseBadRequest, HttpResponseRedirect

from battle.models import Battle, BattleRound, BattleRoundChoice, Application, BattleNotFound, RoundNotFound


__all__ = (
    'NewApplication',
    'ApplicationStatus',
    'BattleResultView',
    'BattleStatusView',
    'MakeMoveView',
)


class NewApplication(View):

    def get(self, request, *args, **kwargs):

        player = request.user

        if player.battle_set.filter(~Q(status=Battle.STATUS.FINISHED)).exists():
            battle = player.battle_set.filter(~Q(status=Battle.STATUS.FINISHED)).first()
            data = {
                'application_id': battle.application1,
                'status': Application.STATUS.PROCESSED,
                'battle_id': battle.id
            }

            return JsonResponse(data)

        if not player.is_healthy:
            return HttpResponseBadRequest()

        try:
            app = Application(player=player)
            app.save()
        except Exception:
            return HttpResponseBadRequest()

        data = {
            'application_id': app.id,
            'status': app.status,
            'battle_id': app.battle.id if app.battle else None,
        }

        return JsonResponse(data)

    post = get


class ApplicationStatus(View):

    def get(self, request, *args, **kwargs):

        player = request.user

        app_id = kwargs['application_id']

        data = {
            'application_id': app_id,
        }

        try:
            app = Application.objects.get(pk=app_id, player=player)

            data['status'] = app.status
            data['battle_id'] = None

        except Application.DoesNotExist:
            try:
                battle = Battle.objects.get(Q(application1=app_id) | Q(application2=app_id))
                data['battle_id'] = battle.id
                data['status'] = Application.STATUS.PROCESSED
            except Battle.DoesNotExist:
                return HttpResponseBadRequest()

        return JsonResponse(data)


class BattleResultView(TemplateView):
    template_name = 'page/battle_result.html'

    def dispatch(self, *args, **kwargs):
        player = self.request.user
        battle = Battle.objects.get(pk=kwargs.get('battle_id'))

        if not battle.players.filter(pk=player.pk).exists():
            return HttpResponseRedirect(reverse('battle:game'))

        return super(BattleResultView, self).dispatch(*args, **kwargs)

    def get_context_data(self, battle_id, *args, **kwargs):
        context = super(BattleResultView, self).get_context_data(*args, **kwargs)

        player = self.request.user
        battle = Battle.objects.get(pk=battle_id)

        data = {
            'won': (battle.winner == player),
            'has_free_exp': player.free_xp_amount > 0,
        }

        context.update(data)

        return context


class BattleStatusView(View):

    def get(self, request, battle_id):
        """
        :param request:
        :param battle_id:
        :return: {
            'status': 200/404,
            'message': ok/not ok,
            'data': [{
                'round_id': 1,
                'round_created': 123456,
                'round_status': 1,
                'battle_id': 1,
                'battle_status': '',
                'players': [{
                    'id': 1,
                    'move': 1,
                    'hp': 1,
                }, {
                    'id': 2,
                    'move': 1,
                    'hp': 1,
                }],
            }]
        }
        """
        try:
            battle_info = Battle.get_battle_info(battle_id=battle_id)
            data = {
                'status': 200,
                'message': 'ok',
                'data': battle_info,
            }
        except BattleNotFound:
            data = {
                'status': 404,
                'message': 'Battle does not exists',
                'data': {},
            }
        except RoundNotFound:
            data = {
                'status': 404,
                'message': 'Round does not exists',
                'data': {},
            }

        return JsonResponse(data)


class MakeMoveView(View):

    def post(self, request, *args, **kwargs):
        player = request.user
        battle_id = kwargs['battle_id']
        data = json.loads(request.body)
        choice = data.get('choice')

        try:
            battle = Battle.objects.get(id=battle_id)
        except Exception as e:
            return JsonResponse({
                'result': False,
                'message': 'Invalid battle'
            })
        battle_players = battle.players.all()
        if player not in battle_players:
            return JsonResponse({
                'result': False,
                'message': 'It\'s not your battle'
            })

        if battle.status != battle.STATUS.RUNNING:
            return JsonResponse({
                'result': False,
                'message': 'Your battle is not running'
            })

        if choice not in [c for c, v in BattleRoundChoice.CHOICE_LIST]:
            return JsonResponse({
                'result': False,
                'message': 'Invalid choice'
            })

        try:
            active_round = BattleRound.objects.filter(battle_id=battle_id).order_by('-created_at')[0]
            if active_round.status == BattleRound.STATUS.STALE:
                return JsonResponse({
                    'result': False,
                    'message': 'Your round lasts too long'
                })
            if active_round.status != BattleRound.STATUS.RUNNING:
                active_round = None
        except IndexError:
            active_round = None

        if not active_round:
            active_round = BattleRound.objects.create(battle_id=battle_id, status=BattleRound.STATUS.RUNNING)

        try:
            BattleRoundChoice.objects.create(
                battle = battle,
                player = player,
                round = active_round,
                choice = choice
            )
        except:
            return JsonResponse({
                'result': False,
                'message': 'You already made your choice. Get out.'
            })

        made_choices_count = BattleRoundChoice.objects.filter(round=active_round).count()
        if made_choices_count == len(battle_players):
            self.finish_round(active_round)

            if active_round.battle.STATUS != Battle.STATUS.FINISHED:
                BattleRound.objects.create(battle_id=active_round.battle.id, status=BattleRound.STATUS.RUNNING)

        return JsonResponse({
            'result': True,
            'message': 'Your choice was saved'
        })

    def finish_round(self, round):
        force_matrix = {
            (BattleRoundChoice.CHOICE.PAPER, BattleRoundChoice.CHOICE.PAPER): 0,
            (BattleRoundChoice.CHOICE.SCISSORS, BattleRoundChoice.CHOICE.SCISSORS): 0,
            (BattleRoundChoice.CHOICE.ROCK, BattleRoundChoice.CHOICE.ROCK): 0,
            (BattleRoundChoice.CHOICE.PAPER, BattleRoundChoice.CHOICE.ROCK): 1,
            (BattleRoundChoice.CHOICE.ROCK, BattleRoundChoice.CHOICE.SCISSORS): 1,
            (BattleRoundChoice.CHOICE.SCISSORS, BattleRoundChoice.CHOICE.PAPER): 1,
        }
        choices = BattleRoundChoice.objects.filter(round=round)
        if len(choices) != 2:
            return

        choice_first, choice_second = choices
        result = force_matrix.get((choice_first.choice, choice_second.choice), -1)

        winner_choice = None
        looser_choice = None
        if result == 1:
            winner_choice = choice_first
            looser_choice = choice_second
        elif result == -1:
            winner_choice = choice_second
            looser_choice = choice_first

        if winner_choice and looser_choice:
            winner_choice.player.attack(looser_choice.player, winner_choice.choice)

            if looser_choice.player.is_dead:
                self.finish_battle(round.battle, winner_choice.player, looser_choice.player)

        round.status = BattleRound.STATUS.FINISHED
        round.save()

    @staticmethod
    def finish_battle(battle, winner, looser):
        winner.add_battle_result(is_winner=True)
        looser.add_battle_result(is_winner=False)
        battle.status = Battle.STATUS.FINISHED
        battle.winner = winner
        battle.save()


class ApplicationList(View):

    def get(self, request, *args, **kwargs):
        player = request.user

        applications = Application.objects.filter(player=player)

        data = []
        for app in applications:
            data.append({
                'application_id': app.id,
                'status': app.status,
                'battle_id': app.battle.id if app.battle else None,
            })

        return JsonResponse({'applications': data})


class GameView(TemplateView):

    template_name = 'page/game.html'

    def get(self, request, *args, **kwargs):
        player = request.user
        context = {"player": player}

        return self.render_to_response(context)
