# -*- coding: utf-8 -*-
import time
try:
    from itertools import zip_longest
except ImportError:
    from itertools import izip_longest as zip_longest

from django.core.management.base import BaseCommand

from battle.models import Application, Battle, BattleRound


class Command(BaseCommand):
    help = 'Makes battles'

    def handle(self, *args, **options):
        level = 0

        while True:
            applications = Application.objects.filter(status=Application.STATUS.IN_PROCESS,
                                                      player__level__in=[level, level+1, level-1])

            if applications:
                for application1, application2 in self.grouper(applications, 2):
                    if not application1 or not application2:
                        break

                    battle = Battle.objects.create(
                        application1=application1.id,
                        application2=application2.id
                    )

                    battle.players = [application1.player, application2.player]
                    battle.save()
                    BattleRound.objects.create(battle=battle)
                    application1.delete()
                    application2.delete()

                    self.stdout.write(self.style.SUCCESS('Made battle between users "{}" and "{}"'.format(application1.player, application2.player)))
            level += 1

            if level > 10:
                level = 0

            time.sleep(1)

    def grouper(self, iterable, n, fillvalue=None):
        args = [iter(iterable)] * n

        return zip_longest(*args, fillvalue=fillvalue)
