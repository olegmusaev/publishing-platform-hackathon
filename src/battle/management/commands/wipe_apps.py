# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from battle.models import Application


class Command(BaseCommand):
    help = 'Wipe all applications'

    def handle(self, *args, **options):
        Application.objects.all().delete()
