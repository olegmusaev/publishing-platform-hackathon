
import logging
import time

from django.core.management.base import BaseCommand
from battle.models import BattleRound, Battle


logger = logging.getLogger('')

FINISH_HIM = 1000000000
SLEEP_SECONDS = 2


class Command(BaseCommand):
    help = 'Watches battles'

    def handle(self, *args, **options):
        while True:
            try:
                battles = Battle.objects.filter(status=Battle.STATUS.RUNNING)

                if not battles:
                    time.sleep(SLEEP_SECONDS)

                for battle in battles:
                    if battle.is_stale():
                        for player in battle.players.all():
                            player.take_damage(FINISH_HIM)
                            player.add_battle_result(False)

                            battle.status = Battle.STATUS.FINISHED
                            battle.save()
                            logger.info('Closed stale battle {}'.format(battle.id))
            except:
                pass
            time.sleep(SLEEP_SECONDS)
