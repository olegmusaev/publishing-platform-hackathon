
import datetime
import logging
import time

from django.core.management.base import BaseCommand

from battle.models import BattleRound, Battle, BattleRoundChoice
from battle.views import MakeMoveView

ROUND_TIME_LIMIT = datetime.timedelta(seconds=10)
SLEEP_SECONDS = 2

logger = logging.getLogger('')


class Command(BaseCommand):
    help = 'Watches rounds'

    def handle(self, *args, **options):
        while True:
            try:
                rounds = BattleRound.objects.filter(status=BattleRound.STATUS.RUNNING)
                if not rounds:
                    time.sleep(SLEEP_SECONDS)

                for round in rounds:
                    if round.battle.status == Battle.STATUS.FINISHED:
                        round.status = BattleRound.STATUS.FINISHED
                        logger.info('This battle is over {}'.format(round.battle.id))
                        round.save()
                    elif round.created_at + ROUND_TIME_LIMIT < datetime.datetime.now():
                        choices = BattleRoundChoice.objects.filter(round=round)
                        if not choices:
                            round.status = BattleRound.STATUS.STALE
                            round.save()
                            logger.info('Closed stale round {}'.format(round.id))
                            if not round.battle.is_stale():
                                BattleRound.objects.create(battle=round.battle)
                                logger.info('Created new round')
                        else:
                            winner_choice = choices[0]
                            looser = round.battle.get_opponent(winner_choice.player)
                            winner_choice.player.attack(looser, winner_choice.choice)
                            round.status = BattleRound.STATUS.FINISHED
                            logger.info('User {} got damage for being so slooooow'.format(looser.username))
                            round.save()

                            if looser.is_dead:
                                MakeMoveView.finish_battle(round.battle, winner_choice.player, looser)
                            else:
                                BattleRound.objects.create(battle=round.battle)
                                logger.info('Created new round')

            except Exception as e:
                logging.exception('Exception %s', str(e))
                pass
            time.sleep(SLEEP_SECONDS)