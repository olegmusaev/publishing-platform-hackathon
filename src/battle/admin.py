# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from battle.models import (
    Application,
    Battle,
    BattleRound,
    BattleRoundChoice,
)


@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'player',
        'battle',
        'status',
    )


@admin.register(Battle)
class BattleAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'display_players',
        'location',
        'status',
    )

    def display_players(self, obj):
        amount = obj.players.count()
        pattern = '{} vs {}'

        if amount <= 0:
            return _('No players here')

        if amount >= 2:
            return pattern.format(obj.players.first(), obj.players.last())

        return pattern.format(obj.players.first(), None)
    display_players.short_description = _('Players')


@admin.register(BattleRound)
class BattleRoundAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'battle',
        'status',
        'created_at',
    )


@admin.register(BattleRoundChoice)
class BattleRoundChoiceAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'player',
        'round',
        'choice',
        'created_at'
    )
