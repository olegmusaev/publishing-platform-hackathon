# -*- coding: utf-8 -*-
import time

from django.db import models
from django.utils.translation import ugettext_lazy as _


class BattleNotFound(Exception):
    pass


class RoundNotFound(Exception):
    pass


class Battle(models.Model):
    class STATUS:
        INIT = 0
        RUNNING = 1
        FINISHED = 2

    STATUS_LIST = (
        (STATUS.INIT, _('init')),
        (STATUS.RUNNING, _('running')),
        (STATUS.FINISHED, _('finished')),
    )

    players = models.ManyToManyField(to='GotyUser.GotyUser')
    location = models.CharField(null=True, max_length=128)
    status = models.SmallIntegerField(choices=STATUS_LIST, default=STATUS.RUNNING)
    application1 = models.IntegerField()
    application2 = models.IntegerField()
    winner = models.ForeignKey('GotyUser.GotyUser', default=None, blank=True, null=True, related_name='won_battle')

    def __str__(self):
        return str(self.pk)

    def get_opponent(self, user):
        return self.players.all().exclude(id=user.id)[0]

    def is_stale(self):
        last_rounds = BattleRound.objects.filter(battle=self).order_by('-created_at')[:3]
        stale_statuses = [round.status for round in last_rounds if round.status == BattleRound.STATUS.STALE]
        return len(stale_statuses) >= 3

    @staticmethod
    def get_battle_info(battle_id):
        try:
            battle = Battle.objects.get(pk=battle_id)
        except Battle.DoesNotExist:
            raise BattleNotFound()

        rounds = BattleRound.objects.filter(battle=battle_id).order_by('-created_at')

        if not rounds:
            rounds = [BattleRound.objects.create(battle=battle)]

        data = []
        for round in rounds:
            players_choices = []
            for player in battle.players.all():
                players_choices.append({
                    'id': player.pk,
                    'choice': None,
                    'hp':player.hp,
                })

            for round_choice in BattleRoundChoice.objects.filter(battle=battle_id, round=round.pk):
                for players_choice in players_choices:
                    if players_choice['id'] == round_choice.player.pk:
                        players_choice['choice'] = round_choice.choice

            data.append({
                'round_id': round.pk,
                'round_created': time.mktime(round.created_at.timetuple()),
                'round_status': round.status,
                'battle_id': battle.pk,
                'battle_status': battle.status,
                'winner': battle.winner.id if battle.winner else None,
                'players_choices': players_choices,
            })

        return data


class Application(models.Model):
    class STATUS:
        IN_PROCESS = 0
        PROCESSED = 1

    STATUS_LIST = (
        (STATUS.IN_PROCESS, _('in_process')),
        (STATUS.PROCESSED, _('processed')),
    )

    player = models.OneToOneField('GotyUser.GotyUser')
    status = models.SmallIntegerField(choices=STATUS_LIST, default=STATUS.IN_PROCESS)
    battle = models.ForeignKey(Battle, default=None, blank=True, null=True)


class BattleRound(models.Model):
    class STATUS:
        RUNNING = 0
        FINISHED = 1
        STALE = 2

    STATUS_LIST = (
        (STATUS.RUNNING, _('running')),
        (STATUS.FINISHED, _('finished')),
        (STATUS.STALE, _('stale')),
    )

    battle = models.ForeignKey(to=Battle)
    status = models.IntegerField(choices=STATUS_LIST, default=STATUS.RUNNING)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'battle: {}, round: {}'.format(self.battle.pk, self.pk)


class BattleRoundChoice(models.Model):
    class CHOICE:
        ROCK = 0
        PAPER = 1
        SCISSORS = 2

    CHOICE_LIST = (
        (CHOICE.ROCK, _('rock')),
        (CHOICE.PAPER, _('paper')),
        (CHOICE.SCISSORS, _('scissors')),
    )

    battle = models.ForeignKey(to=Battle)
    player = models.ForeignKey(to='GotyUser.GotyUser')
    round = models.ForeignKey(to=BattleRound)
    choice = models.IntegerField(choices=CHOICE_LIST)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (('player', 'round'),)
