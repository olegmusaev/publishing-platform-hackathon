# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from battle.views import BattleStatusView, MakeMoveView, NewApplication, ApplicationStatus, ApplicationList, GameView, BattleResultView


urlpatterns = [
    url(r'^(?P<battle_id>\d+)/status/$', login_required(BattleStatusView.as_view()), name='battle_status'),
    url(r'^(?P<battle_id>\d+)/result/$', login_required(BattleResultView.as_view()), name='battle_result'),
    url(r'^(?P<battle_id>\d+)/move/$', login_required(MakeMoveView.as_view()), name='make_move'),
    url(r'^application/new/$', login_required(NewApplication.as_view()), name='new_application'),
    url(r'^application/(?P<application_id>\d+)/status/$', login_required(ApplicationStatus.as_view()), name='application_status'),
    url(r'^game/', login_required(GameView.as_view()), name='game'),
    url(r'^application/list/$', login_required(ApplicationList.as_view()), name='application_list'),
]
