from django.db import models

from GotyUser.models import GotyUser

class FeedMessage(models.Model):
    player = models.ForeignKey(GotyUser)
    text = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
