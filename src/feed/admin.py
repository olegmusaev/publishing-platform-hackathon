from django.contrib import admin

from feed.models import FeedMessage


@admin.register(FeedMessage)
class FeedMessageAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'player',
        'text',
        'created_at',
    )

