# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_page

from feed.views import PostMessage, LoadFeed


urlpatterns = [
    url(r'^post_message/$', login_required(PostMessage.as_view()), name='post_message'),
    url(r'^load_feed/$', cache_page(2)(LoadFeed.as_view()), name='load_feed'),
]
