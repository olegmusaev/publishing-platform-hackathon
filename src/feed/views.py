# -*- coding: utf-8 -*-

from django.views import View
from django.http import JsonResponse, HttpResponseBadRequest

from feed.models import FeedMessage

__all__ = (
    'PostMessage',
    'LoadFeed',
)


class PostMessage(View):

    def get(self, *args, **kwargs):
        return self.post_message(self.request.GET)

    def post(self, *args, **kwargs):
        return self.post_message(self.request.POST)

    def post_message(self, data):
        player = self.request.user
        text = data.get('message')

        if text:
            feed_message = FeedMessage(player=player, text=text)
            feed_message.save()

        return JsonResponse({})


class LoadFeed(View):

    def get(self, *args, **kwargs):
        count = int(self.request.GET.get('count', 10))
        feed_messages = FeedMessage.objects.order_by('-created_at')[:count]
        messages = []

        for feed_message in feed_messages:
            messages.append({
                'player': feed_message.player.username,
                'text': feed_message.text,
            })

        return JsonResponse({'messages': messages})
