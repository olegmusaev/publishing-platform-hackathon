# -*- coding: utf-8 -*-

from django import forms
from django.db.models import F, Case, When
from django.forms import ValidationError
from django.http import JsonResponse
from django.views import View
from django.views.generic import TemplateView

from GotyUser.models import GotyUser


__all__ = (
    'LeaderboardView',
    'RatingsView',
)


class LeaderboardView(TemplateView):
    template_name = 'page/leaderboard.html'

    def get_queryset(self, **kwargs):
        qs = GotyUser.objects.annotate(
            ratio=Case(
                When(wins=0, then=0),
                default=(F('wins') * 100 / F('battles'))
            )).order_by('-ratio')[:20]

        race_mapping = {
            1: u'Титаны',
            2: u'Псы войны',
            3: u'Котики',
        }

        position = 0
        players = []
        for player in qs.filter(wins__gt=1):
            position += 1
            players.append({
                'position': position,
                'name': u'{0} {1}'.format(player.first_name, player.last_name),
                'xp': player.xp,
                'wins': player.wins,
                'battles': player.battles,
                'level': player.level,
                'ratio': player.ratio,
                'race': race_mapping.get(player.race, ''),
            })
        return players

    def get(self, request, *args, **kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        :return: {
            'status': 200/400,
            'message': ok/not ok,
            'data': [
                {'name': 'first_name last_name', 'xp': 1234, 'wins': 100, 'battles': 666, 'level': 3, 'ratio': 123, 'race': 1},
                {'name': 'first_name last_name', 'xp': 1234, 'wins': 100, 'battles': 666, 'level': 3, 'ratio': 123, 'race': 2}
            ]
        }
        """
        ratings_form = RatingsForm(request.GET)
        if not ratings_form.is_valid():
            return []

        ratings = self.get_queryset(**ratings_form.cleaned_data)
        return self.render_to_response({'ratings': ratings, })


class RatingsForm(forms.Form):
    field = forms.CharField(required=False)
    order_type = forms.CharField(required=False)

    def clean_field(self):
        field = self.cleaned_data['field']
        if field and field not in ['xp', 'wins', 'battles', 'level', 'ratio']:
            raise ValidationError('')

        return field

    def clean_order_type(self):
        order_type = self.cleaned_data['order_type']
        if order_type and order_type not in ['asc', 'desc']:
            raise ValidationError('')

        return order_type


class RatingsView(View):

    def get_queryset(self, **kwargs):
        qs = GotyUser.objects.annotate(
            ratio=Case(
                When(wins=0, then=0),
                default=(F('battles') * 100 / F('wins'))
            )).order_by('-ratio')

        players = []
        for player in qs.all():
            players.append({
                'name': '{0} {1}'.format(player.first_name, player.last_name),
                'xp': player.xp,
                'wins': player.wins,
                'battles': player.battles,
                'level': player.level,
                'ratio': player.ratio,
                'race': player.race,
            })
        return players

    def get(self, request, *args, **kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        :return: {
            'status': 200/400,
            'message': ok/not ok,
            'data': [
                {'name': 'first_name last_name', 'xp': 1234, 'wins': 100, 'battles': 666, 'level': 3, 'ratio': 123, 'race': 1},
                {'name': 'first_name last_name', 'xp': 1234, 'wins': 100, 'battles': 666, 'level': 3, 'ratio': 123, 'race': 2}
            ]
        }
        """
        ratings_form = RatingsForm(request.GET)
        if not ratings_form.is_valid():
            data = {
                'status': '400',
                'message': 'Incorrect arguments',
                'data': [],
            }
            return JsonResponse(data)

        ratings = self.get_queryset(**ratings_form.cleaned_data)

        data = {
            'status': '200',
            'message': ratings_form.errors,
            'data': ratings,
        }

        return JsonResponse(data)
