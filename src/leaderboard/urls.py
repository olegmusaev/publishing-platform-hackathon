# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.views.decorators.cache import cache_page

from leaderboard.views import RatingsView, LeaderboardView


urlpatterns = [
    url(r'^$', cache_page(10)(LeaderboardView.as_view()), name=''),
    url(r'^ratings/$', cache_page(10)(RatingsView.as_view()), name='ratings'),
]
