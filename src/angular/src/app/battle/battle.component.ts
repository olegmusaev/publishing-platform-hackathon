import { DomSanitizer } from '@angular/platform-browser';
import { ApiService } from './../api.service';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Person } from './../../animations';

@Component({
  selector: 'app-battle',
  templateUrl: './battle.component.html',
  styleUrls: ['./battle.component.scss']
})
export class BattleComponent implements OnInit {


  @ViewChild('hero') hero: ElementRef;
  @ViewChild('enemy') enemy: ElementRef;



  playerHistory: string[] = [];
  enemyHistory: string[] = [];

  constructor(public api: ApiService, protected sanitizer: DomSanitizer) { }

  leftPerson: Person;
  rightPerson: Person;

  ngOnInit() {
    if (this.api.race === 1) {
      this.leftPerson = new Person('dog', this.hero.nativeElement);
    } else if (this.api.race === 2) {
      this.leftPerson = new Person('titan', this.hero.nativeElement);
    } else if (this.api.race === 3) {
      this.leftPerson = new Person('cat', this.hero.nativeElement);
    }

    this.api.animationCallback = (isAttack: boolean) => {
      if (isAttack) {
        this.leftPerson.attack();
        if (this.rightPerson) {
          this.rightPerson.gotHit();
        }
      } else {
        this.leftPerson.gotHit();
        if (this.rightPerson) {
          this.rightPerson.attack();
        }
      }
    };

    this.api.raceChoosed = (num: number) => {
      if (num === 1) {
        this.rightPerson = new Person('dog', this.enemy.nativeElement, true);
      } else if (num === 2) {
        this.rightPerson = new Person('titan', this.enemy.nativeElement, true);
      } else if (num === 3) {
        this.rightPerson = new Person('cat', this.enemy.nativeElement, true);
      }
    };
  }


  getHistory(): number[][] {

    const returnArray: number[][] = [[], []];
    // console.log(this.api.currentRounds);
    if (this.api.currentRounds.length !== 0) {
      for (const item of this.api.currentRounds) {
        returnArray[0].push(item.my_move);
        returnArray[1].push(item.enemy_move);

      }

    }
    return returnArray;
  }

  getLastEnemy() {
    //   console.log('last:', this.api.currentRounds[0]);
    if (this.api.currentRounds.length <= 2) {

      return this.sanitizer.bypassSecurityTrustStyle('url(/static/assets/images/battle/qwest.png)');
    } else if (this.api.currentRounds[this.api.currentRounds.length - 2].enemy_move === 0) {
      // url(/static/assets/images/battle/round-lazer.png)
      return this.sanitizer.bypassSecurityTrustStyle('url(/static/assets/images/battle/round-fireball.png)');
    } else if (this.api.currentRounds[this.api.currentRounds.length - 2].enemy_move === 1) {
      return this.sanitizer.bypassSecurityTrustStyle('url(/static/assets/images/battle/round-chain.png)');
    } else if (this.api.currentRounds[this.api.currentRounds.length - 2].enemy_move === 2) {
      return this.sanitizer.bypassSecurityTrustStyle('url(/static/assets/images/battle/round-lazer.png)');
    }

    return this.sanitizer.bypassSecurityTrustStyle('url(/static/assets/images/battle/qwest.png)');

  }

  getBackgroundFromVal(val: number) {

    // камень 0
    // бумага 1
    // ножницы 2
    if (val === 0) {
      return this.sanitizer.bypassSecurityTrustStyle('url(/static/assets/images/battle/fireball.png)');
    } else if (val === 1) {
      return this.sanitizer.bypassSecurityTrustStyle('url(/static/assets/images/battle/chain.png)');
    } else if (val === 2) {
      return this.sanitizer.bypassSecurityTrustStyle('url(/static/assets/images/battle/lazer.png)');
    } else {
      return '';
    }
  }




  getMyHealth(): number {
    if (this.api.currentRounds && this.api.currentRounds.length !== 0) {
      return this.api.currentRounds[this.api.currentRounds.length - 1].my_hp;
    } else {
      return 1;
    }

  }
  getEnemyHealth(): number {
    if (this.api.currentRounds && this.api.currentRounds.length !== 0) {
      return this.api.currentRounds[this.api.currentRounds.length - 1].enemy_hp;
    } else {
      return 1;
    }

  }

  getMyPercent(): string {

    return Math.floor(this.getMyHealth() / this.api.myMaxHp * 100) + '%';

  }

  getEnemyPercent(): string {
    return Math.floor(this.getEnemyHealth() / this.api.enemyMaxHp * 100) + '%';


  }


  move(id: number): void {
    this.api.makeMove(id);
  }


  getMyPaper(): number {

    // console.log('get my paper:', this.api.paper_lvl);

    return this.api.paper_lvl;
    //  this.stone_lvl = data.stone_lvl;
    //  this.paper_lvl = data.paper_lvl;
    //  this.scissors_lvl = data.scissors_lvl;

    //   return this.api.paper
  }

  getMyStone(): number {
    return this.api.stone_lvl;
  }

  getMyScissors(): number {
    return this.api.scissors_lvl;
  }


  getEnemyPaper(): number {
    return this.api.enemy.paper_lvl;
  }

  getEnemyStone(): number {
    return this.api.enemy.stone_lvl;
  }

  getEnemyScissors(): number {
    return this.api.enemy.scissors_lvl;
  }




}
