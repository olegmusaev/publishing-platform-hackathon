import { ApiService } from './../api.service';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit {

  constructor(protected sanitizer: DomSanitizer, public api: ApiService) { }

  ngOnInit() {
  }

  getBg() {
    const img = '/assets/images/bg/' + this.api.currentLevel + '.png';
    const style = `background-image: url(${img})`;

    const sanitized = this.sanitizer.bypassSecurityTrustStyle(style);

    // console.log(sanitized)
    return sanitized;
    // this.api.currentLevel
  }

}
