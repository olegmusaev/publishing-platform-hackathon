import { DomSanitizer } from '@angular/platform-browser';
import { ApiService } from './../api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-state',
  templateUrl: './user-state.component.html',
  styleUrls: ['./user-state.component.scss']
})
export class UserStateComponent implements OnInit {

  constructor(protected sanitize: DomSanitizer, public api: ApiService) { }

  ngOnInit() {
  }


  getRaceImage() {

    let san;

    // console.log(this.api.race);
    if (this.api.race === 1) {
      san = this.sanitize.bypassSecurityTrustResourceUrl('/static/assets/hero/race-fox.png');
    } else if (this.api.race === 2) {
      san = this.sanitize.bypassSecurityTrustResourceUrl('/static/assets/hero/race-robot.png');
    } else if (this.api.race === 3) {
      san = this.sanitize.bypassSecurityTrustResourceUrl('/static/assets/hero/race-cat.png');
    }

    // console.log(san);
    return san;
  }

  getMyPaper(): number {

  //  console.log("get my paper:", this.api.paper_lvl)

    return this.api.paper_lvl;
    //  this.stone_lvl = data.stone_lvl;
    //  this.paper_lvl = data.paper_lvl;
    //  this.scissors_lvl = data.scissors_lvl;

    //   return this.api.paper
  }

  getMyStone(): number {
    return this.api.stone_lvl;
  }

  getMyScissors(): number {
    return this.api.scissors_lvl;
  }

  getMyHealth(): number {
    return this.api.hp;
  }

  getMyWins(): number {
    return this.api.wins;
  }

  addExp(): void {

  }

  plus(num: number): void {
    this.api.updateSkill(num);
  }

}
