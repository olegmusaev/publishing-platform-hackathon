import { ApiService } from './api.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { RaceChooserComponent } from './race-chooser/race-chooser.component';
import { LobbyComponent } from './lobby/lobby.component';
import { BattleComponent } from './battle/battle.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ChatComponent } from './chat/chat.component';
import { UserStateComponent } from './user-state/user-state.component';
import { AwaitPageComponent } from './await-page/await-page.component';
import { WinComponent } from './win/win.component';
import { LooseComponent } from './loose/loose.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    RaceChooserComponent,
    LobbyComponent,
    BattleComponent,
    ChatComponent,
    UserStateComponent,
    AwaitPageComponent,
    WinComponent,
    LooseComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
