import { ApiService } from './../api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {


  currentState = '';

  constructor(public api: ApiService) {

  }

  ngOnInit() {
  }

  battle(): void {
    this.api.toBattle();
  }

}
