import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwaitPageComponent } from './await-page.component';

describe('AwaitPageComponent', () => {
  let component: AwaitPageComponent;
  let fixture: ComponentFixture<AwaitPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AwaitPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwaitPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
