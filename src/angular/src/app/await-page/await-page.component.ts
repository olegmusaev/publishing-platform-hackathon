import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-await-page',
  templateUrl: './await-page.component.html',
  styleUrls: ['./await-page.component.scss']
})
export class AwaitPageComponent implements OnInit {

  dots = '...';
  num = 0;
  constructor() { }

  ngOnInit() {
    setInterval(() => {
      if (this.num % 3 === 0) {
        this.dots = '.';
      } else if (this.num % 3 === 1) {
        this.dots = '..';
      } else if (this.num % 3 === 2) {
        this.dots = '...';
      }
      this.num++;
    }, 500);
  }

}
