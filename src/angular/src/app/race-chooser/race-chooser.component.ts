import { ApiService } from './../api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-race-chooser',
  templateUrl: './race-chooser.component.html',
  styleUrls: ['./race-chooser.component.scss']
})
export class RaceChooserComponent implements OnInit {

  constructor(protected api: ApiService) { }

  ngOnInit() {
  }


  select(num: number): void {
    this.api.chooseRace(num);
  }

}
