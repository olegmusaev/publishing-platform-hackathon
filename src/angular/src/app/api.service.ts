import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApiService {

  apilink = 'http://goty.io/';
  apiending = '';

  currentState = '';

  currentBattleStatus = '';

  user: any;

  myId = 0;

  timer;


  applicationId = 0;

  battleId = 0;


  timeForRound = 10000;


  public currentRounds: any[] = [];



  currentLevel = 0;

  hp = 0;

  wins = 0;
  battles = 0;
  hp_lvl = 0;
  scissors_lvl = 0;
  paper_lvl = 0;
  stone_lvl = 0;
  race = 0;

  level = 0;


  free_xp = 0;

  userName = '';


  enemyGet = false;




  enemyMaxHp = 0;
  myMaxHp = 0;

  currentRound = 0;

  public moveMaked = false;



  public roundStartDate: Date;



  public animationCallback: (boolean) => void;


  battleTimer;

  needWins: number[] = [
    5,
    10,
    25,
    30,
    50,
    70,
    90,
    120,
    200,
    400
  ];



  availablePoints = 0;


  raceChoosed: (number) => void;

  enemy: any = {};

  constructor(protected http: HttpClient) {

    /*  this.http.get(this.apilink + "user/fake-login/666", { withCredentials: true }).subscribe((data) => {

    })*/

    // this.currentState = "battle";
    this.getMe();

    setInterval(() => {

      if (this.currentState !== 'await' && this.currentState !== 'battle') {
        this.getMe();

      }


    }, 1000);
  }

  public getMe(): void {
    // console.log("get me:");
    this.http.get(this.apilink + 'user/account_info/', { withCredentials: true }).subscribe((data: any) => {
      if (this.currentState !== 'await') {
        this.user = data;
        // console.log("user:", this.user);

        this.userName = data['first_name'] + ' ' + data['last_name'];
        this.myId = data['user_id'];
        /*
  last_name	"fake_user_666_last_name"
  hp	500
  hp_updated_at	null
  xp	0
  name	"fake_user_666"
  first_name	"fake_user_666_first_name"
  stone_lvl	0
  free_xp_amount	0
  level	0
  wins	0
  battles	0
  hp_lvl	0
  paper_lvl	0
  race	2
  scissors_lvl	0
        */

        this.wins = data.wins;

        this.hp = data.hp;

        this.level = data.level;

        this.availablePoints = data.free_xp_amount;

        // this.availablePoints = 10;
        this.battles = data.battles;
        this.stone_lvl = data.stone_lvl;
        this.paper_lvl = data.paper_lvl;
        //  console.log('set paper level:', this.paper_lvl);
        this.scissors_lvl = data.scissors_lvl;

        this.free_xp = data.free_xp_amount;

        //  this.free_xp = 10;

        this.race = this.user.race;
        if (this.user.race === 0) {
          //  console.log('change to race chooser');
          this.currentState = 'race-chooser';
        } else {
          if (this.currentState !== 'battle') {
            //   console.log('change to lobby', this.currentState);
            this.currentState = 'lobby';
          }
        }
        this.currentLevel = data.level;
      }
    });

  }

  public chooseRace(num: number): void {
    // this.currentState = "lobby";
    this.http.get(this.apilink + 'user/update/?race=' + num, { withCredentials: true }).subscribe((data) => {
      // console.log('change to lobby');
      this.currentState = 'lobby';
    });
  }


  public updateSkill(num: number): void {
    let link: string = this.apilink + 'user/update/';

    if (num === 0) {
      link += '?delta_paper_lvl=1';
    } else if (num === 1) {
      link += '?delta_scissors_lvl=1';
    } else if (num === 2) {
      link += '?delta_stone_lvl=1';
    } else {
      link += '?delta_hp_lvl=1';
    }
    this.http.get(link, { withCredentials: true }).subscribe((data) => {
      // console.log('change to lobby');
      this.currentState = 'lobby';
    });
  }

  public toBattle(): void {
    this.currentState = 'await';
    // console.log('change to await');
    this.http.post(this.apilink + 'battle/application/new/' + this.apiending, {}, { withCredentials: true }).subscribe((data) => {
      this.applicationId = data['application_id'];
      this.startChecking();
    }, (err) => {
      // console.log('change to await');
      this.currentState = 'await';

      this.getApplicationsList();
    });
  }


  getApplicationsList(): void {
    this.http.get(this.apilink + 'battle/application/list/', { withCredentials: true }).subscribe((data) => {
      //  console.log('get applicationslist:', data);
      if (data['applications'].length !== 0) {
        this.applicationId = data['applications'][0].application_id;
        this.startChecking();
      } else {
        setTimeout(() => {
          this.getApplicationsList();
        }, 500);
      }
    });
  }

  startChecking(): void {
    this.timer = setInterval(() => {
      if (this.currentState === 'await') {
        this.http.get(this.apilink + 'battle/application/' + this.applicationId + '/status/' + this.apiending, { withCredentials: true }).subscribe((data: any) => {
          if (this.currentState === 'await') {
            if (data.battle_id != null) {
              clearInterval(this.timer);
              this.battleId = data['battle_id'];
              this.currentState = 'battle';

              this.updateBattleStatus();
            } else {
              //  console.log('not exist');
            }
          }
        });
      }

    }, 1000);
  }



  updateBattleStatus(): void {
    this.http.get(this.apilink + 'battle/' + this.battleId + '/status/', { withCredentials: true }).subscribe((data: any) => {
      if (this.currentState === 'battle') {
        //  console.log('battle status:', data);
        //  console.log(this.currentRound, data.data.length);
        clearInterval(this.battleTimer);
        if (this.timer) {
          clearInterval(this.timer);
        }
        if (this.currentRound !== data.data.length) {

          this.currentRounds = [];

          this.roundStartDate = new Date();


          for (const item of data.data) {
            this.currentRounds.push(this.parseRound(item));
          }

          this.currentRounds = this.currentRounds.reverse();

          //  console.log(this.currentRounds);

          const lastRound = this.currentRounds[this.currentRounds.length - 1];

          // камень 0
          // бумага 1
          // ножницы 2

          // console.log(lastRound, lastRound.my_move, lastRound.enemy_move);

          if (!lastRound.my_move || lastRound.enemy_move) {

            if (this.animationCallback) {
              this.animationCallback(true);
            }
          }
          if (lastRound.my_move === 0) {
            if (lastRound.enemy_move === 1) {
              if (this.animationCallback) {


                this.animationCallback(false);
              }
            } else {
              if (this.animationCallback) {
                this.animationCallback(true);
              }
            }
          } else if (lastRound.my_move === 1) {
            if (lastRound.enemy_move === 0) {
              if (this.animationCallback) {
                this.animationCallback(true);
              }

            } else {
              if (this.animationCallback) {
                this.animationCallback(false);
              }
            }
          } else if (lastRound.my_move === 2) {

            if (lastRound.enemy_move === 1) {

              if (this.animationCallback) {
                this.animationCallback(true);
              }

            } else {
              if (this.animationCallback) {
                this.animationCallback(false);
              }
            }
          }


          if (!this.enemyGet) {

            this.getEnemy(this.currentRounds[0].enemy_id);

            this.myMaxHp = this.currentRounds[0].my_hp;
            this.enemyMaxHp = this.currentRounds[0].enemy_hp;

            this.enemyGet = true;
          }





          if (this.currentRounds.length > this.currentRound) {
            this.moveMaked = false;
            this.currentRound = this.currentRounds.length;
          }

          this.battleTimer = setInterval(() => {
            this.updateBattleStatus();
          }, 1000);

        } else {
          //  console.log('check winner:', this.currentRounds[this.currentRounds.length - 1], this.currentRounds[this.currentRounds.length - 1].winner, this.myId, this.currentRounds[0].enemy_id);
          if (this.currentRounds[this.currentRounds.length - 1].winner === this.myId) {
            this.win();
          } else if (this.currentRounds[this.currentRounds.length - 1].winner === this.currentRounds[0].enemy_id) {
            this.lose();
          } else {
            this.battleTimer = setInterval(() => {
              this.updateBattleStatus();
            }, 1000);
          }
        }
      }
    });
  }


  public getSeconds(): number {
    if (!this.roundStartDate) {
      return 10;
    }
    const date: Date = new Date();

    let seconds: number = 10 - Math.floor((date.getTime() - this.roundStartDate.getTime()) / 1000);
    seconds = Math.max(seconds, 0);

    return seconds;

  }


  lose(): void {
    this.currentState = 'lose';

    //  console.log('LOSE');

  }

  win(): void {
    this.currentState = 'win';
    // console.log('win');
  }

  parseBattleStatus(data): void {

  }

  isBattleAvailable(): boolean {
    if (this.currentState === 'lobby') {
      return true;
    } else {
      return false;
    }
  }


  public makeMove(num: number): void {
    if (!this.moveMaked) {
      this.moveMaked = true;
      this.http.post(this.apilink + 'battle/' + this.battleId + '/move/', { choice: num }, { withCredentials: true }).subscribe((data: any) => {
        if (data.result === true) {
          this.updateBattleStatus();
        } else if (data.result === false) {
          this.updateBattleStatus();
        }
      });
    }
  }

  parseRound(data): any {

    const obj: any = {};
    if (data['players_choices'].length !== 0) {
      obj.round_id = data['round_id'];
      obj.round_created = data['round_created'];
      obj.round_status = data['round_status'];

      // 0,1,2
      // running, finished, stale

      console.log('parse round:', data, this.myId);

      let me = 0;
      let enemy = 0;


      if (data['players_choices'][0]['id'] === this.myId) {
        me = 0;
        enemy = 1;
      } else {
        me = 1;
        enemy = 0;
      }

      obj.battle_id = data['battle_id'];
      obj.battle_status = data['battle_status'];

      // 0,1,2
      // init, running, finished

      obj.winner = data['winner'];

      obj.my_move = data['players_choices'][me]['choice'];
      obj.my_hp = data['players_choices'][me]['hp'];

      obj.enemy_id = data['players_choices'][enemy]['id'];
      obj.enemy_move = data['players_choices'][enemy]['choice'];
      obj.enemy_hp = data['players_choices'][enemy]['hp'];


      return obj;
    } else {

    }
  }



  public getEnemy(id: number): void {
    this.http.get(this.apilink + '/user/' + id).subscribe((data) => {

      this.enemy = {};

      this.enemy.nickname = data['first_name'] + ' ' + data['last_name'];

      this.enemy.stone_lvl = data['stone_lvl'];
      this.enemy.paper_lvl = data['paper_lvl'];
      this.enemy.scissors_lvl = data['scissors_lvl'];
      this.enemy.race = data['race'];

      if (this.raceChoosed) {
        this.raceChoosed(this.enemy.race);

      }


    });
  }

  public getMyHealth(): number {
    if (this.currentRounds) {
      return this.currentRounds[this.currentRounds.length - 1].my_hp;
    }
  }

  public getEnemyHealth(): number {
    if (this.currentRounds) {
      return this.currentRounds[this.currentRounds.length - 1].enemy_hp;
    }
  }

}
