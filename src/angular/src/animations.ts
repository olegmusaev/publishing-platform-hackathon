declare var exportRoot;
declare var createjs: any;
export class Person {

	canvas
	anim_container
	dom_overlay_container

	exportRoot;
	stage;

	lib;

	AdobeAn: any = {};

	constructor(private type: string, element: HTMLElement, reverse: boolean = false) {
		element.innerHTML = `
		<div>
			<canvas id="canvas" width="355" height="370"></canvas>
			<div>
			</div>
		</div>
		`

		this.canvas = element.getElementsByTagName("canvas").item(0);

		if (reverse) {
			this.canvas.style["transform-origin"] = "center";
			this.canvas.style["transform"] = "scaleX(-1)";
		}


		this.anim_container = element;
		this.dom_overlay_container = element.getElementsByClassName("div").item(0);

		let comp

		console.log(type, element);

		if (type == "dog") {
			this.initDogRoutine(createjs, this.AdobeAn);
			comp = this.AdobeAn.getComposition("450F9CB287028449A3566E8BC8BF4A4B");
			console.log(comp.getSpriteSheet());
		} else if (type == "titan") {
			this.initTitanRoutine(createjs, this.AdobeAn);
			comp = this.AdobeAn.getComposition("25788DBD55E1BB4BA3DF07A2FFDA3033");

		} else {
			this.initCatRoutine(createjs, this.AdobeAn);
			comp = this.AdobeAn.getComposition("9F777150AFD2374D92BF3748B611C26C");
		}
		this.lib = comp.getLibrary();
		let loader = new createjs.LoadQueue(false);
		loader.addEventListener("fileload", (evt) => { this.handleFileLoad(evt, comp) });
		loader.addEventListener("complete", (evt) => { this.handleComplete(evt, comp) });
		this.lib = comp.getLibrary();
		loader.loadManifest(this.lib.properties.manifest);


	}


	public attack(): void {
		this.stage.children[0].children[0].gotoAndPlay("attack")
	}

	public gotHit(): void {
		this.stage.children[0].children[0].gotoAndPlay("hit")
	}

	handleFileLoad(evt, comp) {
		var images = comp.getImages();
		if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
	}
	handleComplete(evt, comp) {
		//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
		this.lib = comp.getLibrary();
		var ss = comp.getSpriteSheet();
		var queue = evt.target;
		var ssMetadata = this.lib.ssMetadata;
		for (let i = 0; i < ssMetadata.length; i++) {
			ss[ssMetadata[i].name] = new createjs.SpriteSheet({ "images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames })
		}
		if (this.type == "dog") {
			this.exportRoot = new this.lib.dog_export();
		} else if (this.type == "titan") {
			this.exportRoot = new this.lib.titan_export();
		} else if (this.type == "cat") {
			this.exportRoot = new this.lib.kitty_export();
		}
		this.stage = new this.lib.Stage(this.canvas);
		this.stage.addChild(this.exportRoot);
		//Registers the "tick" event listener.

		//Code to support hidpi screens and responsive scaling.
		/*function makeResponsive(isResp, respDim, isScale, scaleType) {
			var lastW, lastH, lastS = 1;
			window.addEventListener('resize', resizeCanvas);
			resizeCanvas();
			function resizeCanvas() {
				var w = lib.properties.width, h = lib.properties.height;
				var iw = window.innerWidth, ih = window.innerHeight;
				var pRatio = window.devicePixelRatio || 1, xRatio = iw / w, yRatio = ih / h, sRatio = 1;
				if (isResp) {
					if ((respDim == 'width' && lastW == iw) || (respDim == 'height' && lastH == ih)) {
						sRatio = lastS;
					}
					else if (!isScale) {
						if (iw < w || ih < h)
							sRatio = Math.min(xRatio, yRatio);
					}
					else if (scaleType == 1) {
						sRatio = Math.min(xRatio, yRatio);
					}
					else if (scaleType == 2) {
						sRatio = Math.max(xRatio, yRatio);
					}
				}
				this.canvas.width = w * pRatio * sRatio;
				this.canvas.height = h * pRatio * sRatio;
				this.canvas.style.width = this.dom_overlay_container.style.width = this.anim_container.style.width = w * sRatio + 'px';
				this.canvas.style.height = this.anim_container.style.height = this.dom_overlay_container.style.height = h * sRatio + 'px';
				this.stage.scaleX = pRatio * sRatio;
				this.stage.scaleY = pRatio * sRatio;
				lastW = iw; lastH = ih; lastS = sRatio;
			}
		}*/
		//makeResponsive(false, 'both', false, 1);
		this.AdobeAn.compositionLoaded(this.lib.properties.id);
		this.fnStartAnimation();
	}

	fnStartAnimation() {
		createjs.Ticker.setFPS(this.lib.properties.fps);
		createjs.Ticker.addEventListener("tick", this.stage);
	}


	initDogRoutine(cjs, an): void {


		var p; // shortcut to reference prototypes
		var lib: any = {}; var ss = {}; var img = {};
		lib.ssMetadata = [
			{ name: "dog_export_atlas_", frames: [[267, 428, 80, 77], [0, 0, 387, 255], [0, 257, 387, 169], [0, 428, 177, 131], [0, 599, 76, 71], [444, 504, 64, 71], [186, 631, 116, 30], [186, 599, 129, 30], [179, 513, 108, 38], [78, 599, 106, 38], [78, 652, 79, 11], [78, 639, 80, 11], [0, 561, 362, 36], [389, 321, 53, 174], [444, 321, 50, 181], [179, 428, 86, 83], [364, 497, 78, 80], [364, 579, 78, 78], [389, 161, 78, 158], [389, 0, 86, 159]] }
		];

		// symbols:



		(lib.Bitmap1 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(0);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap10 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(1);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap11 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(2);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap12 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(3);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap13 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(4);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap14 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(5);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap15 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(6);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap16 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(7);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap17 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(8);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap18 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(9);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap19 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(10);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap20 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(11);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap21 = function () {
			this.spriteSheet = ss['dog_export_atlas_'];
			this.gotoAndStop(12);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap22 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(13);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap23 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(14);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap3 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(15);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap4 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(16);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap5 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(17);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap8 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(18);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap9 = function () {
			this.spriteSheet = ss["dog_export_atlas_"];
			this.gotoAndStop(19);
		}).prototype = p = new cjs.Sprite();
		// helper functions:

		function mc_symbol_clone() {
			let clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
			clone.gotoAndStop(this.currentFrame);
			clone.paused = this.paused;
			clone.framerate = this.framerate;
			return clone;
		}

		function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
			let prototype = cjs.extend(symbol, cjs.MovieClip);
			prototype.clone = mc_symbol_clone;
			prototype.nominalBounds = nominalBounds;
			prototype.frameBounds = frameBounds;
			return prototype;
		}


		(lib.Symbol24 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.shape = new cjs.Shape();
			this.shape.graphics.f().s('#000000').ss(5, 1, 1).p('AFRgGIA8AwQAAAAAAAAAFRgGIguA0AF+g7IgDADIgqAyAEegvIAzApAlUAAIAqAzIAIAJAkZg1IgHAHIg0AuIg4AwAmBg3IAtA3');
			this.shape.setTransform(41.9, 6);

			this.shape_1 = new cjs.Shape();
			this.shape_1.graphics.f('#FFFFFF').s().p('AmSAvQgTgUgBgcQgCgcAFgTQADgMAagEQAOgDAVAAQAwAAAOAMQADACACAEQAIASgEAdQgEAZgPATIgqgzIA0guIg0AuIgtg3IAtA3Ig4AwIA4gwIAqAzIgFAFQgTAVgcAAQgcAAgTgVgAD/AkQgcgbAAgUQAAgWAfgPQALgFAMgDQAZgIAigCQAUgBAOACQAXADAKALQAQASAAAWQAAAUgdAWIgCACIAAAAIg8gwIAqgyIgqAyIA8AwQgdATgnAFIgNABQggAAgYgWgAEeAlIAug0IgzgpIAzApgAkvAqgAlZgJgAFMgPg');
			this.shape_1.setTransform(42.4, 6.9);

			this.timeline.addTween(cjs.Tween.get({}).to({ state: [{ t: this.shape_1 }, { t: this.shape }] }).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol24, new cjs.Rectangle(-0.3, -2.5, 85, 17.1), null);


		(lib.Symbol23 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap3();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol23, new cjs.Rectangle(0, 0, 86, 83), null);


		(lib.Symbol22 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap1();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol22, new cjs.Rectangle(0, 0, 80, 77), null);


		(lib.Symbol21 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap5();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol21, new cjs.Rectangle(0, 0, 78, 78), null);


		(lib.Symbol20 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap4();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol20, new cjs.Rectangle(0, 0, 78, 80), null);


		(lib.Symbol19 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap11();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol19, new cjs.Rectangle(0, 0, 387, 169), null);


		(lib.Symbol18 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap9();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol18, new cjs.Rectangle(0, 0, 86, 159), null);


		(lib.Symbol17 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap8();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol17, new cjs.Rectangle(0, 0, 78, 158), null);


		(lib.Symbol16 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap23();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol16, new cjs.Rectangle(0, 0, 50, 181), null);


		(lib.Symbol15 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap22();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol15, new cjs.Rectangle(0, 0, 53, 174), null);


		(lib.Symbol14 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap10();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(0, 0, 387, 255), null);


		(lib.Symbol11 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap14();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0, 0, 64, 71), null);


		(lib.Symbol10 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap13();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0, 0, 76, 71), null);


		(lib.Symbol9 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap12();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0, 0, 177, 131), null);


		(lib.Symbol8 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap21();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(0, 0, 362, 36), null);


		(lib.Symbol7 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap17();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0, 0, 108, 38), null);


		(lib.Symbol6 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap18();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0, 0, 106, 38), null);


		(lib.Symbol5 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap19();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0, 0, 79, 11), null);


		(lib.Symbol4 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap20();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0, 0, 80, 11), null);


		(lib.Symbol3 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap15();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0, 0, 116, 30), null);


		(lib.Symbol2 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap16();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0, 0, 129, 30), null);


		(lib.Symbol1 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, { idle: 0, attack: 59, hit: 90 });

			// timeline functions:
			this.frame_58 = function () {
				this.gotoAndPlay(0);
			}
			this.frame_89 = function () {
				this.gotoAndPlay(0);
			}

			// actions tween:
			this.timeline.addTween(cjs.Tween.get(this).wait(58).call(this.frame_58).wait(31).call(this.frame_89).wait(49));

			// Layer 22
			this.instance = new lib.Symbol24();
			this.instance.parent = this;
			this.instance.setTransform(191.7, 110.1, 1, 1, 0, 0, 0, 42.2, 6);
			this.instance.alpha = 0;
			this.instance._off = true;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(90).to({ _off: false }, 0).to({ y: 105.1 }, 4, cjs.Ease.get(1)).wait(2).to({ alpha: 0.002 }, 0).wait(1).to({ alpha: 0.005 }, 0).wait(1).to({ y: 105.2, alpha: 0.01 }, 0).wait(1).to({ alpha: 0.018 }, 0).wait(1).to({ y: 105.3, alpha: 0.028 }, 0).wait(1).to({ y: 105.5, alpha: 0.042 }, 0).wait(1).to({ y: 105.6, alpha: 0.059 }, 0).wait(1).to({ y: 105.8, alpha: 0.08 }, 0).wait(1).to({ y: 106.1, alpha: 0.107 }, 0).wait(1).to({ y: 106.4, alpha: 0.14 }, 0).wait(1).to({ y: 106.8, alpha: 0.18 }, 0).wait(1).to({ y: 107.3, alpha: 0.23 }, 0).wait(1).to({ y: 107.9, alpha: 0.294 }, 0).wait(1).to({ y: 108.8, alpha: 0.382 }, 0).wait(1).to({ y: 110.1, alpha: 0.518 }, 0).wait(1).to({ y: 112.2, alpha: 0.726 }, 0).wait(1).to({ y: 114.9, alpha: 1 }, 0).wait(18).to({ y: 110.1, alpha: 0 }, 7).wait(1));

			// Layer 9
			this.instance_1 = new lib.Symbol4();
			this.instance_1.parent = this;
			this.instance_1.setTransform(140.7, 94.5, 1, 1, 0, 0, 0, 40.1, 5.6);

			this.timeline.addTween(cjs.Tween.get(this.instance_1).to({ y: 91.7 }, 26, cjs.Ease.get(-1)).to({ y: 94.5 }, 27, cjs.Ease.get(1)).wait(6).to({ regY: 5.7, rotation: 19.5, x: 159.8, y: 108.9 }, 3).to({ y: 36.8 }, 7, cjs.Ease.get(-1)).to({ x: 156.6, y: 98.4 }, 5).to({ y: 88.8 }, 2).wait(7).to({ regY: 5.6, rotation: 0, x: 140.7, y: 94.5 }, 6, cjs.Ease.get(-1)).wait(1).to({ y: 89.5 }, 4, cjs.Ease.get(1)).to({ rotation: -11.5, x: 140.6, y: 95.8 }, 18, cjs.Ease.get(-1)).wait(18).to({ rotation: 0, x: 140.7, y: 94.5 }, 7).wait(1));

			// Layer 10
			this.instance_2 = new lib.Symbol5();
			this.instance_2.parent = this;
			this.instance_2.setTransform(240.7, 94.3, 1, 1, 0, 0, 0, 39.5, 5.4);

			this.timeline.addTween(cjs.Tween.get(this.instance_2).to({ y: 91.5 }, 26, cjs.Ease.get(-1)).to({ y: 94.3 }, 27, cjs.Ease.get(1)).wait(6).to({ rotation: -17.2, x: 223.3, y: 108.5 }, 3).to({ y: 36.4 }, 7, cjs.Ease.get(-1)).to({ y: 98 }, 5).to({ y: 88.4 }, 2).wait(7).to({ rotation: 0, x: 240.7, y: 94.3 }, 6, cjs.Ease.get(-1)).wait(1).to({ y: 89.3 }, 4, cjs.Ease.get(1)).to({ regX: 39.6, regY: 5.5, rotation: 11.7, x: 242, y: 94.5 }, 18, cjs.Ease.get(-1)).wait(18).to({ regX: 39.5, regY: 5.4, rotation: 0, x: 240.7, y: 94.3 }, 7).wait(1));

			// Layer 17
			this.instance_3 = new lib.Symbol6();
			this.instance_3.parent = this;
			this.instance_3.setTransform(209.9, 105.2, 1, 1, 0, 0, 0, 10.1, 16.3);

			this.timeline.addTween(cjs.Tween.get(this.instance_3).to({ y: 102.8 }, 26, cjs.Ease.get(-1)).to({ y: 105.2 }, 27, cjs.Ease.get(1)).wait(6).to({ y: 122.7 }, 3).to({ y: 52.1 }, 7, cjs.Ease.get(-1)).to({ y: 116.4 }, 5).to({ y: 105.2 }, 2).wait(14).to({ y: 100.2 }, 4, cjs.Ease.get(1)).to({ y: 110.2 }, 18, cjs.Ease.get(-1)).wait(18).to({ y: 105.2 }, 7).wait(1));

			// Layer 4
			this.instance_4 = new lib.Symbol7();
			this.instance_4.parent = this;
			this.instance_4.setTransform(171.9, 104.1, 1, 1, 0, 0, 0, 96.5, 15.2);

			this.timeline.addTween(cjs.Tween.get(this.instance_4).to({ y: 101.7 }, 26, cjs.Ease.get(-1)).to({ y: 104.1 }, 27, cjs.Ease.get(1)).wait(6).to({ y: 121.6 }, 3).to({ y: 51 }, 7, cjs.Ease.get(-1)).to({ y: 115.3 }, 5).to({ y: 104.1 }, 2).wait(14).to({ y: 99.1 }, 4, cjs.Ease.get(1)).to({ y: 109.1 }, 18, cjs.Ease.get(-1)).wait(18).to({ y: 104.1 }, 7).wait(1));

			// Layer 18
			this.instance_5 = new lib.Symbol2();
			this.instance_5.parent = this;
			this.instance_5.setTransform(246.7, 160.5, 1, 1, 0, 0, 0, 12, 20.7);

			this.timeline.addTween(cjs.Tween.get(this.instance_5).to({ rotation: -1.2, y: 158.1 }, 26, cjs.Ease.get(-1)).to({ rotation: 0, y: 160.5 }, 27, cjs.Ease.get(1)).wait(6).to({ y: 178 }, 3).to({ regX: 12.1, rotation: 7.2, y: 107.5 }, 7, cjs.Ease.get(-1)).to({ regX: 12, rotation: -12, y: 171.7 }, 5).to({ rotation: 0, y: 160.5 }, 2).wait(14).to({ y: 155.5 }, 4, cjs.Ease.get(1)).to({ rotation: 19.2, y: 165.5 }, 18, cjs.Ease.get(-1)).wait(18).to({ rotation: 0, y: 160.5 }, 7).wait(1));

			// Layer 5
			this.instance_6 = new lib.Symbol3();
			this.instance_6.parent = this;
			this.instance_6.setTransform(137.8, 162.4, 1, 1, 0, 0, 0, 102.8, 22.6);

			this.timeline.addTween(cjs.Tween.get(this.instance_6).to({ rotation: 3, y: 160.1 }, 26, cjs.Ease.get(-1)).to({ rotation: 0, y: 162.4 }, 27, cjs.Ease.get(1)).wait(6).to({ y: 179.9 }, 3).to({ regY: 22.7, rotation: -8.7, y: 109.4 }, 7, cjs.Ease.get(-1)).to({ regX: 102.9, rotation: 9.2, y: 173.7 }, 5).to({ regX: 102.8, regY: 22.6, rotation: 0, y: 162.4 }, 2).wait(14).to({ y: 157.4 }, 4, cjs.Ease.get(1)).to({ rotation: -12.5, x: 137.7, y: 167.4 }, 18, cjs.Ease.get(-1)).wait(18).to({ rotation: 0, x: 137.8, y: 162.4 }, 7).wait(1));

			// Layer 2
			this.instance_7 = new lib.Symbol8();
			this.instance_7.parent = this;
			this.instance_7.setTransform(195.3, 192.8, 1, 1, 0, 0, 0, 180.8, 17.8);

			this.timeline.addTween(cjs.Tween.get(this.instance_7).to({ y: 191.2 }, 26, cjs.Ease.get(-1)).to({ y: 192.8 }, 27, cjs.Ease.get(1)).wait(6).to({ y: 210.3 }, 3).to({ y: 149.7 }, 7, cjs.Ease.get(-1)).to({ y: 204 }, 5).to({ y: 192.8 }, 2).wait(18).to({ y: 197.8 }, 18, cjs.Ease.get(-1)).wait(18).to({ y: 192.8 }, 7).wait(1));

			// Layer 11
			this.instance_8 = new lib.Symbol11();
			this.instance_8.parent = this;
			this.instance_8.setTransform(107.9, 37.9, 1, 1, 0, 0, 0, 32.2, 35.5);

			this.timeline.addTween(cjs.Tween.get(this.instance_8).to({ y: 35.5 }, 26, cjs.Ease.get(-1)).to({ y: 37.9 }, 27, cjs.Ease.get(1)).wait(6).to({ regY: 35.6, rotation: -13.7, x: 106.3, y: 61.4 }, 3).to({ rotation: -1.7, x: 106.4, y: -16.3 }, 7, cjs.Ease.get(-1)).to({ regY: 35.5, rotation: 0, x: 107.9, y: 49.1 }, 5).to({ y: 37.9 }, 2).wait(14).to({ y: 32.9 }, 4, cjs.Ease.get(1)).to({ regY: 35.6, rotation: -22.5, x: 102.9, y: 48 }, 18, cjs.Ease.get(-1)).wait(18).to({ regY: 35.5, rotation: 0, x: 107.9, y: 37.9 }, 7).wait(1));

			// Layer 6
			this.instance_9 = new lib.Symbol10();
			this.instance_9.parent = this;
			this.instance_9.setTransform(287.8, 37.9, 1, 1, 0, 0, 0, 37.9, 35.5);

			this.timeline.addTween(cjs.Tween.get(this.instance_9).to({ y: 35.5 }, 26, cjs.Ease.get(-1)).to({ y: 37.9 }, 27, cjs.Ease.get(1)).wait(6).to({ rotation: 5.7, x: 284.1, y: 60.4 }, 3).to({ regX: 38, regY: 35.4, rotation: -7.2, x: 280.2, y: -17.5 }, 7, cjs.Ease.get(-1)).to({ regX: 37.9, regY: 35.5, rotation: 0, x: 287.8, y: 49.1 }, 5).to({ y: 37.9 }, 2).wait(14).to({ y: 32.9 }, 4, cjs.Ease.get(1)).to({ rotation: 17.2, y: 46.7 }, 18, cjs.Ease.get(-1)).wait(18).to({ rotation: 0, y: 37.9 }, 7).wait(1));

			// Layer 3
			this.instance_10 = new lib.Symbol9();
			this.instance_10.parent = this;
			this.instance_10.setTransform(133.7, 234.1, 1, 1, 0, 0, 0, 88.7, 65.3);

			this.timeline.addTween(cjs.Tween.get(this.instance_10).to({ y: 231.7 }, 26, cjs.Ease.get(-1)).to({ y: 234.1 }, 27, cjs.Ease.get(1)).wait(6).to({ y: 252.8 }, 3).to({ y: 182.3 }, 7, cjs.Ease.get(-1)).to({ y: 245.3 }, 5).to({ y: 234.1 }, 2).wait(14).to({ y: 230.4 }, 4, cjs.Ease.get(1)).to({ y: 239.1 }, 18, cjs.Ease.get(-1)).wait(18).to({ y: 234.1 }, 7).wait(1));

			// Layer 12
			this.instance_11 = new lib.Symbol19();
			this.instance_11.parent = this;
			this.instance_11.setTransform(193.3, 134.6, 1, 1, 0, 0, 0, 193.3, 84.7);

			this.timeline.addTween(cjs.Tween.get(this.instance_11).to({ y: 132.2 }, 26, cjs.Ease.get(-1)).to({ y: 134.6 }, 27, cjs.Ease.get(1)).wait(6).to({ y: 152 }, 3).to({ y: 81.5 }, 7, cjs.Ease.get(-1)).to({ y: 145.8 }, 5).to({ y: 134.6 }, 2).wait(14).to({ y: 129.6 }, 4, cjs.Ease.get(1)).to({ y: 139.6 }, 18, cjs.Ease.get(-1)).wait(18).to({ y: 134.6 }, 7).wait(1));

			// Layer 7
			this.instance_12 = new lib.Symbol14();
			this.instance_12.parent = this;
			this.instance_12.setTransform(193.3, 225, 1, 1, 0, 0, 0, 193.3, 127.5);

			this.timeline.addTween(cjs.Tween.get(this.instance_12).to({ y: 223.4 }, 26, cjs.Ease.get(-1)).to({ y: 225 }, 27, cjs.Ease.get(1)).wait(6).to({ y: 242.4 }, 3).to({ y: 181.9 }, 7, cjs.Ease.get(-1)).to({ y: 236.2 }, 5).to({ y: 225 }, 2).wait(18).to({ y: 230 }, 18, cjs.Ease.get(-1)).wait(18).to({ y: 225 }, 7).wait(1));

			// Layer 19
			this.instance_13 = new lib.Symbol23();
			this.instance_13.parent = this;
			this.instance_13.setTransform(239.4, 343.6, 1, 1, 0, 0, 0, 29.4, 12.8);
			this.instance_13._off = true;

			this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(59).to({ _off: false }, 0).to({ regY: 12.9, rotation: -21.7, y: 351.2 }, 3).to({ regY: 13, rotation: 6.2, x: 236.1, y: 301.7 }, 7, cjs.Ease.get(-1)).to({ regY: 12.9, rotation: -8.7, x: 239.4, y: 348.5 }, 5).to({ regY: 12.8, rotation: 0, y: 343.6 }, 2).wait(18).to({ rotation: -11.2, y: 346.1 }, 18, cjs.Ease.get(-1)).wait(18).to({ rotation: 0, y: 343.6 }, 7).wait(1));

			// Symbol 18
			this.instance_14 = new lib.Symbol18();
			this.instance_14.parent = this;
			this.instance_14.setTransform(240.3, 345, 1, 1, 0, 0, 0, 30.3, 14.2);

			this.instance_15 = new lib.Symbol22();
			this.instance_15.parent = this;
			this.instance_15.setTransform(245.7, 415, 1, 1, 0, 0, 0, 29.5, 2);
			this.instance_15._off = true;

			this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(53).to({ _off: true }, 6).wait(79));
			this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(59).to({ _off: false }, 0).to({ regX: 29.6, rotation: 33.3, x: 269.7, y: 409.1 }, 3).to({ rotation: 3.3, x: 234.6, y: 367.6 }, 7, cjs.Ease.get(-1)).to({ rotation: 10.2, x: 255.4, y: 415 }, 5).to({ regX: 29.5, rotation: 0, x: 245.7 }, 2).wait(18).to({ regX: 29.6, rotation: 9.5, x: 258.7, y: 412.8 }, 18, cjs.Ease.get(-1)).wait(18).to({ regX: 29.5, rotation: 0, x: 245.7, y: 415 }, 7).wait(1));

			// Layer 20
			this.instance_16 = new lib.Symbol21();
			this.instance_16.parent = this;
			this.instance_16.setTransform(149.4, 343.4, 1, 1, 0, 0, 0, 48.6, 11.6);
			this.instance_16._off = true;

			this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(59).to({ _off: false }, 0).to({ rotation: 23.7, y: 349.7 }, 3).to({ regY: 11.7, rotation: 4.2, x: 151.8, y: 301.6 }, 7, cjs.Ease.get(-1)).to({ rotation: 19.9, x: 149.4, y: 349.9 }, 5).to({ regY: 11.6, rotation: 0, y: 343.4 }, 2).wait(18).to({ regY: 11.7, rotation: 19.4, x: 149.5, y: 347.3 }, 18, cjs.Ease.get(-1)).wait(18).to({ regY: 11.6, rotation: 0, x: 149.4, y: 343.4 }, 7).wait(1));

			// Symbol 17
			this.instance_17 = new lib.Symbol17();
			this.instance_17.parent = this;
			this.instance_17.setTransform(148.1, 345.5, 1, 1, 0, 0, 0, 47.3, 13.7);

			this.instance_18 = new lib.Symbol20();
			this.instance_18.parent = this;
			this.instance_18.setTransform(153.5, 416.2, 1, 1, 0, 0, 0, 52.7, 6.5);
			this.instance_18._off = true;

			this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(53).to({ _off: true }, 6).wait(79));
			this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(59).to({ _off: false }, 0).to({ regX: 52.8, rotation: -17.9, x: 129.4, y: 408.8 }, 3).to({ rotation: -4, x: 152.1, y: 370.6 }, 7, cjs.Ease.get(-1)).to({ rotation: -18.9, x: 133, y: 413 }, 5).to({ regX: 52.7, rotation: 0, x: 153.5, y: 416.2 }, 2).wait(18).to({ regX: 52.8, rotation: -10.7, x: 132, y: 413.7 }, 18, cjs.Ease.get(-1)).wait(18).to({ regX: 52.7, rotation: 0, x: 153.5, y: 416.2 }, 7).wait(1));

			// Symbol 16
			this.instance_19 = new lib.Symbol16();
			this.instance_19.parent = this;
			this.instance_19.setTransform(18.8, 203.1, 1, 1, 0, 0, 0, 9.1, 8.7);

			this.timeline.addTween(cjs.Tween.get(this.instance_19).to({ regX: 9.3, regY: 8.8, rotation: -0.4, x: 18.9, y: 201.7 }, 26, cjs.Ease.get(-1)).to({ regX: 9.1, regY: 8.7, rotation: 0, x: 18.8, y: 203.1 }, 27, cjs.Ease.get(1)).wait(6).to({ y: 206.9 }, 0).to({ y: 218.1 }, 3).to({ rotation: 9.2, x: 15.5, y: 159.2 }, 7, cjs.Ease.get(-1)).to({ rotation: 0, x: 18.8, y: 218.1 }, 5).to({ y: 206.9 }, 2).wait(18).to({ regX: 9.2, rotation: 9.5, y: 211.9 }, 18, cjs.Ease.get(-1)).wait(18).to({ regX: 9.1, rotation: 0, y: 206.9 }, 7).wait(1));

			// Symbol 15
			this.instance_20 = new lib.Symbol15();
			this.instance_20.parent = this;
			this.instance_20.setTransform(368.8, 202.7, 1, 1, 0, 0, 0, 43.7, 8.3);

			this.timeline.addTween(cjs.Tween.get(this.instance_20).to({ regX: 43.8, rotation: 1, x: 368.9, y: 201.2 }, 26, cjs.Ease.get(-1)).to({ regX: 43.7, rotation: 0, x: 368.8, y: 202.7 }, 27, cjs.Ease.get(1)).wait(6).to({ y: 206.5 }, 0).to({ y: 217.7 }, 3).to({ regX: 43.8, rotation: -16.5, x: 376, y: 157.2 }, 7, cjs.Ease.get(-1)).to({ regX: 43.7, rotation: 0, x: 368.8, y: 217.7 }, 5).to({ y: 206.5 }, 2).wait(18).to({ regY: 8.2, rotation: -12.7, y: 211.4 }, 18, cjs.Ease.get(-1)).wait(18).to({ regY: 8.3, rotation: 0, y: 206.5 }, 7).wait(1));

		}).prototype = p = new cjs.MovieClip();
		p.nominalBounds = new cjs.Rectangle(0, 2.4, 387, 487.4);


		// stage content:
		(lib.dog_export = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Symbol1();
			this.instance.parent = this;
			this.instance.setTransform(179.1, 316.3, 0.633, 0.633, 0, 0, 0, 196, 445.5);

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = p = new cjs.MovieClip();
		p.nominalBounds = new cjs.Rectangle(232.6, 220.9, 244.9, 308.5);
		// library properties:
		lib.properties = {
			id: '450F9CB287028449A3566E8BC8BF4A4B',
			width: 355,
			height: 370,
			fps: 31,
			color: '#FFFFFF',
			opacity: 1.00,
			manifest: [
				{ src: 'static/assets/images/dog_export_atlas_.png?1505548537680', id: 'dog_export_atlas_' }
			],
			preloads: []
		};



		// bootstrap callback support:

		(lib.Stage = function (canvas) {
			createjs.Stage.call(this, canvas);
		}).prototype = p = new createjs.Stage();

		p.setAutoPlay = function (autoPlay) {
			this.tickEnabled = autoPlay;
		};
		p.play = function () { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) };
		p.stop = function (ms) { if (ms) this.seek(ms); this.tickEnabled = false; };
		p.seek = function (ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); };
		p.getDuration = function () { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; };

		p.getTimelinePosition = function () { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; };

		an.bootcompsLoaded = an.bootcompsLoaded || [];
		if (!an.bootstrapListeners) {
			an.bootstrapListeners = [];
		}

		an.bootstrapCallback = function (fnCallback) {
			an.bootstrapListeners.push(fnCallback);
			if (an.bootcompsLoaded.length > 0) {
				for (let i = 0; i < an.bootcompsLoaded.length; ++i) {
					fnCallback(an.bootcompsLoaded[i]);
				}
			}
		};

		an.compositions = an.compositions || {};
		an.compositions['450F9CB287028449A3566E8BC8BF4A4B'] = {
			getStage: function () { return exportRoot.getStage(); },
			getLibrary: function () { return lib; },
			getSpriteSheet: function () { return ss; },
			getImages: function () { return img; }
		};

		an.compositionLoaded = function (id) {
			an.bootcompsLoaded.push(id);
			for (let j = 0; j < an.bootstrapListeners.length; j++) {
				an.bootstrapListeners[j](id);
			}
		};

		an.getComposition = function (id) {
			return an.compositions[id];
		};


	}

	initTitanRoutine(cjs, an): void {

		let p; // shortcut to reference prototypes
		const lib: any = {}; const ss = {}; const img = {};
		lib.webFontTxtInst = {};
		let loadedTypekitCount = 0;
		let loadedGoogleCount = 0;
		const gFontsUpdateCacheList = [];
		const tFontsUpdateCacheList = [];
		lib.ssMetadata = [
			{ name: 'titan_export_atlas_', frames: [[322, 168, 90, 75], [322, 245, 112, 41], [414, 189, 37, 10], [322, 0, 106, 166], [0, 0, 320, 361], [199, 363, 157, 388], [414, 168, 20, 19], [0, 363, 197, 415]] }
		];



		lib.updateListCache = function (cacheList) {
			for (let i = 0; i < cacheList.length; i++) {
				if (cacheList[i].cacheCanvas)
					cacheList[i].updateCache();
			}
		};

		lib.addElementsToCache = function (textInst, cacheList) {
			let cur = textInst;
			while (cur != null && cur != exportRoot) {
				if (cacheList.indexOf(cur) != -1)
					break;
				cur = cur.parent;
			}
			if (cur != exportRoot) {
				let cur2 = textInst;
				let index = cacheList.indexOf(cur);
				while (cur2 != null && cur2 != cur) {
					cacheList.splice(index, 0, cur2);
					cur2 = cur2.parent;
					index++;
				}
			}
			else {
				cur = textInst;
				while (cur != null && cur != exportRoot) {
					cacheList.push(cur);
					cur = cur.parent;
				}
			}
		};

		lib.gfontAvailable = function (family, totalGoogleCount) {
			lib.properties.webfonts[family] = true;
			const txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];
			for (let f = 0; f < txtInst.length; ++f)
				lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);

			loadedGoogleCount++;
			if (loadedGoogleCount == totalGoogleCount) {
				lib.updateListCache(gFontsUpdateCacheList);
			}
		};

		lib.tfontAvailable = function (family, totalTypekitCount) {
			lib.properties.webfonts[family] = true;
			const txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];
			for (let f = 0; f < txtInst.length; ++f)
				lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);

			loadedTypekitCount++;
			if (loadedTypekitCount == totalTypekitCount) {
				lib.updateListCache(tFontsUpdateCacheList);
			}
		};
		// symbols:



		(lib.Bitmap1 = function () {
			this.spriteSheet = ss['titan_export_atlas_'];
			this.gotoAndStop(0);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap10 = function () {
			this.spriteSheet = ss['titan_export_atlas_'];
			this.gotoAndStop(1);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap11 = function () {
			this.spriteSheet = ss['titan_export_atlas_'];
			this.gotoAndStop(2);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap4 = function () {
			this.spriteSheet = ss['titan_export_atlas_'];
			this.gotoAndStop(3);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap5 = function () {
			this.spriteSheet = ss['titan_export_atlas_'];
			this.gotoAndStop(4);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap7 = function () {
			this.spriteSheet = ss['titan_export_atlas_'];
			this.gotoAndStop(5);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap8 = function () {
			this.spriteSheet = ss['titan_export_atlas_'];
			this.gotoAndStop(6);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap9 = function () {
			this.spriteSheet = ss['titan_export_atlas_'];
			this.gotoAndStop(7);
		}).prototype = p = new cjs.Sprite();
		// helper functions:

		function mc_symbol_clone() {
			const clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
			clone.gotoAndStop(this.currentFrame);
			clone.paused = this.paused;
			clone.framerate = this.framerate;
			return clone;
		}

		function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
			const prototype = cjs.extend(symbol, cjs.MovieClip);
			prototype.clone = mc_symbol_clone;
			prototype.nominalBounds = nominalBounds;
			prototype.frameBounds = frameBounds;
			return prototype;
		}


		(lib.Symbol9 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 2
			this.instance = new lib.Bitmap11();
			this.instance.parent = this;
			this.instance.setTransform(14, -2);

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

			// Layer 1
			this.shape = new cjs.Shape();
			this.shape.graphics.f('#ECF8FF').s().p('AjCArIhShHIA6gCIDaAAIAAABIAABIgABsAlIAAhJICogGIAABPg');
			this.shape.setTransform(27.7, 4.3);

			this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0, -2, 55.3, 10.7), null);


		(lib.Symbol8 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap8();
			this.instance.parent = this;
			this.instance.setTransform(13.5, 0, 1, 1, 45);

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(0, 0, 27.6, 27.6), null);


		(lib.Symbol7 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap1();
			this.instance.parent = this;
			this.instance.setTransform(-4, -5);

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-4, -5, 90, 75), null);


		(lib.Symbol5 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap10();
			this.instance.parent = this;
			this.instance.setTransform(-21, -28);

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-21, -28, 112, 41), null);


		(lib.Symbol3 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap5();
			this.instance.parent = this;
			this.instance.setTransform(162, 161);

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(162, 161, 320, 361), null);


		(lib.Symbol2 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 2
			this.instance = new lib.Bitmap9();
			this.instance.parent = this;
			this.instance.setTransform(0, -18);

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0, -18, 197, 415), null);


		(lib.Symbol1 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap7();
			this.instance.parent = this;
			this.instance.setTransform(-24, 0);

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-24, 0, 157, 388), null);


		(lib.Symbol4 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 3 (mask)
			const mask = new cjs.Shape();
			mask._off = true;
			mask.graphics.p('AliB3IgKjZILYgUIAADtg');
			mask.setTransform(54.1, 94.2);

			// Layer 2
			this.instance = new lib.Symbol5();
			this.instance.parent = this;
			this.instance.setTransform(54.6, 89, 1, 1, 0, 0, 0, 36, 7);

			const maskedShapeInstanceList = [this.instance];

			for (let shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
				maskedShapeInstanceList[shapedInstanceItr].mask = mask;
			}

			this.timeline.addTween(cjs.Tween.get(this.instance).to({ y: 105.6 }, 3).to({ y: 89 }, 3).wait(78));

			// Layer 1
			this.instance_1 = new lib.Bitmap4();
			this.instance_1.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(84));

		}).prototype = p = new cjs.MovieClip();
		p.nominalBounds = new cjs.Rectangle(0, 0, 106, 166);


		(lib.Symbol6 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, { idle: 0, attack: 59, hit: 88 });

			// timeline functions:
			this.frame_58 = function () {
				this.gotoAndPlay(0);
			};
			this.frame_87 = function () {
				this.gotoAndPlay(0);
			};
			this.frame_88 = function () {
				this.head.gotoAndPlay(10);
			};

			// actions tween:
			this.timeline.addTween(cjs.Tween.get(this).wait(58).call(this.frame_58).wait(29).call(this.frame_87).wait(1).call(this.frame_88).wait(45));

			// Layer 7
			this.instance = new lib.Symbol8();
			this.instance.parent = this;
			this.instance.setTransform(190.3, 59.9, 0.116, 0.116, 41, 0, 0, 13.7, 13.8);
			this.instance._off = true;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(95).to({ _off: false }, 0).to({ regX: 13.9, scaleX: 0.94, scaleY: 0.94, rotation: 16.3, x: 194.8, y: 58.6 }, 3).to({ scaleX: 0.72, scaleY: 0.72, rotation: -81.8, x: 211.8, y: 52.9 }, 14).to({ regX: 13.8, scaleX: 0.23, scaleY: 0.23, x: 213.2, y: 48.9 }, 4).to({ _off: true }, 1).wait(16));

			// Layer 4
			this.instance_1 = new lib.Symbol8();
			this.instance_1.parent = this;
			this.instance_1.setTransform(136.5, 58.8, 0.209, 0.209, -25, 0, 0, 13.7, 13.8);
			this.instance_1._off = true;

			this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(97).to({ _off: false }, 0).to({ regX: 13.8, scaleX: 1.06, scaleY: 1.06, rotation: -13.8, x: 143.6, y: 59 }, 3).to({ regX: 13.9, regY: 13.9, scaleX: 1.28, scaleY: 1.28, rotation: 31, x: 172.6, y: 59.2 }, 14).to({ regX: 13.5, regY: 13.5, scaleX: 0.22, scaleY: 0.22, rotation: 166, x: 177.8, y: 57.8 }, 4).to({ _off: true }, 1).wait(14));

			// Layer 9
			this.instance_2 = new lib.Symbol8();
			this.instance_2.parent = this;
			this.instance_2.setTransform(101.3, 52, 0.337, 0.337, 0, 0, 0, 13.8, 13.8);
			this.instance_2._off = true;

			this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(99).to({ _off: false }, 0).to({ regY: 13.9, scaleX: 0.96, scaleY: 0.96, rotation: -21.3, x: 104.5, y: 53.1 }, 3).to({ scaleX: 0.79, scaleY: 0.79, rotation: -107.1, x: 117, y: 57 }, 14).to({ regX: 13.9, regY: 13.8, scaleX: 0.17, scaleY: 0.17, x: 125.2, y: 56.5 }, 4).to({ _off: true }, 1).wait(12));

			// Layer 2
			this.instance_3 = new lib.Symbol7();
			this.instance_3.parent = this;
			this.instance_3.setTransform(159.6, 101.2, 1, 1, 0, 0, 0, 42.8, 33.4);
			this.instance_3._off = true;

			this.instance_4 = new lib.Symbol9();
			this.instance_4.parent = this;
			this.instance_4.setTransform(158.5, 85.3, 1, 1, 0, 0, 0, 27.7, 3.6);
			this.instance_4.alpha = 0;
			this.instance_4._off = true;

			this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(62).to({ _off: false }, 0).to({ y: 111.2 }, 4).to({ y: 106.7 }, 3).to({ y: 110.2 }, 5).to({ y: 109.1, alpha: 0 }, 4).to({ _off: true }, 10).wait(45));
			this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(92).to({ _off: false }, 0).to({ y: 90.6, alpha: 1 }, 3).wait(26).to({ x: 157.6, y: 93.6, alpha: 0 }, 5).wait(7));

			// Symbol 4
			this.head = new lib.Symbol4();
			this.head.parent = this;
			this.head.setTransform(158.8, 83, 1, 1, 0, 0, 0, 52.9, 83);

			this.timeline.addTween(cjs.Tween.get(this.head).to({ y: 76.5 }, 28).to({ y: 83 }, 30).wait(1).to({ y: 76 }, 3, cjs.Ease.get(-1)).to({ y: 83 }, 4, cjs.Ease.get(-1)).to({ y: 80 }, 3).to({ y: 83 }, 5).wait(14).to({ y: 70.2 }, 4, cjs.Ease.get(1)).to({ y: 75.5 }, 3, cjs.Ease.get(-0.5)).wait(26).to({ y: 83 }, 11, cjs.Ease.get(-1)).wait(1));

			// Symbol 2
			this.instance_5 = new lib.Symbol2();
			this.instance_5.parent = this;
			this.instance_5.setTransform(52.8, 179.1, 0.579, 0.579, 0, 0, 0, 91.2, 198.5);

			this.timeline.addTween(cjs.Tween.get(this.instance_5).to({ y: 176.5 }, 28).to({ y: 179.1 }, 30).wait(1).to({ y: 152.1 }, 3, cjs.Ease.get(-1)).to({ y: 191.1 }, 4, cjs.Ease.get(-1)).wait(8).to({ y: 179.1 }, 13, cjs.Ease.get(1)).wait(1).to({ regY: 198.6, x: 52.9, y: 177.3 }, 4, cjs.Ease.get(1)).to({ regY: 198.5, x: 52.8, y: 177.1 }, 3).wait(26).to({ y: 179.1 }, 11, cjs.Ease.get(-1)).wait(1));

			// Symbol 1
			this.instance_6 = new lib.Symbol1();
			this.instance_6.parent = this;
			this.instance_6.setTransform(245.4, 111.5, 0.579, 0.579, 0, 0, 0, 49.8, 63.4);

			this.timeline.addTween(cjs.Tween.get(this.instance_6).to({ y: 108.9 }, 28).to({ y: 111.5 }, 30).wait(1).to({ y: 91.5 }, 3, cjs.Ease.get(-1)).to({ y: 119.5 }, 4, cjs.Ease.get(-1)).wait(8).to({ y: 111.5 }, 13, cjs.Ease.get(1)).wait(1).to({ regX: 49.9, regY: 63.5, y: 109.9 }, 4, cjs.Ease.get(1)).to({ regX: 49.8, regY: 63.4, y: 109.7 }, 3).wait(26).to({ y: 111.5 }, 11, cjs.Ease.get(-1)).wait(1));

			// Symbol 3
			this.instance_7 = new lib.Symbol3();
			this.instance_7.parent = this;
			this.instance_7.setTransform(147.7, 152.6, 0.579, 0.579, 0, 0, 0, 255, 260.2);

			this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(66).to({ y: 148.5 }, 2).to({ y: 152.6 }, 3).wait(62));

		}).prototype = p = new cjs.MovieClip();
		p.nominalBounds = new cjs.Rectangle(0, 0, 293.6, 304.1);


		// stage content:
		(lib.titan_export = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.titan = new lib.Symbol6();
			this.titan.parent = this;
			this.titan.setTransform(162, 209.8, 1, 1, 0, 0, 0, 141, 173.1);

			this.timeline.addTween(cjs.Tween.get(this.titan).wait(1));

		}).prototype = p = new cjs.MovieClip();
		p.nominalBounds = new cjs.Rectangle(198.5, 221.7, 293.6, 304.1);
		// library properties:
		lib.properties = {
			id: '25788DBD55E1BB4BA3DF07A2FFDA3033',
			width: 355,
			height: 370,
			fps: 31,
			color: '#FFFFFF',
			opacity: 1.00,
			webfonts: {},
			manifest: [
				{ src: 'static/assets/images/titan_export_atlas_.png', id: 'titan_export_atlas_' }
			],
			preloads: []
		};



		// bootstrap callback support:

		(lib.Stage = function (canvas) {
			createjs.Stage.call(this, canvas);
		}).prototype = p = new createjs.Stage();

		p.setAutoPlay = function (autoPlay) {
			this.tickEnabled = autoPlay;
		};
		p.play = function () { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()); };
		p.stop = function (ms) { if (ms) this.seek(ms); this.tickEnabled = false; };
		p.seek = function (ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); };
		p.getDuration = function () { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; };

		p.getTimelinePosition = function () { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; };

		an.bootcompsLoaded = an.bootcompsLoaded || [];
		if (!an.bootstrapListeners) {
			an.bootstrapListeners = [];
		}

		an.bootstrapCallback = function (fnCallback) {
			an.bootstrapListeners.push(fnCallback);
			if (an.bootcompsLoaded.length > 0) {
				for (let i = 0; i < an.bootcompsLoaded.length; ++i) {
					fnCallback(an.bootcompsLoaded[i]);
				}
			}
		};

		an.compositions = an.compositions || {};
		an.compositions['25788DBD55E1BB4BA3DF07A2FFDA3033'] = {
			getStage: function () { return exportRoot.getStage(); },
			getLibrary: function () { return lib; },
			getSpriteSheet: function () { return ss; },
			getImages: function () { return img; }
		};

		an.compositionLoaded = function (id) {
			an.bootcompsLoaded.push(id);
			for (let j = 0; j < an.bootstrapListeners.length; j++) {
				an.bootstrapListeners[j](id);
			}
		};

		an.getComposition = function (id) {
			return an.compositions[id];
		};



	}

	initCatRoutine(cjs, an): void {

		let p; // shortcut to reference prototypes
		const lib: any = {}; const ss = {}; const img = {};
		lib.ssMetadata = [
			{ name: 'kitty_export_atlas_', frames: [[614, 1493, 633, 308], [1249, 1493, 674, 281], [3328, 0, 537, 802], [0, 0, 2117, 1491], [3328, 804, 677, 332], [2119, 1167, 885, 319], [2119, 0, 1207, 1165], [3006, 1167, 774, 341], [0, 1493, 612, 353], [2119, 1488, 784, 319]] }
		];


		// symbols:



		(lib.Bitmap10 = function () {
			this.spriteSheet = ss['kitty_export_atlas_'];
			this.gotoAndStop(0);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap11 = function () {
			this.spriteSheet = ss['kitty_export_atlas_'];
			this.gotoAndStop(1);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap2 = function () {
			this.spriteSheet = ss['kitty_export_atlas_'];
			this.gotoAndStop(2);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap3 = function () {
			this.spriteSheet = ss['kitty_export_atlas_'];
			this.gotoAndStop(3);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap4 = function () {
			this.spriteSheet = ss['kitty_export_atlas_'];
			this.gotoAndStop(4);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap5 = function () {
			this.spriteSheet = ss['kitty_export_atlas_'];
			this.gotoAndStop(5);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap6 = function () {
			this.spriteSheet = ss['kitty_export_atlas_'];
			this.gotoAndStop(6);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap7 = function () {
			this.spriteSheet = ss['kitty_export_atlas_'];
			this.gotoAndStop(7);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap8 = function () {
			this.spriteSheet = ss['kitty_export_atlas_'];
			this.gotoAndStop(8);
		}).prototype = p = new cjs.Sprite();



		(lib.Bitmap9 = function () {
			this.spriteSheet = ss['kitty_export_atlas_'];
			this.gotoAndStop(9);
		}).prototype = p = new cjs.Sprite();
		// helper functions:

		function mc_symbol_clone() {
			const clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
			clone.gotoAndStop(this.currentFrame);
			clone.paused = this.paused;
			clone.framerate = this.framerate;
			return clone;
		}

		function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
			const prototype = cjs.extend(symbol, cjs.MovieClip);
			prototype.clone = mc_symbol_clone;
			prototype.nominalBounds = nominalBounds;
			prototype.frameBounds = frameBounds;
			return prototype;
		}


		(lib.Symbol14 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.shape = new cjs.Shape();
			this.shape.graphics.f().s('#000000').ss(20, 1, 1).p('AVqoaIgFAHImQIUIG/HOIAaAcAIMnWIHJHXImVIaApPoaIggArIl1HwImVIaA2tnWIHJHXIGGGUIBTBW');
			this.shape.setTransform(163.4, 71.6);

			this.shape_1 = new cjs.Shape();
			this.shape_1.graphics.f('#EBFFEA').s().p('AImH0Qi1jPAAklQAAkjC1jQQC2jPEBAAQDyAACvC3ImRIUIGRoUIAWAYQC1DQABEjQAAERieDGIm/nOIG/HOIgYAdQi1DPkCAAQkBAAi2jPgAJYIjIGVoaInInXIHIHXgA2TH0Qi2jPAAklQAAkjC2jQQC2jPEBAAQECAAB/DUIAEAHIl1HwIF1nwQB7DSgVEgQgRDmhECsImGmUInJnXIHJHXIGGGUQgTAxgYAtQhpDIkCAAQkBAAi2jPgA1hIjIGVoagAWsHXgApGGdgApXnnIAAAAgAV+oLIAAAAg');
			this.shape_1.setTransform(161, 70.7);

			this.timeline.addTween(cjs.Tween.get({}).to({ state: [{ t: this.shape_1 }, { t: this.shape }] }).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(0, 0, 321.9, 141.5), null);


		(lib.Symbol12 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.shape = new cjs.Shape();
			this.shape.graphics.f('#000000').s().p('Av4D+QgLgFgMgHQgogagKgqQgNgyAig4QAZgoA1gwQBAg6ApgeQA9gsA5gXQArgSA6gLQAfgGBJgKQC+gaBWgHQCagOB7AFQBgAEBzARQBUAMB+AZQCgAfB2AeQCVAkB8AsQAlANAVALQAeAQARAWQAYAdABAoQABApgXAfQgXAggpAOQgmANgtgDQgjgDgsgPIhOgdQhzgriOghQhtgZibgYQiXgXhngFQiMgHh0ASQgXADhHAPQg6AMgkAEQhJADgkADQg/AFgqASQgpAThkBdQhDA/g5APQgMADgNABIgIAAQgbAAgZgKg');
			this.shape.setTransform(145.9, 50.1);

			this.shape_1 = new cjs.Shape();
			this.shape_1.graphics.f('#9CFF9B').s().p('AgwHIQibghhNgPQiHgahjgHIibgIQhcgFg9gPQgjgJhDgXQhDgXghgJQg1gNhagIQA4gPBDg/QBkheAqgSQApgSBAgFQAkgDBIgEQAkgEA7gMQBHgOAXgEQBzgSCMAIQBoAFCXAXQCaAYBuAZQCOAgBzAsIBNAdQAtAOAjAEQAsADAmgNQAqgPAXggQAWgfAAgoQgBgpgYgdQgRgVgfgQQgUgLgmgOQh7griVglQh3gdigggQh+gYhTgMQhzgRhggEQh7gGibAOQhWAIi+AZQhIAKggAGQg6AMgqARQg5AXg9AtQgpAehBA6Qg1AvgZApQghA4AMAxQALAqAoAaQALAIAMAFQiCgJg8gNQhTgRgigqQgYggACgrQABgjAUgaIgJgCQhUgXgdg8Qgbg3AdhBQAcg+A6ghQAygdBHgKQAugHBTgBIBJAAIgGgMQgOgcABggQABgiARgZQAYgjA0gMQAjgJA8AAIIMAAQAugBAcAEQApAHAbATQAoAdAFA4QAEAogQAeIBPAAQB7ACA7AIQBeAOBBAmIAIADQABgmAfggQAcgeArgOQAdgIA0gGQA7gGAXgFQA0gKBfgqQBfgrAygLQA/gNBUAJQAfADB0ATQElAyEjAAQAwAAAXACQAnADAdAJQAkANAZAYQAbAbAFAhQAJAvgkArQgiAogyANQgoAMg7gBQhDgDghAAQhjgBi/AoQhmAWhLAKQATAcgFAfIIGBRQAwAIAaAJQApAOAXAYQAfAigCAxQgCAxgiAfQgbAZguAKQgeAGg3ABItIAMQASAdgLAmQgKAkgcAYQgZAVgnALQgbAIgrAFQguAFg1AAQiaAAjUgrg');
			this.shape_1.setTransform(169.6, 50);

			this.timeline.addTween(cjs.Tween.get({}).to({ state: [{ t: this.shape_1 }, { t: this.shape }] }).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(0, 0, 339.2, 99.9), null);


		(lib.Symbol11 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap2();
			this.instance.parent = this;
			this.instance.setTransform(-21, -20);

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(-21, -20, 537, 802), null);


		(lib.Symbol10 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap3();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0, 0, 2117, 1491), null);


		(lib.Symbol9 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap4();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0, 0, 677, 332), null);


		(lib.Symbol8 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 2
			this.instance = new lib.Bitmap6();
			this.instance.parent = this;
			this.instance.setTransform(0, 343);

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(0, 343, 1207, 1165), null);


		(lib.Symbol7 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap8();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0, 0, 612, 353), null);


		(lib.Symbol6 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap7();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0, 0, 774, 341), null);


		(lib.Symbol5 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap11();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0, 0, 674, 281), null);


		(lib.Symbol4 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap10();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0, 0, 633, 308), null);


		(lib.Symbol3 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap9();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0, 0, 784, 319), null);


		(lib.Symbol2 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Bitmap5();
			this.instance.parent = this;

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0, 0, 885, 319), null);


		(lib.Symbol15 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// Layer 1
			this.instance = new lib.Symbol12();
			this.instance.parent = this;
			this.instance.setTransform(169.6, 50, 1, 1, 0, 0, 0, 169.6, 50);

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = getMCSymbolPrototype(lib.Symbol15, new cjs.Rectangle(0, 0, 339.2, 99.9), null);


		(lib.Symbol1 = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, { idle: 0, attack: 60, hit: 90 });

			// timeline functions:
			this.frame_59 = function () {
				this.gotoAndPlay(1);
			};
			this.frame_89 = function () {
				this.gotoAndPlay(1);
			};

			// actions tween:
			this.timeline.addTween(cjs.Tween.get(this).wait(59).call(this.frame_59).wait(30).call(this.frame_89).wait(48));

			// Layer 7
			this.instance = new lib.Symbol3();
			this.instance.parent = this;
			this.instance.setTransform(1284, 823.5, 1, 1, 0, 0, 0, 46.8, 178);

			this.timeline.addTween(cjs.Tween.get(this.instance).to({ regX: 46.9, rotation: -2.2 }, 4, cjs.Ease.get(-1)).to({ regX: 46.8, rotation: 0 }, 12, cjs.Ease.get(1)).wait(52).to({ regX: 46.9, rotation: 8, x: 1284.1, y: 629.7 }, 4).to({ regX: 46.8, rotation: 0, x: 1284, y: 845.1 }, 5).to({ y: 823.5 }, 2).wait(11).to({ regX: 46.9, regY: 178.1, rotation: -15, x: 1284.1, y: 823.6 }, 6, cjs.Ease.get(-1)).to({ regX: 46.8, regY: 178, rotation: 18.5, x: 1284, y: 823.5 }, 12, cjs.Ease.get(-1)).wait(19).to({ rotation: 0 }, 9, cjs.Ease.get(-1)).wait(1));

			// Layer 2
			this.instance_1 = new lib.Symbol8();
			this.instance_1.parent = this;
			this.instance_1.setTransform(1779, 480.6, 1, 1, -0.4, 0, 0, 839.6, 355.1);

			this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(6).to({ rotation: 1.6 }, 23, cjs.Ease.get(1)).to({ rotation: -0.4 }, 23, cjs.Ease.get(-1)).wait(8).to({ rotation: -0.4 }, 0).wait(8).to({ y: 286.8 }, 4).to({ y: 502.2 }, 5).to({ y: 480.6 }, 2).wait(11).to({ rotation: 4.1 }, 5).to({ rotation: -0.4 }, 11).wait(21).to({ rotation: -0.4 }, 0).wait(10));

			// Layer 3
			this.instance_2 = new lib.Symbol7();
			this.instance_2.parent = this;
			this.instance_2.setTransform(528.4, 1016.3, 1, 1, 4.5, 0, 0, 80.3, 102.5);

			this.timeline.addTween(cjs.Tween.get(this.instance_2).to({ rotation: 15 }, 22).to({ rotation: 4.5 }, 37).wait(9).to({ rotation: -27.7, y: 822.4 }, 4).to({ rotation: 29.7, y: 1037.8 }, 5).to({ rotation: 4.5, y: 1016.3 }, 2).wait(11).to({ rotation: -5.5, y: 990 }, 3).to({ rotation: 4.5, y: 1016.3 }, 13).wait(31));

			// Layer 4
			this.instance_3 = new lib.Symbol6();
			this.instance_3.parent = this;
			this.instance_3.setTransform(1637.8, 1016.7, 1, 1, 0, 0, 0, 698.8, 102.9);

			this.timeline.addTween(cjs.Tween.get(this.instance_3).to({ rotation: -9.5 }, 34).to({ rotation: 0 }, 25).wait(9).to({ regX: 698.9, rotation: 19, x: 1637.9, y: 822.9 }, 4).to({ regX: 698.8, rotation: -14, x: 1637.8, y: 1038.2 }, 5).to({ rotation: 0, y: 1016.7 }, 2).wait(11).to({ regX: 698.9, rotation: 11, x: 1638, y: 990.4 }, 5).to({ regX: 698.8, rotation: 0, x: 1637.8, y: 1016.7 }, 11).wait(31));

			// Layer 5
			this.instance_4 = new lib.Symbol5();
			this.instance_4.parent = this;
			this.instance_4.setTransform(798.5, 1431.8, 1, 1, 0, 0, 0, 215.5, 139);

			this.timeline.addTween(cjs.Tween.get(this.instance_4).to({ y: 1402.3 }, 4).to({ y: 1431.8 }, 4).to({ y: 1402.3 }, 4).to({ y: 1431.8 }, 4).to({ y: 1402.3 }, 4).to({ y: 1431.8 }, 4).wait(44).to({ y: 1281 }, 4).to({ y: 1431.8 }, 5).wait(13).to({ y: 1389.4 }, 3).to({ y: 1431.8 }, 13).wait(31));

			// Layer 6
			this.instance_5 = new lib.Symbol4();
			this.instance_5.parent = this;
			this.instance_5.setTransform(1328.1, 1457.3, 1, 1, 0, 0, 0, 421.1, 191);

			this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(68).to({ y: 1328.1 }, 4).to({ y: 1464.7 }, 5).to({ y: 1457.3 }, 2).wait(11).to({ y: 1436.1 }, 4).to({ y: 1457.3 }, 12).wait(31));

			// Layer 10
			this.instance_6 = new lib.Symbol2();
			this.instance_6.parent = this;
			this.instance_6.setTransform(836.5, 811.7, 1, 1, 2, 0, 0, 754, 166.2);

			this.timeline.addTween(cjs.Tween.get(this.instance_6).to({ rotation: 5.2 }, 4, cjs.Ease.get(-1)).to({ rotation: 2 }, 12, cjs.Ease.get(1)).wait(52).to({ regX: 754.1, rotation: -7.3, x: 836.6, y: 617.8 }, 4).to({ regX: 754, rotation: 2, x: 836.5, y: 833.2 }, 5).to({ y: 811.7 }, 2).wait(11).to({ regX: 754.1, regY: 166.3, rotation: 10.7, x: 836.6, y: 811.8 }, 6, cjs.Ease.get(-1)).to({ regY: 166.2, rotation: -17, y: 811.6 }, 10, cjs.Ease.get(-1)).wait(21).to({ regX: 754, rotation: 2, x: 836.5, y: 811.7 }, 9, cjs.Ease.get(-1)).wait(1));

			// Layer 8
			this.instance_7 = new lib.Symbol9();
			this.instance_7.parent = this;
			this.instance_7.setTransform(1114.9, 182.3, 1, 1, 0, 0, 0, 338.3, 166);

			this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(68).to({ y: -11.5 }, 4).to({ y: 203.9 }, 5).to({ y: 182.3 }, 2).wait(11).to({ y: 156 }, 4).to({ y: 182.3 }, 5).wait(38));

			// Layer 12
			this.instance_8 = new lib.Symbol14();
			this.instance_8.parent = this;
			this.instance_8.setTransform(1076.3, 536.7, 1, 1, 0, 0, 0, 161, 70.7);
			this.instance_8.alpha = 0;
			this.instance_8._off = true;

			this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(90).to({ _off: false }, 0).to({ alpha: 1 }, 4).wait(33).to({ alpha: 0 }, 9, cjs.Ease.get(-1)).wait(1));

			// Layer 14
			this.instance_9 = new lib.Symbol15();
			this.instance_9.parent = this;
			this.instance_9.setTransform(1082.5, 953.8, 1, 1, 0, 0, 0, 169.6, 50);
			this.instance_9.alpha = 0;
			this.instance_9._off = true;

			this.instance_10 = new lib.Symbol12();
			this.instance_10.parent = this;
			this.instance_10.setTransform(1082.5, 953.8, 1, 1, 0, 0, 0, 169.6, 50);
			this.instance_10._off = true;

			this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(90).to({ _off: false }, 0).to({ _off: true, alpha: 1 }, 4).wait(33).to({ _off: false, alpha: 0 }, 9, cjs.Ease.get(-1)).wait(1));
			this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(90).to({ _off: false }, 4).wait(33).to({ _off: true, alpha: 0 }, 9, cjs.Ease.get(-1)).wait(1));

			// Layer 11
			this.instance_11 = new lib.Symbol11();
			this.instance_11.parent = this;
			this.instance_11.setTransform(1074.5, 655.1, 1, 1, 0, 0, 0, 240.5, 381.5);
			this.instance_11.alpha = 0;
			this.instance_11._off = true;

			this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(60).to({ _off: false }, 0).to({ y: 633.5, alpha: 1 }, 4).to({ y: 655.1 }, 4).to({ y: 461.2 }, 4).to({ y: 676.6 }, 5).to({ y: 655.1 }, 2).wait(6).to({ alpha: 0 }, 4).wait(48));

			// Layer 9
			this.instance_12 = new lib.Symbol10();
			this.instance_12.parent = this;
			this.instance_12.setTransform(1058.5, 744.5, 1, 1, 0, 0, 0, 1058.5, 744.5);

			this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(60).to({ y: 733.6 }, 4).to({ y: 744.5 }, 4).to({ y: 550.7 }, 4).to({ y: 766.1 }, 5).to({ y: 744.5 }, 2).wait(11).to({ y: 735.8 }, 4).to({ y: 744.5 }, 12).wait(31));

		}).prototype = p = new cjs.MovieClip();
		p.nominalBounds = new cjs.Rectangle(0, 0, 2154.3, 1639.2);


		// stage content:
		(lib.kitty_export = function (mode, startPosition, loop) {
			this.initialize(mode, startPosition, loop, {});

			// angry face
			this.instance = new lib.Symbol1();
			this.instance.parent = this;
			this.instance.setTransform(178.1, 226.9, 0.156, 0.156, 0, 0, 180, 1073.2, 817.3);

			this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

		}).prototype = p = new cjs.MovieClip();
		p.nominalBounds = new cjs.Rectangle(187.5, 284.8, 335.1, 254.9);
		// library properties:
		lib.properties = {
			id: '9F777150AFD2374D92BF3748B611C26C',
			width: 355,
			height: 370,
			fps: 31,
			color: '#FFFFFF',
			opacity: 1.00,
			manifest: [
				{ src: 'static/assets/images/kitty_export_atlas_.png?1505548647994', id: 'kitty_export_atlas_' }
			],
			preloads: []
		};



		// bootstrap callback support:

		(lib.Stage = function (canvas) {
			createjs.Stage.call(this, canvas);
		}).prototype = p = new createjs.Stage();

		p.setAutoPlay = function (autoPlay) {
			this.tickEnabled = autoPlay;
		};
		p.play = function () { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()); };
		p.stop = function (ms) { if (ms) this.seek(ms); this.tickEnabled = false; };
		p.seek = function (ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); };
		p.getDuration = function () { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; };

		p.getTimelinePosition = function () { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; };

		an.bootcompsLoaded = an.bootcompsLoaded || [];
		if (!an.bootstrapListeners) {
			an.bootstrapListeners = [];
		}

		an.bootstrapCallback = function (fnCallback) {
			an.bootstrapListeners.push(fnCallback);
			if (an.bootcompsLoaded.length > 0) {
				for (let i = 0; i < an.bootcompsLoaded.length; ++i) {
					fnCallback(an.bootcompsLoaded[i]);
				}
			}
		};

		an.compositions = an.compositions || {};
		an.compositions['9F777150AFD2374D92BF3748B611C26C'] = {
			getStage: function () { return exportRoot.getStage(); },
			getLibrary: function () { return lib; },
			getSpriteSheet: function () { return ss; },
			getImages: function () { return img; }
		};

		an.compositionLoaded = function (id) {
			an.bootcompsLoaded.push(id);
			for (let j = 0; j < an.bootstrapListeners.length; j++) {
				an.bootstrapListeners[j](id);
			}
		};

		an.getComposition = function (id) {
			return an.compositions[id];
		};


	}
}


