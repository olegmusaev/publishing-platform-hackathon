# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from django.contrib import admin
from django.views.decorators.cache import cache_page
from GotyUser import urls as GotyUserUrls
from EmailCollector import urls as EmailCollectorUrls
from django.contrib.auth import views as auth_views

from home import views


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^user/', include('GotyUser.urls', namespace='user')),
    url(r'^email/', include(EmailCollectorUrls)),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
    url(r'^$', cache_page(60)(views.HomeView.as_view())),
    url(r'^home/', cache_page(60)(views.HomeView.as_view()), name='home'),
    url(r'^choose/', cache_page(60)(views.ChooseView.as_view()), name='choose'),
    url(r'^battle/', include('battle.urls', namespace='battle')),
    url(r'^feed/', include('feed.urls', namespace='feed')),
    url(r'^leaderboard/', include('leaderboard.urls', namespace='leaderboard')),
]
