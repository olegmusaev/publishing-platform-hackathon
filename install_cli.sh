#!/bin/bash

declare -xr LIBRARY_DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/build/"
source $LIBRARY_DIRECTORY/helpers.sh

SRC_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

_fire="git+https://github.com/google/python-fire.git"
_cli="${SRC_ROOT}/cli/"

_tmp_dir=$(mktemp -d XXX.tmp)

APP_NAME="goty"

_venv_name="${APP_NAME}-cli"
_pex_name="${APP_NAME}"
_pip="${_tmp_dir}/${_venv_name}/bin/pip"
_pex="${_tmp_dir}/${_venv_name}/bin/pex"
_python="${_tmp_dir}/${_venv_name}/bin/python"

PEX_BINARY_PATH="/usr/local/bin/${_pex_name}"


SETUP_PY=$(cat <<EOF
from setuptools import setup

setup(
    name='${APP_NAME}cli',
    version='1.0',
    scripts=['cli', '_ROOT_DIR']
)

EOF
)

trap_func() {
    # wappers
    rm -rf $_cli/setup.py
    rm -rf $_cli/_ROOT_DIR

    rm -rf $_tmp_dir
}

trap 'trap_func' INT TERM ERR EXIT


check_requriements() {
    if [ -z $(command -v virtualenv) ]; then
        red "python virtualenv doesnt exist. Exit"
        exit 1
    fi
}

create_tmp_venv() {
    if [ -d $_tmp_dir/$_venv_name ]; then
        rm -rf $_tmp_dir/$_venv_name
    fi

    green "    ✔ Create virtualenv"
    virtualenv -q $_tmp_dir/$_venv_name
}

install_pex() {
    green "    ✔ Install pex"
    $_pip -q install -U pip==9.0.1 pex==1.2.7 requests==2.16.5 wheel==0.29.0
}

generate_pip_wrapper() {
    green "    ✔ Generate pip wrapper"
    echo "$SETUP_PY" > $_cli/setup.py
    echo "$SRC_ROOT" > $_cli/_ROOT_DIR
}

collect_wheel() {
    green "    ✔ Create wheelhouse"
    $_pip -q wheel -w $_tmp_dir/wheelhouse $_fire file://$_cli
}

generate_pex() {
    if [ -d ~/.pex ]; then
        rm -rf ~/.pex
    fi

    green "    ✔ Generate BIN package"
    $_pex -f $_tmp_dir/wheelhouse --no-index -c cli -o $_tmp_dir/$APP_NAME fire ${APP_NAME}cli
}

move_pex() {
    _path="/usr/local/bin"

    if [ -w $_path ]; then
        mv $_tmp_dir/$APP_NAME $_path/$APP_NAME
    else
        purple "        Password sudo is requried."
        sudo mv $_tmp_dir/$APP_NAME $_path/$APP_NAME
    fi
}

_done() {
    green "    ✔ Done!"
}

check_requriements
create_tmp_venv
install_pex
generate_pip_wrapper
collect_wheel
generate_pex
move_pex
_done