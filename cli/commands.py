#!/usr/bin/env python
# -*- coding: utf-8 -*-

# pip install git+https://github.com/google/python-fire.git

import os, re
import subprocess

import fire

_SILENCE = "> /dev/null"
_PROJECT="goty"

def red(text):
    return '\033[0;31m{}\033[0m'.format(text)

_root_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))

class Compose(object):
    _compose_ymls = {
        'dev': {
            'path': 'build/compose/',
            'ymls': [
                'docker-compose.yml',
            ]
        }
    }

    def __init__(self, tag='latest', env='dev', mobile=False):
        self._tag = tag if tag is not None else 'latest'
        self._env = env if env is not None else 'dev'
        self._mobile = mobile if mobile is not None else False

    def _call(self, command, **env):
        env = dict([(k, str(v)) for k, v in env.items()])
        return subprocess.call(command, shell=True, stderr=subprocess.STDOUT, env=dict(os.environ, **env))

    def _docker_compose(self, command):
        ymls = self._compose_ymls[self._env]
        ymls_path = ymls['path']

        dc = 'docker-compose'

        if self._mobile:
            ymls['ymls'].append('docker-compose.mobile.yml')

        for yml in ymls['ymls']:
            dc = '{} -f {}'.format(dc, os.path.join(ymls_path, yml))

        full_command = '{} -p {} {}'.format(dc, _PROJECT, command)

        return full_command

    def log(self, service='app'):
        self._call(self._docker_compose('logs -f {service}'.format(service=service)))

    def restart(self, service='app'):
        self._call(self._docker_compose('restart {service}'.format(service=service)))

    def up(self, daemon=True):
        self._call(self._docker_compose('up {}'.format('-d' if daemon else '')))

    def ps(self):
        self._call(self._docker_compose('ps'))

    def down(self, volume=False):
        self._call(self._docker_compose('down {}'.format('-v' if volume else '')))

    def build(self):
        self._call(self._docker_compose('build'))

    def deploy(self, static=False, build=True):
        if build:
            self.build()
        self.up(daemon=True)

        if static:
            GOTY(tag=None, nocache=None).run_compilestatic()

    def re_deploy(self, build=True):
        self.down(volume=True)
        self.deploy(build=build)

    def managepy(self, command=''):
        self._call(self._docker_compose('exec app python manage.py {}'.format(command)))


class Build(object):

    _entities = {
        'base': {'dockerfile': 'build/images/base/Dockerfile', 'context': '.', 'depends_on': 'libs'},
        'ingress': {'dockerfile': 'build/images/ingress/Dockerfile', 'context': '.'},
        'flake8': {'dockerfile': 'build/images/helpers/flake8/Dockerfile', 'context': 'build/images/helpers/flake8/'},
        'febuilder': {'dockerfile': 'build/images/helpers/febuilder/Dockerfile', 'context': '.'},
    }

    def __init__(self, tag, nocache):
        self._tag = tag if tag is not None else 'latest'
        self._nocache = nocache if nocache is not None else False

    def _call(self, command, output=False, **env):
        env = dict([(k, str(v)) for k, v in env.items()])

        kwargs = {
            'shell': True,
            'stderr': subprocess.STDOUT,
            'env': dict(os.environ, **env),
        }
        method = subprocess.check_output if output else subprocess.call

        return method(command, **kwargs)

    def _get_image_name(self, dockerfile):
        with open(dockerfile, 'r') as f:
            m = re.search('^#\s?image_name\s?\:(.+)\n', f.read())

        try:
            return m.group(1).strip()
        except:
            raise Exception("Cannot find image_name: in Dockerfile!")

    def _docker_tag(self, dockerfile):
        return '{}:{}'.format(self._get_image_name(dockerfile), self._tag)

    def _docker_build(self, dockerfile, context='.'):

        docker_tag = self._docker_tag(dockerfile)

        print('\ntag: {}\n'.format(red(docker_tag)))

        command = 'docker build'

        if self._nocache:
            command = '{} --no-cache'.format(command)

        full_command = '{command} ' \
                       '-f {dockerfile} ' \
                       '-t {dockertag} {context}'.format(command=command,
                                                         dockerfile=dockerfile,
                                                         dockertag=docker_tag,
                                                         context=context)

        return full_command

    def _depends_on(self, entity):
        _dict = self._entities[entity]
        depends_tag = self._docker_tag(_dict['dockerfile'])
        docker_ps = self._call('docker images --format "{{.Repository}}:{{.Tag}}" %s' % depends_tag, output=True).strip()

        if docker_ps != depends_tag:
            method = '{}'.format(entity)
            getattr(self, method)()

    def _builds(self, entity):
        _dict = self._entities[entity]

        # Feature is disabled until docker-ce 17.05 will released
        # if 'depends_on' in _dict.keys():
        #     self._depends_on(_dict['depends_on'])

        self._call(self._docker_build(dockerfile=_dict['dockerfile'],context=_dict['context']))

    def base(self):
        self._builds('base')

    def ingress(self):
        self._builds('ingress')

    def flake8(self):
        self._builds('flake8')

    def febuilder(self):
        self._builds('febuilder')

class GOTY(object):

    def __init__(self, tag=None, nocache=None, env=None, mobile=None):
        self.build = Build(tag=tag, nocache=nocache)
        self.compose = Compose(tag=tag, env=env, mobile=mobile)

    def _call(self, command, **env):
        env = dict([(k, str(v)) for k, v in env.items()])
        return subprocess.call(command, shell=True, stderr=subprocess.STDOUT, env=dict(os.environ, **env))

    def run_flake(self, report=False):
        self._call('./build/scripts/run_flake8.sh {}'.format('--noreport' if not report else ''))

    def run_compilestatic(self, report=False):
        self._call('./build/scripts/run_compilestatic.sh')

def main():
  fire.Fire(GOTY, name='goty')


if __name__ == '__main__':
  main()
