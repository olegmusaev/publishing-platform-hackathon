'use strict';

const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const resolve = (relativePath) => path.resolve(process.cwd(), relativePath);

module.exports = {
    entry: {
        main: resolve('./src/home/frontend/main.js')
    },
    output: {
        filename: 'js/[name].js',
        publicPath: '/static/',
        path: resolve('./src/home/static')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader'
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract([
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMaps: true
                        }
                    },
                    {
                        loader: 'resolve-url-loader'
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            outputStyle: 'expanded',
                            sourceMap: true
                        }
                    }
                ])
            },
            {
                test: /\.(jpe?g|png)$/,
                loader: 'file-loader',
                options: {
                    name: '/media/[name].[hash:8].[ext]'
                }
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('css/[name].css')
    ]
};
