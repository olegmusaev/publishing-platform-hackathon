### 

#### Deploy

1. ```./install_cli.sh```
2. ```goty compose  deploy```

#### Deploy on windows
1. ```./run_win.sh```

### Front API

#### User Api

1. Get user: **GET** user/{user_id}
    ```
    {
        hp: 0,
        hp_updated_at: null,
        xp: 0,
        level: 50,
        hp_lvl: 0,
        stone_lvl: 0,
        paper_lvl: 0,
        scissors_lvl: 17,
        name: "username",
        first_name: "first_name",
        last_name: "last_name",
        battles: 100,
        wins: 50,
        race: "race"
    }
    ``` 

2. Update user: **POST** user/update/?delta_stone_lvl=1&delta_paper_lvl=1&delta_scissors_lvl=1&race=race
    Response ok:
    ```
    200
    {
        name: name,
        race: race,
        scissors_lvl: 0,
        stone_lvl: 50,
        paper_lvl: 0,
        hp_lvl: 0,
    }
    ``` 
    Response error:
    ```
    200
    {
        'error': 'error'
    }
    ```
    
#### Email collector

1) Add email **POST** email/create/?email=test@test.com
    
    Response ok: status 200


#### Feed Api

1. отправить сообщение в чат(только для залогиненных): **POST** feed/post_message/?message=Как_тут_играть?
    Response ok(json):
    ```
    200
    {}
    ```


2. забрать ленту сообщений: **GET** feed/load_feed/?count=2
    count - сколько сообщений грузить(по дефолту 10)
    возвращает count последних сообщений начиная с самого последнего
    Response ok(json):
    ```
    200
    {
        "messages": [
            {
                "player": "DenisShushkevich",
                "text": "message3"
            },
            {
                "player": "DenisShushkevich",
                "text": "message2"
            }
        ]
    }
    ```

#### Application Api

1. подать заявку на бой (кнопка В БОЙ): **POST** battle/application/new/
    Response ok(json):
    ```
    200
    {
        "application_id": 1  // id заявки
        "status": 'in_process'  //  статус заявки
        "battle_id": null  // id боя
    }
    ```
    Response error:
    400 если у пользователя мало HP для вступления в бой


2. статус заявки: **POST** battle/application/{app_id}/status/
    Response ok(json):
    ```
    200
    {
        "application_id": 1  // id заявки
        "status": 'processed'  //  статус заявки
        "battle_id": 5  // id боя
    }
    ```
    Response error:
    400 если нету такой заявки

3. Список заявок пользователя: **GET** battle/application/list/
    Response ok(json):
    ```
    200
    {
    "applications": [
        {
            "status": 0,
            "application_id": 1,
            "battle_id": null
        }
        ]
    }
    ```


статусы заявки: 'in_process', 'processed'

Если заявка в in_process, значит для игрока подбирается оппонент.
и нужно опять идти за статусом

Если заявка в processed, то в battle_id содержится id боя, который был подобран.
и нужно идти уже за статусом боя


#### Battle Api

1. Make move: **POST** /battle/{battle_id}/move/
    Request json body (choices: ROCK = 0, PAPER = 1, SCISSORS = 2):
    ```
    {"choice": 1}
    ```
    Response ok:
    ```
    200
    {
        "result": true,
        "message": "Your choice was saved"
    }
    ```
    Response error:
    ```
    200
    {
        "result": false,
        "message": "Invalid choice"
    }
    ```

