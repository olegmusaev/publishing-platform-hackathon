#!/bin/sh

# global_vars


DOCKER_EXEC="docker"
DOCKER_REGISTRY="registry.wdo.io"
CODE_REPO="https://stash.wargaming.net/scm/csc/tickets_facade.git"


DOCKER_CLIENT_ENV=${DOCKER_CLIENT_CI:-$DOCKER_EXEC}
DOCKER_REGISTRY_ENV=${DOCKER_REGISTRY_CI:-$DOCKER_REGISTRY}
CODE_REPO_ENV=${CODE_REPO_CI:-$CODE_REPO}

declare -x DOCKER_CLIENT=${bamboo_DOCKER_CLIENT:-$DOCKER_CLIENT_ENV}
declare -x DOCKER_REGISTRY=${bamboo_DOCKER_REGISTRY:-$DOCKER_REGISTRY_ENV}
declare -x CODE_REPO=${bamboo_CODE_REPO:-$CODE_REPO_ENV}

# helpers func
red ()    { printf "\033[0;31m${1}\033[0m\n"; }
green ()  { printf "\033[0;32m${1}\033[0m\n"; }
yellow () { printf "\033[0;33m${1}\033[0m\n"; }
blue ()   { printf "\033[0;34m${1}\033[0m\n"; }
purple () { printf "\033[0;35m${1}\033[0m\n"; }
cyan ()   { printf "\033[0;36m${1}\033[0m\n"; }

get_image_name() {
    file=$1

    echo $(cat $file | grep -E "^#\s{0,}image_name" | sed -E 's/#\ {0,}image_name:\ {0,}//g' )
}

get_jira_tag() {
    text=$1
    echo $(echo "$text" | egrep -o '[A-Z]{1,10}-[0-9]+' | tr '[:upper:]' '[:lower:]')
}

generate_uniq_id() {
    VAR=${1:-be3a7a9f8997}
    BUILD_KEY=$(echo "${VAR}70d6b4fdf6bbaa28f8d250cb01228495bdf0f0f5" | cut -c1-12)

    if [  -f /proc/sys/kernel/random/uuid ]; then
        UUID=$(cat /proc/sys/kernel/random/uuid)
    else
        # uuid.uuid4()
        UUID=$(python -c "import uuid; print(uuid.uuid4())")
    fi

    UUID=$(echo $UUID | python -c "import sys; uuid=sys.stdin.read(); print('-'.join(uuid.split('-', -1)[:-1]))")
    echo "${UUID}-${BUILD_KEY}"
}

get_version_from_bamboo_branch() {
    VER=$(echo $bamboo_repository_branch_name | sed -e 's#release/##g')
    echo "${VER}rc.0+build.${bamboo_buildNumber}"
}

get_verbose_version() {
    BRANCH_NAME=""
    REVISION_NUMBER=""

    if [ ! -z $bamboo_repository_branch_name ] && \
       [ ! -z $bamboo_repository_revision_number ];
    then
        BRANCH_NAME=${bamboo_repository_branch_name}
        REVISION_NUMBER=${bamboo_repository_revision_number}
    else
        if [ $(git rev-parse --is-inside-work-tree) ];
        then
            BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)
            REVISION_NUMBER=$(git rev-parse HEAD)
        fi
    fi

    VERBOSE_VERSION="${BRANCH_NAME}_${REVISION_NUMBER}"

    echo $VERBOSE_VERSION
}

trap_func() {
	declare -a ARR=("${!1}")
    red "-- trap "
	for ((i = ${#ARR[@]}; i > 0; i--))
	do
	    eval "${ARR[$i]} 2>&1"
	done
}


wait_for_container() {
    CONTAINER_NAME=$1
    CONTAINER_PATTERN=$2

    TIMEOUT=${3:-80}
    TOTAL_TIME=0

    while ! $DOCKER_CLIENT logs $CONTAINER_NAME 2>&1 | grep "${CONTAINER_PATTERN}"
    do
        echo "Wait container: ${CONTAINER_NAME}. Sleep: ${TOTAL_TIME}"
        sleep 2

        if [ "${TOTAL_TIME}" -gt ${TIMEOUT} ]; then
            exit 1
        fi

        let "TOTAL_TIME+=2"
    done
}
