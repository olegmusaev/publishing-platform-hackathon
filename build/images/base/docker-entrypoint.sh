#!/bin/sh


cd /home/httpd/app/src/

if [ ! -f /home/httpd/.done ]; then
    echo "REGLAMENT start"
    python manage.py migrate --noinput
    touch /home/httpd/.done
fi


exec "$@"