#!/bin/sh

_null_test=$(cat <<EOF
<?xml version='1.0' encoding='utf-8'?>
<testsuite errors="0" skipped="1" failures="0" name="flake8" tests="1" time="1">
    <testcase name="flake8.all_ok">
    </testcase>
</testsuite>
EOF
)

git_ignorable="$(cat .gitignore | grep -ve '^#' | tr '\n' ',' | sed 's/,$//')"


flake8 \
    --disable-noqa \
    --ignore=E501,E902,E731 \
    --exclude .git,build,cli,logs,docs,migrations,${git_ignorable} \
    --show-source \
    --statistics \
    --max-complexity 10 \
    --max-line-length 120 \
    --import-order-style=pep8 \
    --count . > flake8_report.txt

cat flake8_report.txt

if [ "$(cat flake8_report.txt)" == "0" ]; then
        echo $_null_test > flake8_junit.xml
else
        flake8_junit flake8_report.txt flake8_junit.xml
fi
