#!/bin/bash
set -e
# ./build/scripts/build_ingress.sh 1.0dev1 <login> <password> true

_HELPERS="$(cd $(dirname ${BASH_SOURCE[0]})/../ && pwd )/helpers.sh"
source $_HELPERS

tag=$1
login=$2
password=$3
is_push=$4

_root_path="$(cd $(dirname ${BASH_SOURCE[0]})/../../ && pwd )"

_dockerfile=${_root_path}/build/images/ingress/Dockerfile
_context=${_root_path}/build/images/ingress/

_image=$(get_image_name $_dockerfile)

$DOCKER_CLIENT build \
    -t ${_image}:${tag} \
    -f $_dockerfile \
    $_context

if [ ! -z $login ] && [ ! -z $password ]; then
    $DOCKER_CLIENT login -u $login -p $password $DOCKER_REGISTRY
fi

if [ ! -z $is_push ]; then
    $DOCKER_CLIENT push ${_image}:${tag}
fi