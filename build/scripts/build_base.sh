#!/bin/bash
set -e
# ./build/scripts/build_base.sh 1.0dev1 <login> <password> true

_HELPERS="$(cd $(dirname ${BASH_SOURCE[0]})/../ && pwd )/helpers.sh"
source $_HELPERS

tag=$1
login=$2
password=$3
is_push=$4

_root_path="$(cd $(dirname ${BASH_SOURCE[0]})/../../ && pwd )"

_dockerfile=${_root_path}/build/images/base/Dockerfile

_modify_tag() {
    from=$1
    to=$2
    file=$3

    sed -i "s/:${from}/:${to}/g" $file || sed -i "" "s/:${from}/:${to}/g" $file
}

_image=$(get_image_name $_dockerfile)

_modify_tag "latest" "${tag}" "${_dockerfile}"

echo "-----------"
head $_dockerfile
echo "-----------"

$DOCKER_CLIENT build \
    -t ${_image}:${tag} \
    -f $_dockerfile \
    $_root_path

_modify_tag "${tag}" "latest" "${_dockerfile}"

if [ ! -z $login ] && [ ! -z $password ]; then
    $DOCKER_CLIENT login -u $login -p $password docker.io
fi

if [ ! -z $is_push ]; then
    $DOCKER_CLIENT push ${_image}:${tag}
fi