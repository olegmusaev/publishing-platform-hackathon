#!/bin/bash
set -e


_HELPERS="$(cd $(dirname ${BASH_SOURCE[0]})/../ && pwd )/helpers.sh"
source $_HELPERS

UNIQ_ID=$(generate_uniq_id "e470d6b4fd6b28f8d250cb45bdf0f0f5")

declare -a _trap=()=""

trap 'trap_func _trap[@]' EXIT
trap 'exit 127' INT TERM ERR

TAG=${1:-"latest"}

green "Pull febuilder container"
$DOCKER_CLIENT pull ppgoty/febuilder || \
$DOCKER_CLIENT images --format "{{.Repository}}:{{.Tag}}" | grep ppgoty/febuilder || \
    ( yellow "Please build image ppgoty/febuilder with build febuilder" && exit 1 )

green "Create container"
_trap+=('$DOCKER_CLIENT rm -v -f febuilder-${UNIQ_ID}')
$DOCKER_CLIENT create --name "febuilder-${UNIQ_ID}" \
    -a STDERR \
    -a STDOUT $ENV \
    ppgoty/febuilder \
    npm run build

green "Copy source to container"
for f in $(ls -A . | grep -v node_modules | grep -v cli |grep -v build); do
    blue "      $f"
    $DOCKER_CLIENT cp $f febuilder-${UNIQ_ID}:/src/frontend/
done

green "Run febuilder"
_trap+=('$DOCKER_CLIENT stop febuilder-${UNIQ_ID}')
$DOCKER_CLIENT start -a febuilder-${UNIQ_ID}


green "Copy report from container"
$DOCKER_CLIENT cp febuilder-${UNIQ_ID}:/src/frontend/ .