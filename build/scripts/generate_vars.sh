#!/bin/sh
set -e

_HELPERS="$(cd $(dirname ${BASH_SOURCE[0]})/../ && pwd )/helpers.sh"
source $_HELPERS

str=$1

jira_id=$(get_jira_tag $str)
bamboo_branch_sha=$(sha1sum <<<"$bamboo_repository_branch_name" | cut -c1-8)
str_sha=$(sha1sum <<<"$str" | cut -c1-8)

id=$jira_id
if [ -z $id ]; then id=$bamboo_branch_sha; fi
if [ -z $id ]; then id=$str_sha; fi
if [ -z $id ]; then echo "Cannot define ID. Check str for jira_tag or pass argument"; fi

feature_realm=$(echo "$id" | tr -d -c '[:alnum:]')

echo "id=${id}" > meta.inject
echo "feature_realm=${feature_realm}" >> meta.inject
