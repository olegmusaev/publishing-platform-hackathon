#!/bin/bash
set -e


_HELPERS="$(cd $(dirname ${BASH_SOURCE[0]})/../ && pwd )/helpers.sh"
source $_HELPERS

UNIQ_ID=$(generate_uniq_id "e470d6b4fd6b28f8d250cb45bdf0f0f5")

declare -a _trap=()=""

trap 'trap_func _trap[@]' EXIT
trap 'exit 127' INT TERM ERR

green "Pull flake container"
$DOCKER_CLIENT pull ppgoty/flake8 || \
$DOCKER_CLIENT images --format "{{.Repository}}" | grep ppgoty/flake8 || \
    ( yellow "Please build image ppgoty/flake8 with docker build-flake8" && exit 1 )

green "Create container"
_trap+=('$DOCKER_CLIENT rm -v -f flake8-${UNIQ_ID}')
$DOCKER_CLIENT create --name "flake8-${UNIQ_ID}" \
    -a STDERR \
    -a STDOUT \
    ppgoty/flake8

green "Copy source to container"
$DOCKER_CLIENT cp src/ flake8-${UNIQ_ID}:/

green "Copy .gitignore file"
$DOCKER_CLIENT cp .gitignore flake8-${UNIQ_ID}:/src/.gitignore

green "Run flake8"
_trap+=('$DOCKER_CLIENT stop flake8-${UNIQ_ID}')
$DOCKER_CLIENT start -a flake8-${UNIQ_ID}

echo ""
if [ "$1" != "--noreport" ]; then
    green "Copy report from container"
    $DOCKER_CLIENT cp flake8-${UNIQ_ID}:/src/flake8_junit.xml flake8_junit.xml
fi
